-- phpMyAdmin SQL Dump
-- version 3.3.10
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2016 年 04 月 28 日 08:19
-- 服务器版本: 5.1.37
-- PHP 版本: 5.2.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `demo`
--

-- --------------------------------------------------------

--
-- 表的结构 `xt_access`
--

CREATE TABLE IF NOT EXISTS `xt_access` (
  `role_id` smallint(6) unsigned NOT NULL,
  `node_id` smallint(6) unsigned NOT NULL,
  `level` tinyint(1) NOT NULL,
  `pid` smallint(6) NOT NULL,
  `module` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `groupId` (`role_id`) USING BTREE,
  KEY `nodeId` (`node_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `xt_access`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_address`
--

CREATE TABLE IF NOT EXISTS `xt_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `moren` int(11) NOT NULL COMMENT '是否为默认地址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `xt_address`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_baodan`
--

CREATE TABLE IF NOT EXISTS `xt_baodan` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(5) NOT NULL,
  `zhitui` int(4) NOT NULL,
  `tuandui` int(4) NOT NULL,
  `jine` int(4) NOT NULL,
  `shij` varchar(225) CHARACTER SET utf8 NOT NULL,
  `zhuantai` smallint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `xt_baodan`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_blist`
--

CREATE TABLE IF NOT EXISTS `xt_blist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `rdt` int(11) NOT NULL,
  `nums` int(11) NOT NULL,
  `cj_nums` int(11) NOT NULL,
  `cj_time` int(11) NOT NULL,
  `is_cj` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `prices` decimal(12,2) NOT NULL,
  `bz` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `xt_blist`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_bonus`
--

CREATE TABLE IF NOT EXISTS `xt_bonus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `did` int(11) NOT NULL,
  `s_date` int(11) NOT NULL,
  `e_date` int(11) NOT NULL,
  `b0` decimal(12,2) NOT NULL,
  `b1` decimal(12,2) NOT NULL,
  `b2` decimal(12,2) NOT NULL,
  `b3` decimal(12,2) NOT NULL,
  `b4` decimal(12,2) NOT NULL,
  `b5` decimal(12,2) NOT NULL,
  `b6` decimal(12,2) NOT NULL,
  `b7` decimal(12,2) NOT NULL,
  `b8` decimal(12,2) NOT NULL,
  `b9` decimal(12,2) NOT NULL,
  `b11` decimal(12,2) NOT NULL,
  `b12` decimal(12,2) NOT NULL,
  `b10` decimal(12,2) NOT NULL,
  `encash_l` int(11) NOT NULL,
  `encash_r` int(11) NOT NULL,
  `encash` int(11) NOT NULL,
  `is_count_b` int(11) NOT NULL,
  `is_count_c` int(11) NOT NULL,
  `is_pay` int(11) NOT NULL,
  `u_level` int(11) NOT NULL,
  `type` smallint(2) NOT NULL DEFAULT '0',
  `additional` varchar(50) NOT NULL COMMENT '额外奖',
  `encourage` varchar(50) NOT NULL COMMENT '阶段鼓励奖',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=261 ;

--
-- 转存表中的数据 `xt_bonus`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_bonushistory`
--

CREATE TABLE IF NOT EXISTS `xt_bonushistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `did` int(11) NOT NULL,
  `d_user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `action_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pdt` int(11) NOT NULL,
  `epoints` decimal(12,2) NOT NULL,
  `prep` decimal(12,2) NOT NULL,
  `allp` decimal(12,2) NOT NULL,
  `bz` text COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(1) NOT NULL COMMENT '充值0明细1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6463 ;

--
-- 转存表中的数据 `xt_bonushistory`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_card`
--

CREATE TABLE IF NOT EXISTS `xt_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bid` int(11) NOT NULL DEFAULT '0',
  `buser_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `card_no` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `card_pw` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `c_time` int(11) NOT NULL DEFAULT '0',
  `f_time` int(11) NOT NULL DEFAULT '0',
  `l_time` int(11) NOT NULL DEFAULT '0',
  `b_time` int(11) NOT NULL DEFAULT '0',
  `is_sell` int(3) NOT NULL DEFAULT '0',
  `is_use` int(3) NOT NULL DEFAULT '0',
  `money` decimal(12,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=150 ;

--
-- 转存表中的数据 `xt_card`
--

INSERT INTO `xt_card` (`id`, `bid`, `buser_id`, `card_no`, `card_pw`, `c_time`, `f_time`, `l_time`, `b_time`, `is_sell`, `is_use`, `money`) VALUES
(137, 0, '', '8181311080000', '364387', 1388974786, 1383840000, 1415376000, 0, 0, 30, 90.00),
(138, 0, '', '8181311080001', '364388', 1388974786, 1383840000, 1415376000, 0, 0, 30, 90.00),
(139, 0, '', '8181311080002', '364389', 1388974786, 1383840000, 1415376000, 0, 0, 30, 90.00),
(140, 0, '', '8181311080003', '364390', 1388974786, 1383840000, 1415376000, 0, 0, 30, 90.00),
(141, 0, '', '8181311080004', '364391', 1388974786, 1383840000, 1415376000, 0, 0, 30, 90.00),
(142, 0, '', '8181311080005', '364392', 1388974786, 1383840000, 1415376000, 0, 0, 30, 90.00),
(143, 0, '', '8181311080006', '364393', 1388974786, 1383840000, 1415376000, 0, 0, 30, 90.00),
(144, 0, '', '8181311080007', '364394', 1388974786, 1383840000, 1415376000, 0, 0, 30, 90.00),
(145, 0, '', '8181311080008', '364395', 1388974786, 1383840000, 1415376000, 0, 0, 30, 90.00),
(146, 0, '', '8181311080009', '364396', 1388974786, 1383840000, 1415376000, 0, 0, 30, 90.00),
(147, 0, '', '8181311080010', '364397', 1388974786, 1383840000, 1415376000, 0, 0, 30, 90.00),
(148, 0, '', '8181311080011', '364398', 1388974786, 1383840000, 1415376000, 0, 0, 30, 90.00),
(149, 0, '', '8181311080012', '364399', 1388974786, 1383840000, 1415376000, 0, 0, 30, 90.00);

-- --------------------------------------------------------

--
-- 表的结构 `xt_cash`
--

CREATE TABLE IF NOT EXISTS `xt_cash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `uid` int(11) NOT NULL,
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bid` int(11) NOT NULL DEFAULT '0',
  `b_user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `rdt` int(11) NOT NULL,
  `money` decimal(12,2) NOT NULL,
  `money_two` decimal(12,2) NOT NULL,
  `epoint` int(11) NOT NULL DEFAULT '0',
  `is_pay` int(11) NOT NULL COMMENT '1是已确认 0是未确认',
  `user_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_card` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `x1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x3` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x4` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sellbz` text COLLATE utf8_unicode_ci NOT NULL,
  `s_type` smallint(3) NOT NULL DEFAULT '0' COMMENT '0是提供帮助1是接受帮助',
  `is_buy` int(11) NOT NULL DEFAULT '0',
  `is_out` int(11) NOT NULL DEFAULT '0',
  `bdt` int(11) NOT NULL DEFAULT '0',
  `ldt` int(11) NOT NULL DEFAULT '0',
  `okdt` int(11) NOT NULL DEFAULT '0',
  `bz` text COLLATE utf8_unicode_ci NOT NULL,
  `is_sh` int(11) NOT NULL DEFAULT '0' COMMENT '0是未匹配1是已匹配',
  `is_sj` int(5) NOT NULL DEFAULT '0' COMMENT '延长时间',
  `is_leixing` int(5) NOT NULL COMMENT '1代表经理钱包2代表基金钱包',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1687 ;

--
-- 转存表中的数据 `xt_cash`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_cashhistory`
--

CREATE TABLE IF NOT EXISTS `xt_cashhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL,
  `order_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `did` int(11) NOT NULL,
  `d_order_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `action_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pdt` int(11) NOT NULL,
  `epoints` decimal(12,2) NOT NULL,
  `allp` decimal(12,2) NOT NULL,
  `bz` text COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(1) NOT NULL COMMENT '充值0明细1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `xt_cashhistory`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_cashpp`
--

CREATE TABLE IF NOT EXISTS `xt_cashpp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pp_orderid` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `order_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `b_order_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `uid` int(11) NOT NULL,
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bid` int(11) NOT NULL DEFAULT '0',
  `b_user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `rdt` int(11) NOT NULL,
  `money` decimal(12,2) NOT NULL,
  `money_two` decimal(12,2) NOT NULL,
  `epoint` int(11) NOT NULL DEFAULT '0',
  `is_pay` int(11) NOT NULL,
  `user_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_card` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `x1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x3` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x4` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sellbz` text COLLATE utf8_unicode_ci NOT NULL,
  `s_type` smallint(3) NOT NULL DEFAULT '0',
  `is_buy` int(11) NOT NULL DEFAULT '0',
  `bdt` int(11) NOT NULL DEFAULT '0',
  `ldt` int(11) NOT NULL DEFAULT '0',
  `okdt` int(11) NOT NULL DEFAULT '0',
  `bz` text COLLATE utf8_unicode_ci NOT NULL,
  `is_sh` int(11) NOT NULL DEFAULT '0',
  `is_sj` int(5) NOT NULL DEFAULT '0' COMMENT '延长时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=324 ;

--
-- 转存表中的数据 `xt_cashpp`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_chongzhi`
--

CREATE TABLE IF NOT EXISTS `xt_chongzhi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(50) COLLATE utf8_bin NOT NULL,
  `epoint` decimal(12,2) NOT NULL,
  `huikuan` decimal(12,2) NOT NULL,
  `zhuanghao` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `rdt` int(11) NOT NULL,
  `pdt` int(11) NOT NULL DEFAULT '0',
  `is_pay` smallint(2) NOT NULL,
  `stype` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=331 ;

--
-- 转存表中的数据 `xt_chongzhi`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_cody`
--

CREATE TABLE IF NOT EXISTS `xt_cody` (
  `c_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `cody_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- 转存表中的数据 `xt_cody`
--

INSERT INTO `xt_cody` (`c_id`, `cody_name`) VALUES
(1, 'profile'),
(2, 'password'),
(3, 'Jj_FA'),
(4, '4'),
(5, '5'),
(6, '6'),
(7, '7'),
(8, '8'),
(9, '9'),
(10, '10'),
(11, '11'),
(12, '12'),
(13, '13'),
(14, '14'),
(15, '15'),
(16, '16'),
(17, '17'),
(18, '18'),
(19, '19'),
(20, '20'),
(21, '21'),
(22, '22'),
(23, '23'),
(24, '24'),
(25, '25'),
(26, '26'),
(27, '27'),
(28, '28'),
(29, '29'),
(30, '30');

-- --------------------------------------------------------

--
-- 表的结构 `xt_cptype`
--

CREATE TABLE IF NOT EXISTS `xt_cptype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tpname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `b_id` int(11) NOT NULL DEFAULT '0',
  `s_id` int(11) NOT NULL DEFAULT '0',
  `t_pai` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `xt_cptype`
--

INSERT INTO `xt_cptype` (`id`, `tpname`, `b_id`, `s_id`, `t_pai`, `status`) VALUES
(1, '家用电器', 0, 0, 0, 0),
(2, '食品', 0, 0, 0, 0),
(3, '生活用品', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `xt_fck`
--

CREATE TABLE IF NOT EXISTS `xt_fck` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(64) DEFAULT NULL,
  `bind_account` varchar(50) DEFAULT NULL,
  `new_login_time` int(11) NOT NULL DEFAULT '0',
  `new_login_ip` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_login_time` int(11) unsigned DEFAULT '0',
  `last_login_ip` varchar(40) DEFAULT NULL,
  `create_ip` varchar(40) DEFAULT NULL,
  `login_count` mediumint(8) unsigned DEFAULT '0',
  `verify` varchar(32) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `type_id` tinyint(2) unsigned DEFAULT '0',
  `info` text,
  `name` varchar(25) DEFAULT NULL,
  `dept_id` smallint(3) DEFAULT NULL,
  `user_id` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '用户编号',
  `user_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '银行开户名',
  `password` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '一级密码',
  `pwd1` varchar(50) DEFAULT NULL COMMENT '一级密码不加密',
  `passopen` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '二级密码',
  `pwd2` varchar(50) DEFAULT NULL COMMENT '二级密码不加密',
  `passopentwo` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '三级密码',
  `pwd3` varchar(50) DEFAULT NULL COMMENT '三级密码不加密',
  `nickname` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '昵称',
  `qq` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'QQ',
  `bank_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '开户银行',
  `bank_card` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '银行卡号',
  `bank_province` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '开户银行所在省',
  `bank_city` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '开户银行所在城市',
  `bank_address` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '支行地址',
  `user_code` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '身份证',
  `user_address` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '联系地址',
  `user_post` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '联系方式',
  `user_tel` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '电话',
  `user_tuij` varchar(200) NOT NULL,
  `user_phone` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '手机',
  `rdt` int(11) NOT NULL COMMENT '注册时间',
  `treeplace` int(11) DEFAULT NULL COMMENT '区分左(中)右',
  `father_id` int(11) NOT NULL COMMENT '父节点',
  `father_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '父名',
  `re_id` int(11) NOT NULL COMMENT '推荐ID',
  `re_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '推荐人名称',
  `is_pay` int(11) NOT NULL COMMENT '是否开通(0,1)',
  `is_lock` int(11) NOT NULL COMMENT '是否锁定(0,1)',
  `is_lock_ok` int(3) NOT NULL DEFAULT '0',
  `shoplx` int(11) NOT NULL COMMENT '报单中心ID',
  `shop_a` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '//中心所在省',
  `shop_b` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '//中心所在县',
  `is_agent` int(11) NOT NULL COMMENT '报单中心(0,1,2)',
  `agent_max` decimal(12,2) NOT NULL COMMENT '申请报单总金额',
  `agent_use` decimal(12,2) NOT NULL COMMENT '奖金币',
  `agent_cash` decimal(12,2) NOT NULL COMMENT '报单币',
  `agent_kt` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '激活币',
  `agent_xf` decimal(12,2) NOT NULL DEFAULT '0.00',
  `agent_cf` decimal(12,2) NOT NULL DEFAULT '0.00',
  `agent_gp` decimal(12,2) NOT NULL DEFAULT '0.00',
  `gp_num` int(11) NOT NULL DEFAULT '0',
  `agent_lock` decimal(12,2) NOT NULL DEFAULT '0.00',
  `live_gupiao` int(11) NOT NULL DEFAULT '0',
  `all_in_gupiao` int(11) NOT NULL DEFAULT '0',
  `all_out_gupiao` int(11) NOT NULL DEFAULT '0',
  `p_out_gupiao` int(11) NOT NULL DEFAULT '0',
  `no_out_gupiao` int(11) NOT NULL DEFAULT '0',
  `ok_out_gupiao` int(11) NOT NULL DEFAULT '0',
  `yuan_gupiao` int(11) NOT NULL DEFAULT '0',
  `all_gupiao` int(11) NOT NULL DEFAULT '0',
  `tx_num` int(3) NOT NULL DEFAULT '0',
  `lssq` decimal(12,2) NOT NULL,
  `zsq` decimal(12,2) NOT NULL,
  `adt` int(11) NOT NULL COMMENT '申请成报单中心时间',
  `l` int(11) NOT NULL COMMENT '左边总人数',
  `r` int(11) NOT NULL COMMENT '右边总人数',
  `is_x` smallint(4) NOT NULL DEFAULT '0',
  `benqi_l` int(11) NOT NULL COMMENT '本期左区新增',
  `benqi_r` int(11) NOT NULL COMMENT '本期右区新增',
  `shangqi_l` int(11) NOT NULL COMMENT '上期左区剩余',
  `shangqi_r` int(11) NOT NULL COMMENT '上期右区剩余',
  `peng_num` int(11) NOT NULL DEFAULT '0',
  `u_level` int(11) NOT NULL COMMENT '等级(会员级别)',
  `is_boss` int(11) NOT NULL COMMENT '管理人为1,其它为0',
  `idt` int(11) NOT NULL,
  `pdt` int(11) NOT NULL COMMENT '开通时间',
  `re_level` int(11) NOT NULL COMMENT '相对于推的代数',
  `p_level` int(11) NOT NULL COMMENT '绝对层数',
  `re_path` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '推荐的路径',
  `p_path` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '自已的路径',
  `tp_path` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_del` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL COMMENT '隶属报单ID',
  `shop_name` varchar(50) NOT NULL,
  `b0` decimal(12,2) NOT NULL COMMENT '每期总资金',
  `b1` decimal(12,2) NOT NULL COMMENT '奖1',
  `b2` decimal(12,2) NOT NULL COMMENT '奖2',
  `b3` decimal(12,2) NOT NULL COMMENT '奖3',
  `b4` decimal(12,2) NOT NULL COMMENT '奖4',
  `b5` decimal(12,2) NOT NULL COMMENT '奖5',
  `b6` decimal(12,2) NOT NULL COMMENT '奖6',
  `b7` decimal(12,2) NOT NULL COMMENT '奖7',
  `b8` decimal(12,2) NOT NULL COMMENT '奖8',
  `b9` decimal(12,2) NOT NULL COMMENT '奖9',
  `b12` decimal(12,2) NOT NULL COMMENT '奖12',
  `b11` decimal(12,2) NOT NULL COMMENT '奖11',
  `b10` decimal(12,2) NOT NULL COMMENT '奖10',
  `wlf` int(11) NOT NULL COMMENT '网络费',
  `wlf_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `cpzj` decimal(12,2) NOT NULL COMMENT '注册金额',
  `zjj` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '总奖金',
  `re_money` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '推荐总注册金额',
  `cz_epoint` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '冲值总金额',
  `lr` int(11) NOT NULL COMMENT '中间总单数',
  `shangqi_lr` int(11) NOT NULL COMMENT '中间上期剩余单数',
  `benqi_lr` int(11) NOT NULL COMMENT '中间本期单数',
  `user_type` varchar(200) NOT NULL COMMENT '多线登录限制',
  `re_peat_money` decimal(12,2) NOT NULL COMMENT 'x',
  `re_nums` smallint(4) NOT NULL DEFAULT '0' COMMENT 'x',
  `re_nums_b` int(11) NOT NULL DEFAULT '0',
  `re_nums_l` int(11) NOT NULL DEFAULT '0',
  `re_nums_r` int(11) NOT NULL DEFAULT '0',
  `duipeng` decimal(12,2) NOT NULL,
  `_times` int(11) NOT NULL,
  `fanli` int(11) NOT NULL,
  `fanli_time` int(11) NOT NULL,
  `fanli_num` int(11) NOT NULL,
  `fanli_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `is_fenh` smallint(2) NOT NULL,
  `open` smallint(2) NOT NULL,
  `f4` int(11) NOT NULL DEFAULT '0',
  `new_agent` smallint(1) NOT NULL DEFAULT '0' COMMENT '//是否新服务中心',
  `day_feng` decimal(12,2) NOT NULL DEFAULT '0.00',
  `get_date` int(11) DEFAULT '0',
  `get_numb` int(11) DEFAULT '0',
  `is_jb` int(11) DEFAULT '0',
  `sq_jb` int(11) DEFAULT '0',
  `jb_sdate` int(11) DEFAULT '0',
  `jb_idate` int(11) DEFAULT '0',
  `man_ceng` int(11) NOT NULL DEFAULT '0' COMMENT '//满层数',
  `prem` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '//权限',
  `wang_j` smallint(1) NOT NULL DEFAULT '0' COMMENT '//结构图',
  `wang_t` smallint(1) NOT NULL DEFAULT '0' COMMENT '//推荐图',
  `get_level` int(11) NOT NULL DEFAULT '0',
  `is_xf` smallint(11) NOT NULL DEFAULT '0',
  `xf_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `is_zy` int(11) NOT NULL DEFAULT '0',
  `zyi_date` int(11) NOT NULL DEFAULT '0',
  `zyq_date` int(11) NOT NULL DEFAULT '0',
  `mon_get` decimal(12,2) NOT NULL DEFAULT '0.00',
  `xy_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `xx_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `down_num` int(11) NOT NULL DEFAULT '0',
  `u_pai` int(11) NOT NULL DEFAULT '0',
  `n_pai` int(11) NOT NULL DEFAULT '0',
  `ok_pay` int(11) NOT NULL DEFAULT '0',
  `wenti` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `wenti_dan` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_tj` int(11) NOT NULL DEFAULT '0',
  `re_f4` int(11) NOT NULL DEFAULT '0',
  `is_aa` int(3) NOT NULL DEFAULT '0',
  `is_bb` int(3) NOT NULL DEFAULT '0',
  `us_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `x_pai` int(11) NOT NULL DEFAULT '0',
  `x_out` int(3) NOT NULL DEFAULT '0',
  `x_num` int(11) NOT NULL DEFAULT '0',
  `is_lockqd` int(11) NOT NULL COMMENT '是否关闭签到',
  `is_lockfh` int(11) NOT NULL COMMENT '是否关闭分红',
  `seller_rate` int(11) NOT NULL DEFAULT '5',
  `lang` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '语言',
  `countrys` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '国家',
  `get_ceng` int(11) NOT NULL DEFAULT '0',
  `dengji` varchar(20) NOT NULL COMMENT '经理级别',
  `jingli` decimal(6,0) NOT NULL,
  `touzi` smallint(5) NOT NULL COMMENT '投资奖',
  `xiaohao` decimal(4,0) DEFAULT NULL,
  `baodanhao` int(10) DEFAULT '0',
  `suoshubaodan` int(5) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1584 ;

--
-- 转存表中的数据 `xt_fck`
--

INSERT INTO `xt_fck` (`id`, `account`, `bind_account`, `new_login_time`, `new_login_ip`, `last_login_time`, `last_login_ip`, `create_ip`, `login_count`, `verify`, `email`, `remark`, `create_time`, `update_time`, `status`, `type_id`, `info`, `name`, `dept_id`, `user_id`, `user_name`, `password`, `pwd1`, `passopen`, `pwd2`, `passopentwo`, `pwd3`, `nickname`, `qq`, `bank_name`, `bank_card`, `bank_province`, `bank_city`, `bank_address`, `user_code`, `user_address`, `user_post`, `user_tel`, `user_tuij`, `user_phone`, `rdt`, `treeplace`, `father_id`, `father_name`, `re_id`, `re_name`, `is_pay`, `is_lock`, `is_lock_ok`, `shoplx`, `shop_a`, `shop_b`, `is_agent`, `agent_max`, `agent_use`, `agent_cash`, `agent_kt`, `agent_xf`, `agent_cf`, `agent_gp`, `gp_num`, `agent_lock`, `live_gupiao`, `all_in_gupiao`, `all_out_gupiao`, `p_out_gupiao`, `no_out_gupiao`, `ok_out_gupiao`, `yuan_gupiao`, `all_gupiao`, `tx_num`, `lssq`, `zsq`, `adt`, `l`, `r`, `is_x`, `benqi_l`, `benqi_r`, `shangqi_l`, `shangqi_r`, `peng_num`, `u_level`, `is_boss`, `idt`, `pdt`, `re_level`, `p_level`, `re_path`, `p_path`, `tp_path`, `is_del`, `shop_id`, `shop_name`, `b0`, `b1`, `b2`, `b3`, `b4`, `b5`, `b6`, `b7`, `b8`, `b9`, `b12`, `b11`, `b10`, `wlf`, `wlf_money`, `cpzj`, `zjj`, `re_money`, `cz_epoint`, `lr`, `shangqi_lr`, `benqi_lr`, `user_type`, `re_peat_money`, `re_nums`, `re_nums_b`, `re_nums_l`, `re_nums_r`, `duipeng`, `_times`, `fanli`, `fanli_time`, `fanli_num`, `fanli_money`, `is_fenh`, `open`, `f4`, `new_agent`, `day_feng`, `get_date`, `get_numb`, `is_jb`, `sq_jb`, `jb_sdate`, `jb_idate`, `man_ceng`, `prem`, `wang_j`, `wang_t`, `get_level`, `is_xf`, `xf_money`, `is_zy`, `zyi_date`, `zyq_date`, `mon_get`, `xy_money`, `xx_money`, `down_num`, `u_pai`, `n_pai`, `ok_pay`, `wenti`, `wenti_dan`, `is_tj`, `re_f4`, `is_aa`, `is_bb`, `us_img`, `x_pai`, `x_out`, `x_num`, `is_lockqd`, `is_lockfh`, `seller_rate`, `lang`, `countrys`, `get_ceng`, `dengji`, `jingli`, `touzi`, `xiaohao`, `baodanhao`, `suoshubaodan`) VALUES
(1, 'admin', '0', 1461831313, '::1', 1461831145, '::1', NULL, 0, '1', '853616368@qq.com', '00', 0, 0, 1, 0, '0', 'admin', 0, '100000', '友软科技', 'e00cf25ad42683b3df678c61f42c6bda', 'admin1', 'c84258e9c39059a89ab77d846ddab909', 'admin2', 'd41d8cd98f00b204e9800998ecf8427e', '', '', '853616368', '支付宝', '853616368@qq.com', '河北', '石家庄', '详细开户地址', '8888', '51111', '853616368@QQ.com', '13931995587', '', '0', 1295884800, 0, 0, '0', 0, '0', 1, 0, 1, 1, '全国总代理', '全国总代理', 2, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 1.00, 10000000, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 2147483647, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0x2c, 0x2c, '', 0, 0, '0', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 9600.00, 0.00, 0.00, 0.00, 0, 0, 0, '285c6f87a0f69f7dbaa3f1c8a4cdb698', 0.00, 0, 0, 0, 0, 0.00, 0, 0, 1461772800, 0, 0.00, 0, 0, 3, 0, 0.00, 1461772800, 0, 1, 0, 0, 1323230473, 0, ',1,2,3,4,16,17,5,14,6,7,8,15,9,10,11,12,13,', 0, 0, 0, 0, 0.00, 0, 0, 4, 919379.05, 0.00, 0.00, 0, 1, 1, 1, '你爱人叫什么名字？', '123', 0, 0, 0, 0, '/A199/Public/Uploads/2014010816185380.jpg', 1, 1, 0, 0, 0, 5, '中文', '中国', 0, '见习经理', 860, 0, NULL, 2557, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `xt_fck2`
--

CREATE TABLE IF NOT EXISTS `xt_fck2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ceng` int(11) DEFAULT '0',
  `numb` int(11) DEFAULT '0',
  `fend` int(11) DEFAULT '0',
  `jishu` int(11) NOT NULL DEFAULT '0' COMMENT '//����',
  `fck_id` int(11) DEFAULT '0',
  `user_id` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nickname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_pay` int(11) NOT NULL DEFAULT '1',
  `u_level` int(11) DEFAULT '0',
  `re_num` int(11) NOT NULL DEFAULT '0',
  `pdt` int(11) NOT NULL,
  `treeplace` int(11) DEFAULT '0',
  `father_id` int(11) DEFAULT '0',
  `father_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `p_level` int(11) DEFAULT '0',
  `p_path` text CHARACTER SET utf8 COLLATE utf8_bin,
  `u_pai` varchar(200) COLLATE utf8_unicode_ci DEFAULT '0',
  `is_over` smallint(1) NOT NULL DEFAULT '0' COMMENT '//�ж��Ƿ�Ϊ����',
  `is_out` smallint(1) NOT NULL DEFAULT '0' COMMENT '//�ж��Ƿ��Ѿ�����',
  `is_yinc` smallint(1) NOT NULL DEFAULT '0' COMMENT '//����',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=84 ;

--
-- 转存表中的数据 `xt_fck2`
--

INSERT INTO `xt_fck2` (`id`, `ceng`, `numb`, `fend`, `jishu`, `fck_id`, `user_id`, `user_name`, `nickname`, `is_pay`, `u_level`, `re_num`, `pdt`, `treeplace`, `father_id`, `father_name`, `p_level`, `p_path`, `u_pai`, `is_over`, `is_out`, `is_yinc`) VALUES
(1, 0, 0, 0, 0, 1, '100000', '', '', 1, 1, 0, 1322031843, 0, 0, '', 0, 0x2c, '1', 1, 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `xt_fck_shop`
--

CREATE TABLE IF NOT EXISTS `xt_fck_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `did` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `create_time` int(11) NOT NULL,
  `is_pay` smallint(1) NOT NULL DEFAULT '0',
  `pdt` int(11) NOT NULL,
  `type` smallint(2) NOT NULL DEFAULT '0',
  `num` int(11) NOT NULL DEFAULT '1',
  `content` text NOT NULL,
  `p_dt` int(11) NOT NULL COMMENT '退货时间',
  `p_is_pay` smallint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `out_type` smallint(2) NOT NULL DEFAULT '0' COMMENT '0为未评论，1为已评论',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2533 ;

--
-- 转存表中的数据 `xt_fck_shop`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_fee`
--

CREATE TABLE IF NOT EXISTS `xt_fee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `i1` int(12) DEFAULT '0',
  `i2` int(12) DEFAULT '0',
  `i3` int(12) DEFAULT '0',
  `i4` int(12) DEFAULT '0',
  `i5` int(12) DEFAULT '0',
  `i6` int(12) DEFAULT '0',
  `i7` int(12) DEFAULT '0',
  `i8` int(12) DEFAULT '0',
  `i9` int(12) DEFAULT '0',
  `i10` int(12) DEFAULT '0',
  `s1` varchar(200) DEFAULT NULL,
  `s2` varchar(200) DEFAULT NULL,
  `s3` varchar(200) DEFAULT NULL,
  `s4` varchar(200) DEFAULT NULL,
  `s5` varchar(200) DEFAULT NULL,
  `s6` varchar(200) DEFAULT NULL,
  `s7` varchar(200) DEFAULT NULL,
  `s8` varchar(200) DEFAULT NULL,
  `s9` varchar(200) DEFAULT NULL,
  `s10` varchar(200) DEFAULT NULL,
  `s11` varchar(200) DEFAULT NULL,
  `s12` varchar(200) DEFAULT NULL,
  `s13` varchar(200) DEFAULT NULL,
  `s14` varchar(200) DEFAULT NULL,
  `s15` varchar(200) DEFAULT NULL,
  `s16` varchar(200) DEFAULT NULL,
  `s17` varchar(200) DEFAULT NULL,
  `s18` varchar(200) DEFAULT NULL,
  `s19` varchar(200) DEFAULT NULL,
  `s20` varchar(200) DEFAULT NULL,
  `str1` varchar(200) DEFAULT NULL,
  `str2` varchar(200) DEFAULT NULL,
  `str3` varchar(200) DEFAULT NULL,
  `str4` varchar(200) DEFAULT NULL,
  `str5` varchar(200) DEFAULT NULL,
  `str6` varchar(200) DEFAULT NULL,
  `str7` varchar(200) DEFAULT NULL,
  `str8` varchar(200) DEFAULT NULL,
  `str9` varchar(200) DEFAULT NULL,
  `str10` varchar(200) DEFAULT NULL,
  `str11` varchar(200) DEFAULT NULL,
  `str12` varchar(200) DEFAULT NULL,
  `str13` varchar(200) DEFAULT NULL,
  `str14` varchar(200) DEFAULT NULL,
  `str15` varchar(200) DEFAULT NULL,
  `str16` varchar(200) DEFAULT NULL,
  `str17` varchar(200) DEFAULT NULL,
  `str18` varchar(200) DEFAULT NULL,
  `str19` varchar(200) DEFAULT NULL,
  `str20` varchar(200) DEFAULT NULL,
  `str21` varchar(200) DEFAULT NULL,
  `str22` varchar(200) DEFAULT NULL,
  `str23` varchar(200) DEFAULT NULL,
  `str24` varchar(200) DEFAULT NULL,
  `str25` varchar(200) DEFAULT NULL,
  `str26` varchar(200) DEFAULT NULL,
  `str27` varchar(200) DEFAULT NULL,
  `str28` varchar(200) DEFAULT NULL,
  `str29` varchar(200) DEFAULT NULL,
  `str30` varchar(200) DEFAULT NULL,
  `str99` text NOT NULL,
  `us_num` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) DEFAULT NULL COMMENT '清空数据时间截',
  `f_time` int(11) NOT NULL DEFAULT '0',
  `a_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `b_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `ff_num` int(11) NOT NULL DEFAULT '0',
  `gp_one` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '当前价格',
  `gp_open` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '开盘价格',
  `gp_close` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '关盘价格',
  `gp_kg` int(3) NOT NULL DEFAULT '0' COMMENT '开关',
  `gp_cnum` int(11) NOT NULL DEFAULT '0' COMMENT '拆股次数',
  `gp_perc` varchar(50) NOT NULL COMMENT '手续费',
  `gp_inm` varchar(50) NOT NULL COMMENT '交易分账',
  `gp_inn` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `gp_cgbl` varchar(50) NOT NULL COMMENT '拆比例',
  `gp_fxnum` int(11) NOT NULL DEFAULT '0',
  `gp_senum` int(11) NOT NULL DEFAULT '0',
  `s22` varchar(200) DEFAULT NULL COMMENT '经理奖',
  `s23` varchar(200) DEFAULT NULL COMMENT '投资奖',
  `s24` varchar(200) DEFAULT NULL COMMENT '提供接受百分比',
  `s26` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `xt_fee`
--

INSERT INTO `xt_fee` (`id`, `i1`, `i2`, `i3`, `i4`, `i5`, `i6`, `i7`, `i8`, `i9`, `i10`, `s1`, `s2`, `s3`, `s4`, `s5`, `s6`, `s7`, `s8`, `s9`, `s10`, `s11`, `s12`, `s13`, `s14`, `s15`, `s16`, `s17`, `s18`, `s19`, `s20`, `str1`, `str2`, `str3`, `str4`, `str5`, `str6`, `str7`, `str8`, `str9`, `str10`, `str11`, `str12`, `str13`, `str14`, `str15`, `str16`, `str17`, `str18`, `str19`, `str20`, `str21`, `str22`, `str23`, `str24`, `str25`, `str26`, `str27`, `str28`, `str29`, `str30`, `str99`, `us_num`, `create_time`, `f_time`, `a_money`, `b_money`, `ff_num`, `gp_one`, `gp_open`, `gp_close`, `gp_kg`, `gp_cnum`, `gp_perc`, `gp_inm`, `gp_inn`, `gp_cgbl`, `gp_fxnum`, `gp_senum`, `s22`, `s23`, `s24`, `s26`) VALUES
(1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, '3', '10|10|5|5|', '100|5000|10000|20000|40000|60000', '0', '20', '10', '1|2|3|4|5|6|', '5', '100|2000|10000|30000|', '正式会员|见习经理|正式经理|市场总监', '1', '12', '40', '1|3', '2', '100', '银行帐号：农业银行\r\n银行卡号：123456789\r\n开户名：1111\r\n开户地址：aaaa bbbb cccc \r\n联系电话：13899998888', '本息|基金|经理', '农业银行|工商银行', '1|2|3|4|5', '20000', '10', '3400', '5', '0', '1', '123123|456789|123456789|987654321', 'zh_rm_ge_g_ws_ws_wws8111', '12', '7', '6', '对碰奖百分比', '见单奖', '领导奖', '领导奖封顶', 'C网见单金额 1级|2级', '80089701', '48zxYEVxfiC5k3el8dwQ49trHJ6hqvbpoP3n2nbu3tUdxv5ylUo1qxLUphGoiNgKgwruZRJvccxkFqTvnhVwgAYQd8Yu6MmFnkWbdlzulSzoaBpZnvbaSKD6y1r0TyPp', '开户银行', '', '/A181/Public/Uploads/media/qqcjtzjtyxgs_baofeng.flv', '/A136/Public/Images/02.jpg', '/A136/Public/Images/03.jpg', '中文|英文|马来西亚|泰语', '中国|美国|马来西亚|泰国', '', '维护时间00.00-8.00给您带来的不便敬请谅解~！', '恭喜您注册成功，请联系上级领导', '微信|支付宝', '', '你爱人叫什么名字？|你家养有什么宠物？|你的生日是多少？|你最喜欢什么？|你的出生地是？', 1, 1461831326, 1461772800, 240.00, 4000.00, 1, 0.10, 0.10, 0.10, 0, 0, '5', '50', '10', '1:1', 300000, 0, '10|5|3|2|1|0.2|', '0', '80|20|', 1);

-- --------------------------------------------------------

--
-- 表的结构 `xt_fenhong`
--

CREATE TABLE IF NOT EXISTS `xt_fenhong` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0',
  `user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `f_num` decimal(12,2) NOT NULL DEFAULT '0.00',
  `f_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `pdt` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `xt_fenhong`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_form`
--

CREATE TABLE IF NOT EXISTS `xt_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `e_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `e_content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `baile` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=91 ;

--
-- 转存表中的数据 `xt_form`
--

INSERT INTO `xt_form` (`id`, `title`, `content`, `e_title`, `e_content`, `user_id`, `create_time`, `update_time`, `status`, `baile`, `type`) VALUES
(90, 'fund家人们', '<p><img alt="" src="http://fund-ssss.1rcy.com/admin/Lib/ORG/FCKeditor/editor/images/smiley/msn/heart.gif" />家人们 由于目前会员数量较多,平台决定每晚24:00至8:00为服务器维护重启时间,在此时间除注册账号外禁止任何操作,</p>', '', '', 1, 1456248560, 0, 1, 0, '1');

-- --------------------------------------------------------

--
-- 表的结构 `xt_form_class`
--

CREATE TABLE IF NOT EXISTS `xt_form_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `baile` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `xt_form_class`
--

INSERT INTO `xt_form_class` (`id`, `name`, `baile`) VALUES
(1, '新闻公告', 0);

-- --------------------------------------------------------

--
-- 表的结构 `xt_game`
--

CREATE TABLE IF NOT EXISTS `xt_game` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `uid` int(12) NOT NULL,
  `g_money` decimal(12,2) NOT NULL,
  `used_money` decimal(12,2) NOT NULL,
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `xt_game`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_gouwu`
--

CREATE TABLE IF NOT EXISTS `xt_gouwu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `did` int(11) NOT NULL,
  `lx` int(11) NOT NULL,
  `ispay` smallint(2) NOT NULL,
  `pdt` int(11) NOT NULL,
  `money` decimal(12,2) NOT NULL,
  `shu` int(11) NOT NULL,
  `cprice` decimal(12,2) NOT NULL,
  `pvzhi` decimal(12,2) NOT NULL,
  `guquan` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `s_type` int(11) NOT NULL DEFAULT '0',
  `user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `us_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `us_address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `us_tel` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `isfh` int(11) NOT NULL DEFAULT '0',
  `fhdt` int(11) NOT NULL DEFAULT '0',
  `okdt` int(11) NOT NULL DEFAULT '0',
  `ccxhbz` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `countid` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=79 ;

--
-- 转存表中的数据 `xt_gouwu`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_gp`
--

CREATE TABLE IF NOT EXISTS `xt_gp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gp_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '股票名称',
  `danhao` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '股票单号',
  `opening` decimal(12,2) NOT NULL COMMENT '开盘价',
  `closing` decimal(12,2) NOT NULL COMMENT '收盘价',
  `today` decimal(12,2) NOT NULL COMMENT '当前报价',
  `most_g` decimal(12,2) NOT NULL COMMENT '最高价',
  `most_d` decimal(12,2) NOT NULL COMMENT '最低价',
  `up` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '涨幅',
  `down` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '跌幅',
  `gp_quantity` int(11) NOT NULL DEFAULT '0' COMMENT '股票数量',
  `cgp_num` int(11) NOT NULL DEFAULT '0',
  `gp_zongji` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '总价',
  `turnover` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '成交量',
  `f_date` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '发布时间',
  `status` int(11) NOT NULL COMMENT '状态（0关闭1开启）',
  `pao_num` int(11) NOT NULL DEFAULT '0',
  `ca_numb` int(11) NOT NULL DEFAULT '0',
  `all_sell` int(11) NOT NULL DEFAULT '0',
  `day_sell` int(11) NOT NULL DEFAULT '0',
  `yt_sellnum` int(11) NOT NULL DEFAULT '0',
  `buy_num` int(11) NOT NULL DEFAULT '0',
  `sell_num` int(11) NOT NULL DEFAULT '0',
  `fx_num` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `xt_gp`
--

INSERT INTO `xt_gp` (`id`, `gp_name`, `danhao`, `opening`, `closing`, `today`, `most_g`, `most_d`, `up`, `down`, `gp_quantity`, `cgp_num`, `gp_zongji`, `turnover`, `f_date`, `status`, `pao_num`, `ca_numb`, `all_sell`, `day_sell`, `yt_sellnum`, `buy_num`, `sell_num`, `fx_num`) VALUES
(1, '基金', 'GP1308185648', 0.10, 0.10, 0.10, 0.10, 0.10, NULL, '', 0, 212672, 0.00, '0', '1461831419', 1, 0, 0, 192672, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `xt_gp_sell`
--

CREATE TABLE IF NOT EXISTS `xt_gp_sell` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `uid` int(12) NOT NULL,
  `sNun` int(11) NOT NULL DEFAULT '0',
  `ispay` int(2) NOT NULL DEFAULT '0',
  `eDate` int(11) NOT NULL DEFAULT '0',
  `sell_mm` decimal(12,2) NOT NULL DEFAULT '0.00',
  `sell_ln` int(11) NOT NULL DEFAULT '0',
  `sell_mon` int(11) NOT NULL DEFAULT '0',
  `sell_num` int(11) NOT NULL DEFAULT '0',
  `sell_date` int(11) NOT NULL DEFAULT '0',
  `is_over` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `xt_gp_sell`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_gupiao`
--

CREATE TABLE IF NOT EXISTS `xt_gupiao` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `uid` int(12) NOT NULL,
  `pid` int(12) NOT NULL,
  `price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `one_price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `sNun` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `used_num` int(12) NOT NULL,
  `lnum` int(12) NOT NULL,
  `ispay` int(2) NOT NULL,
  `status` smallint(2) NOT NULL,
  `eDate` int(11) NOT NULL,
  `type` smallint(2) NOT NULL,
  `is_en` smallint(1) NOT NULL DEFAULT '0',
  `sell_mon` int(11) NOT NULL DEFAULT '0',
  `sell_num` int(11) NOT NULL DEFAULT '0',
  `sell_date` int(11) NOT NULL DEFAULT '0',
  `is_over` smallint(1) NOT NULL DEFAULT '0',
  `bz` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `buy_s` decimal(12,2) NOT NULL DEFAULT '0.00',
  `buy_a` decimal(12,2) NOT NULL DEFAULT '0.00',
  `buy_nn` int(11) NOT NULL DEFAULT '0',
  `sell_g` decimal(12,2) NOT NULL DEFAULT '0.00',
  `is_cancel` int(11) NOT NULL DEFAULT '0',
  `spid` int(11) NOT NULL DEFAULT '0',
  `last_s` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `xt_gupiao`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_hgupiao`
--

CREATE TABLE IF NOT EXISTS `xt_hgupiao` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `uid` int(12) NOT NULL,
  `pid` int(12) NOT NULL,
  `price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `gprice` decimal(12,2) NOT NULL DEFAULT '0.00',
  `one_price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `gmp` decimal(12,2) NOT NULL DEFAULT '0.00',
  `pmp` decimal(12,2) NOT NULL DEFAULT '0.00',
  `sNun` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ispay` int(2) NOT NULL,
  `eDate` int(11) NOT NULL,
  `type` smallint(2) NOT NULL,
  `is_en` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `xt_hgupiao`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_history`
--

CREATE TABLE IF NOT EXISTS `xt_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `did` int(11) NOT NULL,
  `user_did` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `action_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pdt` int(11) NOT NULL,
  `epoints` decimal(12,2) NOT NULL,
  `allp` decimal(12,2) NOT NULL,
  `bz` text NOT NULL,
  `type` smallint(1) NOT NULL COMMENT '充值0明细1',
  `act_pdt` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2691 ;

--
-- 转存表中的数据 `xt_history`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_huikui`
--

CREATE TABLE IF NOT EXISTS `xt_huikui` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `touzi` varchar(255) CHARACTER SET latin1 NOT NULL,
  `zhuangkuang` varchar(255) CHARACTER SET latin1 NOT NULL,
  `hk` decimal(12,2) NOT NULL,
  `time_hk` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `xt_huikui`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_img`
--

CREATE TABLE IF NOT EXISTS `xt_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `c_time` int(11) NOT NULL DEFAULT '0',
  `small_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `xt_img`
--

INSERT INTO `xt_img` (`id`, `title`, `c_time`, `small_url`, `img_url`) VALUES
(2, '1', 1389942148, '/B002/Public/Uploads/img/m_2014011714582241.jpg', '/B002/Public/Uploads/img/b_2014011714582241.jpg'),
(4, '2', 1389942531, '__PUBLIC__/Uploads/img/m_2014011715084822.jpg', '__PUBLIC__/Uploads/img/b_2014011715084822.jpg'),
(5, '3', 1389942544, '__PUBLIC__/Uploads/img/m_2014011715090176.jpg', '__PUBLIC__/Uploads/img/b_2014011715090176.jpg');

-- --------------------------------------------------------

--
-- 表的结构 `xt_item`
--

CREATE TABLE IF NOT EXISTS `xt_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `fsize` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `zip_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zip_title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `is_read` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `xt_item`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_jiadan`
--

CREATE TABLE IF NOT EXISTS `xt_jiadan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `adt` int(11) NOT NULL,
  `pdt` int(11) NOT NULL,
  `money` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '金额',
  `danshu` smallint(5) NOT NULL DEFAULT '0' COMMENT '单数',
  `is_pay` smallint(3) NOT NULL DEFAULT '0',
  `up_level` smallint(2) NOT NULL DEFAULT '0',
  `out_level` smallint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `xt_jiadan`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_jinl`
--

CREATE TABLE IF NOT EXISTS `xt_jinl` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL COMMENT '用户id',
  `name` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '姓名',
  `renshu` int(10) NOT NULL COMMENT '团队人数',
  `time` varchar(300) CHARACTER SET utf8 NOT NULL COMMENT '申请时间',
  `pay` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '是否申请成功',
  `status` smallint(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- 转存表中的数据 `xt_jinl`
--

INSERT INTO `xt_jinl` (`id`, `user_id`, `name`, `renshu`, `time`, `pay`, `status`) VALUES
(6, 100000, '神奇总店', 3, '1447816267', '见习经理', 1);

-- --------------------------------------------------------

--
-- 表的结构 `xt_msg`
--

CREATE TABLE IF NOT EXISTS `xt_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_uid` int(11) NOT NULL DEFAULT '0',
  `f_user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `s_uid` int(11) NOT NULL DEFAULT '0',
  `s_user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `f_time` int(11) NOT NULL DEFAULT '0',
  `f_del` smallint(3) NOT NULL DEFAULT '0',
  `s_del` smallint(3) NOT NULL DEFAULT '0',
  `f_read` smallint(3) NOT NULL DEFAULT '0',
  `s_read` smallint(3) NOT NULL DEFAULT '0',
  `yourid` int(11) NOT NULL COMMENT '您的id',
  `taid` int(11) NOT NULL COMMENT '匹配id',
  `xingming` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '匹配姓名',
  `dianhua` int(11) NOT NULL COMMENT '对方电话',
  `tupian` varchar(300) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- 转存表中的数据 `xt_msg`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_news_a`
--

CREATE TABLE IF NOT EXISTS `xt_news_a` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `n_title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `n_content` text COLLATE utf8_unicode_ci NOT NULL,
  `n_top` int(11) NOT NULL DEFAULT '0',
  `n_status` tinyint(1) NOT NULL DEFAULT '1',
  `n_create_time` int(11) NOT NULL,
  `n_update_time` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `xt_news_a`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_news_class`
--

CREATE TABLE IF NOT EXISTS `xt_news_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` int(11) NOT NULL,
  `type` smallint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- 转存表中的数据 `xt_news_class`
--

INSERT INTO `xt_news_class` (`id`, `title`, `create_time`, `type`) VALUES
(5, '成衣', 1295838556, 0),
(6, '汽车', 1295838667, 0),
(7, '食品', 1295838691, 1),
(8, '包包', 1295840380, 0),
(9, '数码', 1295853018, 0),
(10, '日常用品', 1295853092, 2),
(11, '电子产品', 1295921932, 2);

-- --------------------------------------------------------

--
-- 表的结构 `xt_peng`
--

CREATE TABLE IF NOT EXISTS `xt_peng` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(12) NOT NULL,
  `ceng` int(12) NOT NULL,
  `l` int(12) NOT NULL,
  `r` int(12) NOT NULL,
  `l1` int(12) NOT NULL,
  `r1` int(12) NOT NULL,
  `l2` int(12) NOT NULL,
  `r2` int(12) NOT NULL,
  `l3` int(12) NOT NULL,
  `r3` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `xt_peng`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_plan`
--

CREATE TABLE IF NOT EXISTS `xt_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '奖励计划',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `xt_plan`
--

INSERT INTO `xt_plan` (`id`, `content`) VALUES
(1, '<P>奖励计划</P>\r\n<P>\r\n<HR>\r\n\r\n<P></P>\r\n<P>&nbsp;</P>'),
(2, '<p>&nbsp;<span style="font-size: small">12332132154545454</span></p>'),
(3, '<p>我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章</p>'),
(4, '<embed allowfullscreen="true" allowscriptaccess="always" src="http://player.youku.com/player.php/sid/XNjYyNzAzNjMy/v.swf" quality="high" width="670" height="400" align="middle" type="application/x-shockwave-flash"></embed>'),
(5, '123123');

-- --------------------------------------------------------

--
-- 表的结构 `xt_product`
--

CREATE TABLE IF NOT EXISTS `xt_product` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `cid` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cptype` int(11) NOT NULL DEFAULT '0',
  `ccname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `xhname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `money` decimal(12,2) DEFAULT '0.00',
  `a_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `b_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `create_time` int(11) DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `yc_cp` int(11) NOT NULL DEFAULT '0',
  `countid` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `is_reg` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- 转存表中的数据 `xt_product`
--

INSERT INTO `xt_product` (`id`, `cid`, `name`, `cptype`, `ccname`, `xhname`, `money`, `a_money`, `b_money`, `create_time`, `content`, `img`, `yc_cp`, `countid`, `is_reg`) VALUES
(19, 'test0001', 'test', 1, '', '', 100.00, 100.00, 0.00, 1420208203, '<p>3123123123123</p>', '', 0, '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `xt_promo`
--

CREATE TABLE IF NOT EXISTS `xt_promo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `money` decimal(12,2) NOT NULL,
  `money_two` decimal(12,2) NOT NULL,
  `u_level` smallint(3) NOT NULL DEFAULT '0' COMMENT '升级前级别',
  `uid` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `up_level` smallint(3) NOT NULL COMMENT '升级后级别',
  `danshu` smallint(2) NOT NULL COMMENT '单数',
  `pdt` int(11) NOT NULL,
  `is_pay` smallint(3) NOT NULL DEFAULT '0',
  `u_bank_name` smallint(2) NOT NULL DEFAULT '0' COMMENT '汇款银行',
  `type` smallint(2) NOT NULL DEFAULT '0' COMMENT '0标示晋级，1标示加单',
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- 转存表中的数据 `xt_promo`
--

INSERT INTO `xt_promo` (`id`, `money`, `money_two`, `u_level`, `uid`, `create_time`, `up_level`, `danshu`, `pdt`, `is_pay`, `u_bank_name`, `type`, `user_name`, `user_id`) VALUES
(25, 0.00, 0.00, 1, 1, 1461831409, 4, 0, 1461831409, 1, 0, 0, ' <font color=red>後台升降級</font>', '100000');

-- --------------------------------------------------------

--
-- 表的结构 `xt_remit`
--

CREATE TABLE IF NOT EXISTS `xt_remit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0',
  `user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `b_uid` int(11) NOT NULL DEFAULT '0',
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `kh_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `or_time` int(11) NOT NULL DEFAULT '0',
  `orderid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bankid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ok_time` int(11) NOT NULL DEFAULT '0',
  `ok_type` int(11) NOT NULL DEFAULT '0',
  `is_pay` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `xt_remit`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_shouru`
--

CREATE TABLE IF NOT EXISTS `xt_shouru` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0',
  `user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `in_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `in_time` int(11) NOT NULL DEFAULT '0',
  `in_bz` text COLLATE utf8_unicode_ci NOT NULL,
  `in_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=225 ;

--
-- 转存表中的数据 `xt_shouru`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_sms`
--

CREATE TABLE IF NOT EXISTS `xt_sms` (
  `sid` int(13) NOT NULL AUTO_INCREMENT,
  `sms_type` varchar(20) NOT NULL DEFAULT '' COMMENT '短信类型',
  `sms_account` varchar(60) NOT NULL DEFAULT '' COMMENT '短信帐号',
  `sms_token` int(10) NOT NULL DEFAULT '0' COMMENT '短信密码',
  `sms_kg` int(5) NOT NULL,
  PRIMARY KEY (`sid`,`sms_token`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `xt_sms`
--

INSERT INTO `xt_sms` (`sid`, `sms_type`, `sms_account`, `sms_token`, `sms_kg`) VALUES
(2, 'huyi', 'cf_1002', 110011, 2);

-- --------------------------------------------------------

--
-- 表的结构 `xt_times`
--

CREATE TABLE IF NOT EXISTS `xt_times` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `benqi` int(11) NOT NULL COMMENT '本期结算日期',
  `shangqi` int(11) NOT NULL COMMENT '上期结算日期',
  `is_count_b` int(11) NOT NULL,
  `is_count_c` int(11) NOT NULL,
  `is_count` int(11) NOT NULL,
  `type` smallint(2) NOT NULL DEFAULT '0' COMMENT '是否已经结算',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=114 ;

--
-- 转存表中的数据 `xt_times`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_tiqu`
--

CREATE TABLE IF NOT EXISTS `xt_tiqu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `rdt` int(11) NOT NULL,
  `money` decimal(12,2) NOT NULL,
  `money_two` decimal(12,2) NOT NULL,
  `epoint` decimal(12,2) NOT NULL,
  `is_pay` int(11) NOT NULL,
  `user_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_card` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_address` varchar(200) NOT NULL,
  `user_tel` varchar(200) NOT NULL,
  `x1` varchar(50) DEFAULT NULL,
  `x2` varchar(50) DEFAULT NULL,
  `x3` varchar(50) DEFAULT NULL,
  `x4` varchar(50) DEFAULT NULL,
  `t_type` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `xt_tiqu`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_touzi`
--

CREATE TABLE IF NOT EXISTS `xt_touzi` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `money` int(10) NOT NULL,
  `time` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pay` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- 转存表中的数据 `xt_touzi`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_ulevel`
--

CREATE TABLE IF NOT EXISTS `xt_ulevel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `money` decimal(12,2) DEFAULT NULL,
  `u_level` smallint(3) DEFAULT '0' COMMENT '升级前级别',
  `uid` int(11) DEFAULT NULL,
  `user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` int(11) DEFAULT NULL,
  `up_level` smallint(3) DEFAULT NULL COMMENT '升级后级别',
  `danshu` smallint(2) DEFAULT NULL COMMENT '单数',
  `pdt` int(11) DEFAULT NULL,
  `is_pay` smallint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `xt_ulevel`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_verify_report`
--

CREATE TABLE IF NOT EXISTS `xt_verify_report` (
  `vid` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '电话',
  `verify_code` varchar(6) NOT NULL DEFAULT '' COMMENT '验证码',
  `datetime` varchar(20) NOT NULL DEFAULT '' COMMENT '短信发送时间',
  PRIMARY KEY (`vid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `xt_verify_report`
--

INSERT INTO `xt_verify_report` (`vid`, `phone`, `verify_code`, `datetime`) VALUES
(1, '18051825166', '4665', '1446708666'),
(2, '18658111562', '3244', '1446777219'),
(3, '15190014159', '9772', '1446714249'),
(4, '18036836430', '8114', '1448678107'),
(5, '13656214541', '您匹配成功的', '1448905461');

-- --------------------------------------------------------

--
-- 表的结构 `xt_xfhistory`
--

CREATE TABLE IF NOT EXISTS `xt_xfhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `did` int(11) NOT NULL,
  `d_user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `action_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pdt` int(11) NOT NULL,
  `epoints` decimal(12,2) NOT NULL,
  `allp` decimal(12,2) NOT NULL,
  `bz` text COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(1) NOT NULL COMMENT '充值0明细1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=564 ;

--
-- 转存表中的数据 `xt_xfhistory`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_xiaof`
--

CREATE TABLE IF NOT EXISTS `xt_xiaof` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `rdt` int(11) NOT NULL,
  `pdt` int(11) NOT NULL DEFAULT '0',
  `money` decimal(12,2) NOT NULL,
  `money_two` int(11) NOT NULL DEFAULT '0',
  `epoint` decimal(12,2) NOT NULL,
  `fh_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `is_pay` int(11) NOT NULL,
  `user_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_card` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `x1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x3` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x4` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `xt_xiaof`
--


-- --------------------------------------------------------

--
-- 表的结构 `xt_xml`
--

CREATE TABLE IF NOT EXISTS `xt_xml` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `money` decimal(12,2) NOT NULL,
  `amount` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `x_date` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `is_lock` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=133 ;

--
-- 转存表中的数据 `xt_xml`
--

INSERT INTO `xt_xml` (`id`, `money`, `amount`, `x_date`, `is_lock`) VALUES
(1, 0.16, '13100', '1405872000', 0),
(2, 0.30, '7101', '1405958400', 0),
(3, 0.18, '3100', '1406822400', 0),
(4, 0.18, '0', '1407081600', 0),
(5, 0.18, '100', '1407168000', 0),
(6, 0.18, '0', '1409068800', 0),
(7, 0.10, '0', '1409155200', 0),
(8, 0.10, '0', '1409241600', 0),
(9, 0.10, '0', '1409328000', 0),
(10, 0.10, '0', '1409500800', 0),
(11, 0.10, '0', '1409760000', 0),
(12, 0.10, '0', '1409846400', 0),
(13, 0.10, '0', '1410364800', 0),
(14, 0.10, '0', '1410883200', 0),
(15, 0.10, '0', '1413561600', 0),
(16, 0.10, '0', '1413648000', 0),
(17, 0.10, '0', '1413734400', 0),
(18, 0.10, '0', '1415894400', 0),
(19, 0.10, '0', '1416499200', 0),
(20, 0.10, '0', '1416585600', 0),
(21, 0.10, '0', '1416758400', 0),
(22, 0.10, '0', '1416844800', 0),
(23, 0.10, '0', '1416931200', 0),
(24, 0.10, '0', '1417104000', 0),
(25, 0.10, '0', '1417190400', 0),
(26, 0.10, '0', '1417708800', 0),
(27, 0.10, '0', '1419782400', 0),
(28, 0.10, '0', '1419868800', 0),
(29, 0.10, '0', '1419955200', 0),
(30, 0.10, '0', '1420300800', 0),
(31, 0.10, '0', '1420387200', 0),
(32, 0.10, '0', '1421251200', 0),
(33, 0.10, '0', '1424188800', 0),
(34, 0.10, '0', '1424880000', 0),
(35, 0.10, '0', '1424966400', 0),
(36, 0.10, '0', '1425139200', 0),
(37, 0.10, '0', '1425225600', 0),
(38, 0.10, '0', '1430064000', 0),
(39, 0.10, '0', '1439308800', 0),
(40, 0.10, '0', '1439568000', 0),
(41, 0.10, '0', '1439654400', 0),
(42, 0.10, '0', '1439740800', 0),
(43, 0.10, '0', '1439827200', 0),
(44, 0.10, '0', '1439913600', 0),
(45, 0.10, '0', '1440000000', 0),
(46, 0.10, '0', '1440172800', 0),
(47, 0.10, '0', '1440345600', 0),
(48, 0.10, '0', '1440432000', 0),
(49, 0.10, '0', '1440518400', 0),
(50, 0.10, '0', '1440604800', 0),
(51, 0.10, '0', '1440691200', 0),
(52, 0.10, '0', '1440864000', 0),
(53, 0.10, '0', '1440950400', 0),
(54, 0.10, '0', '1441036800', 0),
(55, 0.10, '0', '1441123200', 0),
(56, 0.10, '0', '1441209600', 0),
(57, 0.10, '0', '1441296000', 0),
(58, 0.10, '0', '1441382400', 0),
(59, 0.10, '0', '1441555200', 0),
(60, 0.10, '0', '1441728000', 0),
(61, 0.10, '0', '1445875200', 0),
(62, 0.10, '0', '1445961600', 0),
(63, 0.10, '0', '1446048000', 0),
(64, 0.10, '0', '1446134400', 0),
(65, 0.10, '0', '1446220800', 0),
(66, 0.10, '0', '1446307200', 0),
(67, 0.10, '0', '1446393600', 0),
(68, 0.10, '0', '1446480000', 0),
(69, 0.10, '0', '1446566400', 0),
(70, 0.10, '0', '1446652800', 0),
(71, 0.10, '0', '1446739200', 0),
(72, 0.10, '0', '1446998400', 0),
(73, 0.10, '0', '1447084800', 0),
(74, 0.10, '0', '1447171200', 0),
(75, 0.10, '0', '1447257600', 0),
(76, 0.10, '0', '1447344000', 0),
(77, 0.10, '0', '1447430400', 0),
(78, 0.10, '0', '1447603200', 0),
(79, 0.10, '0', '1447689600', 0),
(80, 0.10, '0', '1447776000', 0),
(81, 0.10, '0', '1447862400', 0),
(82, 0.10, '0', '1447948800', 0),
(83, 0.10, '0', '1448035200', 0),
(84, 0.10, '0', '1448208000', 0),
(85, 0.10, '0', '1448294400', 0),
(86, 0.10, '0', '1448380800', 0),
(87, 0.10, '0', '1448467200', 0),
(88, 0.10, '0', '1448553600', 0),
(89, 0.10, '0', '1448640000', 0),
(90, 0.10, '0', '1448726400', 0),
(91, 0.10, '0', '1448812800', 0),
(92, 0.10, '0', '1448899200', 0),
(93, 0.10, '0', '1448985600', 0),
(94, 0.10, '0', '1449072000', 0),
(95, 0.10, '0', '1449158400', 0),
(96, 0.10, '0', '1449244800', 0),
(97, 0.10, '0', '1449331200', 0),
(98, 0.10, '0', '1449417600', 0),
(99, 0.10, '0', '1449504000', 0),
(100, 0.10, '0', '1449590400', 0),
(101, 0.10, '0', '1449676800', 0),
(102, 0.10, '0', '1449763200', 0),
(103, 0.10, '0', '1450108800', 0),
(104, 0.10, '0', '1450195200', 0),
(105, 0.10, '0', '1450281600', 0),
(106, 0.10, '0', '1450368000', 0),
(107, 0.10, '0', '1450454400', 0),
(108, 0.10, '0', '1450540800', 0),
(109, 0.10, '0', '1450627200', 0),
(110, 0.10, '0', '1450713600', 0),
(111, 0.10, '0', '1450800000', 0),
(112, 0.10, '0', '1451232000', 0),
(113, 0.10, '0', '1451318400', 0),
(114, 0.10, '0', '1451404800', 0),
(115, 0.10, '0', '1451491200', 0),
(116, 0.10, '0', '1451750400', 0),
(117, 0.10, '0', '1451836800', 0),
(118, 0.10, '0', '1451923200', 0),
(119, 0.10, '0', '1452009600', 0),
(120, 0.10, '0', '1452096000', 0),
(121, 0.10, '0', '1452182400', 0),
(122, 0.10, '0', '1452268800', 0),
(123, 0.10, '0', '1453132800', 0),
(124, 0.10, '0', '1455811200', 0),
(125, 0.10, '0', '1455897600', 0),
(126, 0.10, '0', '1455984000', 0),
(127, 0.10, '0', '1456070400', 0),
(128, 0.10, '0', '1456156800', 0),
(129, 0.10, '0', '1456243200', 0),
(130, 0.10, '0', '1456329600', 0),
(131, 0.10, '0', '1456416000', 0),
(132, 0.10, '0', '1461772800', 0);

-- --------------------------------------------------------

--
-- 表的结构 `xt_zhuanj`
--

CREATE TABLE IF NOT EXISTS `xt_zhuanj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `in_uid` int(11) DEFAULT NULL,
  `out_uid` int(11) DEFAULT NULL,
  `in_userid` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `out_userid` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `epoint` decimal(12,2) DEFAULT NULL,
  `rdt` int(11) DEFAULT NULL,
  `sm` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `type` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `xt_zhuanj`
--

