<?php
//注册模块
class AgentAction extends CommonAction{
	
	public function _initialize() {
		header("Content-Type:text/html; charset=utf-8");
		$this->_inject_check(0);//调用过滤函数
		$this->_Config_name();//调用参数
 		$this->_checkUser();
	}
	
	
	public function cody(){
		//===================================二级验证
		$UrlID = (int) $_GET['c_id'];
		if (empty($UrlID)){
			$this->error('二级密码错误!');
			exit;
		}
		if(!empty($_SESSION['user_pwd2'])){
			$url = __URL__."/codys/Urlsz/$UrlID";
			$this->_boxx($url);
			exit;
		}
		$cody   =  M ('cody');
		$list	=  $cody->where("c_id=$UrlID")->field('c_id')->find();
		if ($list){
			$this->assign('vo',$list);
			$this->display('../Public/cody');
			exit;
		}else{
			$this->error('二级密码错误!');
			exit;
		}
	}
	public function codys(){
		//=============================二级验证后调转页面
		$Urlsz = (int) $_POST['Urlsz'];
		if(empty($_SESSION['user_pwd2'])){
			$pass  = $_POST['oldpassword'];
			$fck   =  M ('fck');
			if (!$fck->autoCheckToken($_POST)){
				$this->error('页面过期请刷新页面!');
				exit();
			}
			if (empty($pass)){
				$this->error('二级密码错误!');
				exit();
			}
	
			$where = array();
			$where['id'] = $_SESSION[C('USER_AUTH_KEY')];
			$where['passopen'] = md5($pass);
			$list = $fck->where($where)->field('id,is_agent')->find();
			if($list == false){
				$this->error('二级密码错误!');
				exit();
			}
			$_SESSION['user_pwd2'] = 1;
		}else{
			$Urlsz = $_GET['Urlsz'];
		}
		switch ($Urlsz){
			case 1;
				if($list['is_agent'] >=2){
					$this->error('您已经是区域代理!');
            		exit();
				}
				$_SESSION['Urlszpass'] = 'MyssXiGua';
				$bUrl = __URL__.'/agents';//申请代理
                $this->_boxx($bUrl);
			break;
				case 2;
				$_SESSION['Urlszpass'] = 'MyssShuiPuTao';
				$bUrl = __URL__.'/menber'; //未开通会员
				$this->_boxx($bUrl);
			break;
		
			case 3;
				$_SESSION['Urlszpass'] = 'Myssmenberok';
				$bUrl = __URL__.'/menberok'; //已开通会员
				$this->_boxx($bUrl);
			break;
			
			case 4;
				$_SESSION['UrlPTPass'] = 'MyssGuanXiGua';
				$bUrl = __URL__.'/adminAgents'; //后台确认区域代理
				$this->_boxx($bUrl);
			break;
			default;
			$this->error('二级密码错误!');
			exit;
		}
	}
	public function agents($Urlsz=0){
		//======================================申请会员中心/代理中心/区域代理
		if ($_SESSION['Urlszpass'] == 'MyssXiGua'){
			$fee_rs = M ('fee') -> find();
	
			$fck = M ('fck');
			$where = array();
			//查询条件
			$where['id'] = $_SESSION[C('USER_AUTH_KEY')];
			$field ='*';
			$fck_rs = $fck ->where($where)->field($field)->find();
	
			if ($fck_rs){
				//会员级别
				switch($fck_rs['is_agent']){
					case 0:
						$agent_status = '未申请区域代理!';
						break;
					case 1:
						$agent_status = '申请正在审核中!';
						break;
					case 2:
						$agent_status = '区域代理已开通!';
						break;
				}
	
				$this->assign ( 'fee_s6',$fee_rs['i1']);
				$this->assign ( 'agent_level',0);
				$this->assign ( 'agent_status',$agent_status);
				$this->assign ( 'fck_rs', $fck_rs);
				
				$Agent_Us_Name = C('Agent_Us_Name');
				$Aname = explode("|",$Agent_Us_Name);
				$this->assign ( 'Aname', $Aname);
				
				$this->display ('agents');
			}else{
				$this->error ('操作失败!');
				exit;
			}
		}else{
			$this->error ('错误!');
			exit;
		}
	}
	
	
	public function agentsAC(){
		//================================申请会员中心中转函数
		$content  = $_POST['content'];
		$agentMax = $_POST['agentMax'];
		$shoplx  = (int)$_POST['shoplx'];
		$shop_a  = $_POST['shop_a'];
		$shop_b  = $_POST['shop_b'];
		$fee=M('fee');
		$fee_rs=$fee->where('s9,s14')->find(1);
		$s14=(int)$fee_rs['s14'];
    	$s9 = explode("|",$fee_rs['s9']);		//会员级别费用
//		$one_mm = $s9[0];
		$one_mm = 1;
		
		if($shoplx==1){
			if($shop_a == "请选择"){
				$this->error('请输入区域代理区域!');
			}
		}else{
			if($shop_a == "请选择" || $shop_b == "请选择"){
				$this->error('请输入区域代理区域!');
			}
		}
		
		$fck = M ('fck');
		$id = $_SESSION[C('USER_AUTH_KEY')];		
		$where = array();
		$where['id'] = $id;
	
		$fck_rs = $fck->where($where)->field('*')->find();
		if($fck_rs){
			if($fck_rs['is_pay']  == 0){
				$this->error ('临时会员不能申请!');
				exit;
			}
			if($fck_rs['is_agent']  == 1){
				$this->error('上次申请还没通过审核!');
				exit;
			}
			if($fck_rs['u_level'] == 1){
				$this->error('只有4级代理和5级代理可以申请区域代理!');
				exit;
			}
			
			$bqycount=0;
			if($shoplx==1){
				$bqycount = $fck->where("is_agent>0 and shop_a=".$shop_a)->count;
			}elseif($shoplx==2){
				$bqycount = $fck->where("is_agent>0 and shop_b=".$shop_b)->count;
			}
			if($bqycount>0){
				$this->error('本区域的区域代理已经存在!');
				exit;
			}

			if(empty($content)){
 				$this->error ('请输入备注!');
 				exit;
			}

	
			if($fck_rs['is_agent'] == 0){
				$nowdate = time();
				$result = $fck -> query("update __TABLE__ set verify='".$content."',is_agent=1,shoplx=".$shoplx.",shop_a='".$shop_a."',shop_b='".$shop_b."',idt=$nowdate where id=".$id);
			}
	
			$bUrl = __URL__ .'/agents';
			$this->_box(1,'申请成功！',$bUrl,2);
	
		}else{
			$this->error('非法操作');
			exit;
		}
	}
	
	//未开通会员
	public function menber($Urlsz=0){
		//列表过滤器，生成查询Map对象
		if ($_SESSION['Urlszpass'] == 'MyssShuiPuTao'){
			$fck = M('fck');
			$map = array();
			$id = $_SESSION[C('USER_AUTH_KEY')];
			$gid = (int) $_GET['bj_id'];
// 			$map['shop_id'] = $id;
			$UserID = $_POST['UserID'];
			if (!empty($UserID)){
				import ( "@.ORG.KuoZhan" );  //导入扩展类
                $KuoZhan = new KuoZhan();
                if ($KuoZhan->is_utf8($UserID) == false){
                    $UserID = iconv('GB2312','UTF-8',$UserID);
                }
                unset($KuoZhan);
				$where['nickname'] = array('like',"%".$UserID."%");
				$where['user_id'] = array('like',"%".$UserID."%");
				$where['_logic']    = 'or';
				$map['_complex']    = $where;
				$UserID = urlencode($UserID);
			}
			$map['is_pay'] = array('eq',0);
			$map['_string'] = "shop_id=".$id." or re_id=".$id."";

            //查询字段
            $field  = '*';
            //=====================分页开始==============================================
            import ( "@.ORG.ZQPage" );  //导入分页类
            $count = $fck->where($map)->count();//总页数
    	    $listrows = C('ONE_PAGE_RE');//每页显示的记录数
            $page_where = 'UserID='.$UserID;//分页条件
            $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where);
            //===============(总页数,每页显示记录数,css样式 0-9)
            $show = $Page->show();//分页变量
            $this->assign('page',$show);//分页变量输出到模板
            $list = $fck->where($map)->field($field)->order('is_pay asc,pdt desc')->page($Page->getPage().','.$listrows)->select();
            $this->assign('list',$list);//数据输出到模板
            //=================================================
            
			$HYJJ = '';
            $this->_levelConfirm($HYJJ,1);
            $this->assign('voo',$HYJJ);//会员级别
			$where = array();
			$where['id'] = $id;
			$fck_rs = $fck->where($where)->field('*')->find();
			$this->assign('frs',$fck_rs);//注册币
			$this->display ('menber');
			exit;
		}else{
			$this->error('数据错误!');
			exit;
		}
	}
	
	//未开通会员
	public function menberok($Urlsz=0){
		//列表过滤器，生成查询Map对象
		if ($_SESSION['Urlszpass'] == 'Myssmenberok'){
			$fck = M('fck');
			$map = array();
			$id = $_SESSION[C('USER_AUTH_KEY')];
			$gid = (int) $_GET['bj_id'];
			$map['shop_id'] = $id;
			$map['is_pay'] = array('gt',0);
			$UserID = $_POST['UserID'];
			if (!empty($UserID)){
				import ( "@.ORG.KuoZhan" );  //导入扩展类
                $KuoZhan = new KuoZhan();
                if ($KuoZhan->is_utf8($UserID) == false){
                    $UserID = iconv('GB2312','UTF-8',$UserID);
                }
                unset($KuoZhan);
				$where['nickname'] = array('like',"%".$UserID."%");
				$where['user_id'] = array('like',"%".$UserID."%");
				$where['_logic']    = 'or';
				$map['_complex']    = $where;
				$UserID = urlencode($UserID);
			}

            //查询字段
            $field  = '*';
            //=====================分页开始==============================================
            import ( "@.ORG.ZQPage" );  //导入分页类
            $count = $fck->where($map)->count();//总页数
    	    $listrows = C('ONE_PAGE_RE');//每页显示的记录数
            $page_where = 'UserID='.$UserID;//分页条件
            $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where);
            //===============(总页数,每页显示记录数,css样式 0-9)
            $show = $Page->show();//分页变量
            $this->assign('page',$show);//分页变量输出到模板
            $list = $fck->where($map)->field($field)->order('is_pay asc,pdt desc')->page($Page->getPage().','.$listrows)->select();
            $this->assign('list',$list);//数据输出到模板
            //=================================================
            
			$HYJJ = '';
            $this->_levelConfirm($HYJJ,1);
            $this->assign('voo',$HYJJ);//会员级别
			$where = array();
			$where['id'] = $id;
			$fck_rs = $fck->where($where)->field('*')->find();
			$this->assign('frs',$fck_rs);//注册币
			$this->display ('menberok');
			exit;
		}else{
			$this->error('数据错误!');
			exit;
		}
	}
	
	
	public function menberAC(){
		//处理提交按钮
		$action = $_POST['action'];
		//获取复选框的值
		$OpID = $_POST['tabledb'];
		if (!isset($OpID) || empty($OpID)){
			$bUrl = __URL__.'/menber';
			$this->_box(0,'没有该会员！',$bUrl,1);
			exit;
		}
		switch ($action){
			case '开通会员':
				$this->_menberOpenUse($OpID,0);
				break;
			case '注册币开通会员':
				$this->_menberOpenUse($OpID,0);
				break;
			case '注册币开通会员':
				$this->_menberOpenUse($OpID,1);
				break;
			case '一进一出币开通会员':
				$this->_menberOpenUse($OpID,2);
				break;
			case '删除会员':
				$this->_menberDelUse($OpID);
				break;
			default:
				$bUrl = __URL__.'/menber';
				$this->_box(0,'没有该会员！',$bUrl,1);
				break;
		}
	}
	
	
	private function _menberOpenUse($OpID=0,$reg_money=0){
		//=============================================开通会员
		if ($_SESSION['Urlszpass'] == 'MyssShuiPuTao'){
			
// 			$length_arr = count($OpID);
// 			if($length_arr > 1){
// 				$this->error('一次只能开通一个会员');
// 				exit;
// 			}
			
		    $fck = D ('Fck');
			$fee = M ('fee');
			$gouwu = M ('gouwu');
			$shouru = M ('shouru');
			$blist = M('blist');
			$Guzhi = A('Guzhi');
		    if (!$fck->autoCheckToken($_POST)){
                $this->error('页面过期，请刷新页面！');
                exit;
            }

			//被开通会员参数
			$where = array();
			$where['id'] = array ('in',$OpID);  //被开通会员id数组
			$where['is_pay'] = 0;  //未开通的
			$field = '*';
			$vo = $fck ->where($where)->field($field)->order('id asc')->select();
			$fee_rs = $fee->field('s4')->find();
	    	$s4 = explode("|",$fee_rs['s4']);

			//区域代理参数
			$where_two =array();
			$field_two = '*';
			$ID = $_SESSION[C('USER_AUTH_KEY')];
			$where_two['id'] = $ID;
//			$where_two['is_agent'] = array('gt',1);
			$nowdate = strtotime(date('c'));
			$nowday=strtotime(date('Y-m-d'));
			$nowmonth = date('m');
			$fck->emptyTime();
	
			foreach($vo as $voo){
				$rs = $fck->where($where_two)->field($field_two)->find();  //找出登录会员(必须为区域代理并且已经登录)
				if (!$rs){
					$this->error('会员错误！');
					exit;
				}
				$ppath=$voo['p_path'];
				//上级未开通不能开通下级员工
				$frs_where['is_pay'] = array('eq',0);
				$frs_where['id'] = $voo['father_id'];
				$frs = $fck -> where($frs_where) -> find();
				if($frs){
					$this->error('开通失败，上级未开通');
					exit;
				}
// 				if($reg_money==1){
// 					$us_money = $rs['agent_kt'];
// 					$money_a = $voo['cpzj'];
// 				}elseif($reg_money==2){
// 					$us_money = $rs['agent_xf'];
// 					$money_a = $voo['cpzj'];
// 				}else{
					$us_money = $rs['agent_cash'];
					$money_a = $voo['cpzj'];
					$pre_ulevel = $voo['u_level'];
// 				}
				if ($us_money < $money_a){
					$bUrl = __URL__.'/menber';
					$this->_box(0,'注册币余额不足！',$bUrl,1);
					exit;
				}
// 				if($reg_money==1){
// 					$result = $fck->execute("update __TABLE__ set `agent_kt`=agent_kt-".$money_a." where `id`=".$ID);
// 				}elseif($reg_money==2){
// 					$result = $fck->execute("update __TABLE__ set `agent_xf`=agent_xf-".$money_a." where `id`=".$ID);
// 				}else{
					$result = $fck->execute("update __TABLE__ set `agent_cash`=agent_cash-".$money_a." where `id`=".$ID);
// 				}
				if($result){
					if($reg_money==1){
						$kt_cont = "注册币开通会员";
					}elseif($reg_money==2){
						$kt_cont = "一进一出币开通会员";
					}else{
						$kt_cont = "注册币开通会员";
					}
					$fck->addencAdd($rs['id'], $voo['user_id'], -$money_a, 19,0,0,0,$kt_cont);//历史记录
					
					//给推荐人添加推荐人数或单数
					$fck->query("update __TABLE__ set `re_nums`=re_nums+1,re_f4=re_f4+".$voo['f4']." where `id`=".$voo['re_id']);
					
					$nnrs = $fck->where('is_pay>0')->field('n_pai')->order('n_pai desc')->find();
					$mynpai = ((int)$nnrs['n_pai'])+1;
					
// 					//接点人信息
// 					$arry = array();
// 					$arry = $this->gongpaixtsmall($voo['re_id']);
// 					$father_id      = $arry['father_id'];
// 					$father_name = $arry['father_name'];
// 					$TreePlace     = $arry['treeplace'];
// 					$p_level        = $arry['p_level'];
// 					$p_path        = $arry['p_path'];
// 					$u_pai          = $arry['u_pai'];
					
					$in_gp = $s4[$voo['u_level']-1];
					$data = array();
					$data['is_pay'] = 1;
					$data['pdt'] = $nowdate;
					$data['open'] = 0;
					$data['get_date'] = $nowday;
					$data['fanli_time'] = $nowday;//当天没有分红奖
					$data['agent_lock'] = $in_gp;//
					$data['gp_num'] = $in_gp;//
					$data['n_pai'] = $mynpai;
					$data['is_zy']  = $voo['id'];
					if($pre_ulevel==5){
					$data['adt']  = $nowdate;
					$data['is_agent'] = 2;
					}
					
// 					$data['father_id'] = $father_id;
// 					$data['father_name'] = $father_name;
// 					$data['treeplace'] = $TreePlace;
// 					$data['p_level'] = $p_level;
// 					$data['p_path'] = $p_path;
// 					$data['u_pai'] = $u_pai;
					
					//开通会员
					$result = $fck->where('id='.$voo['id'])->save($data);
					unset($data,$varray);
					
					$data = array();
					$data['uid'] = $voo['id'];
					$data['user_id'] = $voo['user_id'];
					$data['in_money'] = $voo['cpzj'];
					$data['in_time'] = time();
					$data['in_bz'] = "新会员加入";
					$shouru->add($data);
					unset($data);
					
					//统计单数
					$fck->xiangJiao($voo['id'], $voo['f4']);
					
					//算出奖金
					$fck->getusjj($voo['id'],1);
				}
				
//				//全部奖金结算
//				$this->_clearing();

			}
			unset($fck,$where,$where_two,$rs);
			if ($vo){
				unset($vo);
				$bUrl = __URL__.'/menber';
				$this->_box(1,'开通会员成功！',$bUrl,2);
				exit;
			}else{
				unset($vo);
				$bUrl = __URL__.'/menber';
				$this->_box(0,'开通会员失败！',$bUrl,1);
				exit;
			}
		}else{
			$this->error('错误！');
			exit;
		}
	}
	
	private function _menberDelUse($OpID=0){
		//=========================================删除会员
		if ($_SESSION['Urlszpass'] == 'MyssShuiPuTao'){
			$fck = M ('fck');
			$where['is_pay'] = 0;
			foreach($OpID as $voo){
				$rs = $fck -> find($voo);
				if($rs){
					$whe['father_name'] = $rs['user_id'];
					$rss = $fck -> where($whe)->field('id') -> find();
					if($rss){
						$bUrl = __URL__.'/menber';
						$this -> error('该 '. $rs['user_id'] .' 会员有下级会员，不能删除！');
						exit;
					}else{
						$where['id'] = $voo;
						$fck -> where($where) -> delete();
					}
				}else{
					$this->error('错误!');
				}
			}
			$bUrl = __URL__.'/menber';
			$this->_box(1,'删除会员！',$bUrl,1);
			exit;
		}else{
			$this->error('错误!');
		}
	}
	
	//已开通会员
	public function frontMenber($Urlsz=0){
		//列表过滤器，生成查询Map对象
		if ($_SESSION['Urlszpass'] == 'MyssDaShuiPuTao'){
			$fck = M('fck');
			$id = $_SESSION[C('USER_AUTH_KEY')];
			$map = array();
			$map['open'] = $id;
			$map['is_pay'] = array('gt',0);
			$UserID = $_POST['UserID'];
			if (!empty($UserID)){
				import ( "@.ORG.KuoZhan" );  //导入扩展类
				$KuoZhan = new KuoZhan();
				if ($KuoZhan->is_utf8($UserID) == false){
					$UserID = iconv('GB2312','UTF-8',$UserID);
				}
				unset($KuoZhan);
				$where['nickname'] = array('like',"%".$UserID."%");
				$where['user_id'] = array('like',"%".$UserID."%");
				$where['_logic']    = 'or';
				$map['_complex']    = $where;
				$UserID = urlencode($UserID);
			}
	
			//查询字段
			$field  = "*";
			//=====================分页开始==============================================
			import ( "@.ORG.ZQPage" );  //导入分页类
			$count = $fck->where($map)->count();//总页数
			$listrows = C('ONE_PAGE_RE');//每页显示的记录数
			$page_where = 'UserID='.$UserID;//分页条件
			$Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where);
			//===============(总页数,每页显示记录数,css样式 0-9)
			$show = $Page->show();//分页变量
			$this->assign('page',$show);//分页变量输出到模板
			$list = $fck->where($map)->field($field)->order('pdt desc')->page($Page->getPage().','.$listrows)->select();
	
			$HYJJ = '';
			$this->_levelConfirm($HYJJ,1);
			$this->assign('voo',$HYJJ);//会员级别
			$this->assign('list',$list);//数据输出到模板
			//=================================================
	
			$this->display ('frontMenber');
			exit;
		}else{
			$this->error('数据错误2!');
			exit;
		}
	}
	
	
	public function adminAgents(){
		//=====================================后台区域代理管理
		$this->_Admin_checkUser();
		if ($_SESSION['UrlPTPass'] == 'MyssGuanXiGua'){
			$fck = M('fck');
			$UserID = $_POST['UserID'];
			if (!empty($UserID)){
				import ( "@.ORG.KuoZhan" );  //导入扩展类
                $KuoZhan = new KuoZhan();
                if ($KuoZhan->is_utf8($UserID) == false){
                    $UserID = iconv('GB2312','UTF-8',$UserID);
                }
                unset($KuoZhan);
				$where['nickname'] = array('like',"%".$UserID."%");
				$where['user_id'] = array('like',"%".$UserID."%");
				$where['_logic']    = 'or';
				$map['_complex']    = $where;
				$UserID = urlencode($UserID);
			}
			//$map['is_del'] = array('eq',0);
			$map['is_agent'] = array('gt',0);
			if (method_exists ( $this, '_filter' )) {
				$this->_filter ( $map );
			}
            $field  = '*';
            //=====================分页开始==============================================
            import ( "@.ORG.ZQPage" );  //导入分页类
            $count = $fck->where($map)->count();//总页数
       		$listrows = C('ONE_PAGE_RE');//每页显示的记录数
            $page_where = 'UserID=' . $UserID;//分页条件
            $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where);
            //===============(总页数,每页显示记录数,css样式 0-9)
            $show = $Page->show();//分页变量
            $this->assign('page',$show);//分页变量输出到模板
            $list = $fck->where($map)->field($field)->order('id desc')->page($Page->getPage().','.$listrows)->select();
            $this->assign('list',$list);//数据输出到模板
            //=================================================
            
            $Agent_Us_Name = C('Agent_Us_Name');
			$Aname = explode("|",$Agent_Us_Name);
			$this->assign ( 'Aname', $Aname);

			$this->display ('adminAgents');
			return;
		}else{
			$this->error('数据错误!');
			exit;
		}
	}
	
	public function adminAgentsShow(){
		//查看详细信息
		if ($_SESSION['UrlPTPass'] == 'MyssGuanXiGua'){
			$fck = M('fck');
			$ID = (int) $_GET['Sid'];
			$where = array();
			$where['id'] = $ID;
			$srs = $fck->where($where)->field('user_id,verify')->find();
			$this->assign('srs',$srs);
			unset($fck,$where,$srs);
			$this->display ('adminAgentsShow');
			return;
		}else{
			$this->error('数据错误!');
			exit;
		}
	}
	
	public function adminAgentsAC(){  //审核区域代理(区域代理)申请
		$this->_Admin_checkUser();
		//处理提交按钮
		$action = $_POST['action'];
		//获取复选框的值
		$XGid = $_POST['tabledb'];
		$fck = M ('fck');
//	    if (!$fck->autoCheckToken($_POST)){
//            $this->error('页面过期，请刷新页面！');
//            exit;
//        }
        unset($fck);
		if (!isset($XGid) || empty($XGid)){
			$bUrl = __URL__.'/adminAgents';
			$this->_box(0,'请选择会员！',$bUrl,1);
			exit;
		}
		switch ($action){
			case '确认';
				$this->_adminAgentsConfirm($XGid);
				break;
			case '删除';
				$this->_adminAgentsDel($XGid);
				break;
		default;
			$bUrl = __URL__.'/adminAgents';
			$this->_box(0,'没有该会员！',$bUrl,1);
			break;
		}
	}
	
	
	private function _adminAgentsConfirm($XGid=0){
		//==========================================确认申请区域代理
		if ($_SESSION['UrlPTPass'] == 'MyssGuanXiGua'){
			$fck  = D ('Fck');
			$where['id'] = array ('in',$XGid);
			$where['is_agent'] = 1;
			$rs = $fck->where($where)->field('*')->select();

			$data = array();
			$history = M ('history');
            $rewhere = array();
//          $nowdate = strtotime(date('c'));
            $nowdate = time();
            $jiesuan = 0;
			foreach($rs as $rss){

				$myreid = $rss['re_id'];
				$shoplx = $rss['shoplx'];

				$data['user_id'] = $rss['user_id'];
				$data['uid'] = $rss['uid'];
				$data['action_type'] = '申请成为区域代理';
				$data['pdt'] = $nowdate;
				$data['epoints'] = $rss['agent_no'];
				$data['bz'] = '申请成为区域代理';
				$data['did'] = 0;
				$data['allp'] = 0;
				$history ->add($data);

				$fck ->query("UPDATE __TABLE__ SET is_agent=2,adt=$nowdate,agent_max=0 where id=".$rss['id']);  //开通
			}
			unset($fck,$where,$rs,$history,$data,$rewhere);
			$bUrl = __URL__.'/adminAgents';
			$this->_box(1,'确认申请！',$bUrl,1);
			exit;
		}else{
			$this->error('错误！');
			exit;
		}
	}
	public function adminAgentsCoirmAC(){
		if ($_SESSION['UrlPTPass'] == 'MyssGuanXiGua'){
			//$this->_checkUser();
			$fck = M ('fck');
			$content  = $_POST['content'];
			$userid =trim($_POST['userid']);
			$where['user_id']=$userid;
			//$rs=$fck->where($where)->find();
			$fck_rs = $fck->where($where)->field('id,is_agent,is_pay,user_id,user_name,agent_max,is_agent')->find();
				
	
			if($fck_rs){
				if($fck_rs['is_pay']  == 0){
					$this->error ('临时代理商不能授权区域代理!');
					exit;
				}
				if($fck_rs['is_agent']  == 1){
					$this->error('上次申请还没通过审核!');
					exit;
				}
				if($fck_rs['is_agent']  == 2){
					$this->error('该代理商已是区域代理!');
					exit;
				}
				if(empty($content)){
					$this->error ('请输入备注!');
					exit;
				}
					
				if($fck_rs['is_agent'] == 0){
					$nowdate = time();
					$result = $fck -> query("update __TABLE__ set verify='".$content."',is_agent=2,idt=$nowdate,adt={$nowdate} where id=".$fck_rs['id']);
				}
	
				$bUrl = __URL__ .'/adminAgents';
				$this->_box(1,'授权成功！',$bUrl,2);
			}else{
				$this->error('会员不存在！');
				exit;
			}
		}else{
			$this->error('错误！');
			exit;
		}
	
	}
	private function _adminAgentsDel($XGid=0){
		//=======================================删除申请区域代理信息
		if ($_SESSION['UrlPTPass'] == 'MyssGuanXiGua'){
			$fck = M ('fck');
			$rewhere = array();
			$where['is_agent'] = array('gt',0);
			$where['id'] = array ('in',$XGid);
			$rs = $fck -> where($where) -> select();
			foreach ($rs as $rss){
				$fck ->query("UPDATE __TABLE__ SET is_agent=0,idt=0,adt=0,new_agent=0,shoplx=0,shop_a='',shop_b='' where id>1 and id = ".$rss['id']);
			}
	
			//			$shop->where($where)->delete();
			unset($fck,$where,$rs,$rewhere);
			$bUrl = __URL__.'/adminAgents';
			$this->_box('操作成功','删除申请！',$bUrl,1);
			exit;
		}else{
			$this->error('错误!');
			exit;
		}
	}
	//区域代理表
	public function financeDaoChu_BD(){
		$this->_Admin_checkUser();
		//导出excel
		set_time_limit(0);
	
		header("Content-Type:   application/vnd.ms-excel");
		header("Content-Disposition:   attachment;   filename=Member-Agent.xls");
		header("Pragma:   no-cache");
		header("Content-Type:text/html; charset=utf-8");
		header("Expires:   0");
	
	
	
		$fck = M ('fck');  //奖金表
	
		$map = array();
		$map['id'] = array('gt',0);
		$map['is_agent'] = array('gt',0);
		$field   = '*';
		$list = $fck->where($map)->field($field)->order('idt asc,adt asc')->select();
	
		$title   =   "区域代理表 导出时间:".date("Y-m-d   H:i:s");
	
		echo   '<table   border="1"   cellspacing="2"   cellpadding="2"   width="50%"   align="center">';
		//   输出标题
		echo   '<tr   bgcolor="#cccccc"><td   colspan="9"   align="center">'   .   $title   .   '</td></tr>';
		//   输出字段名
		echo   '<tr  align=center>';
		echo   "<td>序号</td>";
		echo   "<td>会员编号</td>";
		echo   "<td>姓名</td>";
		echo   "<td>联系电话</td>";
		echo   "<td>申请时间</td>";
		echo   "<td>确认时间</td>";
		echo   "<td>类型</td>";
		echo   "<td>区域代理区域</td>";
		echo   "<td>剩余注册币</td>";
		echo   '</tr>';
		//   输出内容
	
		//		dump($list);exit;
	
		$i = 0;
		foreach($list as $row)   {
			$i++;
			$num = strlen($i);
			if ($num == 1){
				$num = '000'.$i;
			}elseif ($num == 2){
				$num = '00'.$i;
			}elseif ($num == 3){
				$num = '0'.$i;
			}else{
				$num = $i;
			}
			if($row['shoplx']==1){
				$nnn = '区域代理';
			}elseif($row['shoplx']==2){
				$nnn = '县/区代理商';
			}else{
				$nnn = '市级代理商';
			}
	
	
			echo   '<tr align=center>';
			echo   '<td>'   .  chr(28).$num   .   '</td>';
			echo   "<td>"   .   $row['user_id'].  "</td>";
			echo   "<td>"   .   $row['user_name'].  "</td>";
			echo   "<td>"   .   $row['user_tel'].  "</td>";
			echo   "<td>"   .   date("Y-m-d H:i:s",$row['idt']).  "</td>";
			echo   "<td>"   .   date("Y-m-d H:i:s",$row['adt']).  "</td>";
			echo   "<td>"   .   $nnn.  "</td>";
			echo   "<td>"   .   $row['shop_a'].  " / " . $row['shop_b']  .   "</td>";
			echo   "<td>"   .   $row['agent_cash'].  "</td>";
			echo   '</tr>';
		}
		echo   '</table>';
	}
	
}
?>