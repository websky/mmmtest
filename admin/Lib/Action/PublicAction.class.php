<?php
class PublicAction extends CommonAction {
	public function _initialize() {
		header("Content-Type:text/html; charset=utf-8");
		$this->_inject_check(1);//调用过滤函数
		$this->_Config_name();//调用参数
	}

	//过滤查询字段
	function _filter(&$map){
		$map['title'] = array('like',"%".$_POST['name']."%");
	}
	// 顶部页面
	public function top() {
		C('SHOW_RUN_TIME',false);			// 运行时间显示
		C('SHOW_PAGE_TRACE',false);
		$this->display();
	}
	// 尾部页面
	public function footer() {
		C('SHOW_RUN_TIME',false);			// 运行时间显示
		C('SHOW_PAGE_TRACE',false);
		$this->display();
	}
	// 菜单页面
	public function menu() {
        $this->_checkUser();
		$map = array();
		$id = $_SESSION[C('USER_AUTH_KEY')];
		$field = '*';

		$map = array();
		$map['s_uid']   = $id;   //会员ID
		$map['s_read'] = 0;     // 0 为未读
        $info_count = M ('msg') -> where($map) -> count(); //总记录数
		$this -> assign('info_count',$info_count);

		$fck = M('fck');
		$fwhere = array();
		$fwhere['ID'] = $_SESSION[C('USER_AUTH_KEY')];
		$frs = $fck->where($fwhere)->field('*')->find();
		//dump($frs);
		$HYJJ = '';
		$this->_levelConfirm($HYJJ,1);
		$this->assign('voo',$HYJJ);

		$this->assign('fck_rs',$frs);
		$this->display('menu');
	}

    // 后台首页 查看系统信息
    public function main() {
        $this->_checkUser();
		$ppfg = $_POST['ppfg'];
        $id = $_SESSION[C('USER_AUTH_KEY')];  //登录AutoId
		$fck = M('fck');
		$cash = M ('cash');
		$cashpp = M ('cashpp');
		$ebwhere= array();
		$ebwhere['uid'] = $id;
		$ebwhere['s_type'] = 0;
		$ebwhere['is_pay'] = 0;
        $ebfield  = '*';
		$f_list = $cash->where($ebwhere)->field($ebfield)->order('id desc')->limit(5)->select();
        $this->assign('f_list',$f_list);//数据输出到模板
		
		$ebwhere= array();
		$ebwhere['uid'] = $id;
		$ebwhere['s_type'] = 1;
		$ebwhere['is_pay'] = 0;
        $ebfield  = '*';
		$s_list = $cash->where($ebwhere)->field($ebfield)->order('id desc')->limit(5)->select();
        $this->assign('s_list',$s_list);//数据输出到模板
		
		
		$cwhere= array();
		$cwhere['uid']	   	= $id;
		$cwhere['is_pay']	= 0;
		
		$elist = $cashpp->where($cwhere)->field($ebfield)->order('id desc')->select();
		$this->assign('elist',$elist);//数据输出到模板
		
		foreach($elist as $elvo){
			$skid = $elvo['bid'];
			$sk_frs[$skid] = $fck -> where('id='.$skid)->find();
		}
		$this->assign('sk_frs',$sk_frs);
		//=================================================
		
		$fwhere= array();
		$fwhere['bid']	   = $id;
		$fwhere['is_pay']	= 0;
		
		$flist = $cashpp->where($fwhere)->field($ebfield)->order('id desc')->select();
		$this->assign('flist',$flist);//数据输出到模板
		
		foreach($flist as $flvo){
			$hkid = $flvo['uid'];
			$hk_frs[$hkid] = $fck -> where('id='.$hkid)->find();
		}
		$this->assign('hk_frs',$hk_frs);
		//=================================================
		
		

		$map = array();
		$map['s_uid']   = $id;   //会员ID
		$map['s_read'] = 0;     // 0 为未读
        $info_count = M ('msg') -> where($map) -> count(); //总记录数
		$this -> assign('info_count',$info_count);

		//会员级别
        $urs = $fck -> where('id='.$id)->field('*') -> find();
		$this -> assign('fck_rs',$urs);//总奖金

		$fee = M('fee');
	    $fee_rs = $fee->field('s3,s12,str1,str7,str9,str21,str22,str23')->find();
		$str21 = $fee_rs['str21'];
		$str22 = $fee_rs['str22'];
		$str23 = $fee_rs['str23'];
		$all_img = $str21."|".$str22."|".$str23;
		$this->assign('all_img',$all_img);
		$s3 = explode("|",$fee_rs['s3']);
		$s12 = $fee_rs['s12'];
		$str1 = $fee_rs['str1'];
		$str5 = explode("|",$fee_rs['str7']);
		$str9 = $fee_rs['str9'];
		$this->assign('s3',$s3);
		$this->assign('s12',$s12);
		$this->assign('str1',$str1);
		$this->assign('str9',$str9);
		
	    $maxqq = 4;
	    if(count($str5)>$maxqq){
	    	$lenn = $maxqq;
	    }else{
	    	$lenn = count($str5);
	    }
	    for($i=0;$i<$lenn;$i++){
	    	$qqlist[$i] = $str5[$i];
	    }
	    $this->assign('qlist',$qqlist);
	    
	    $HYJJ="";
		$this->_levelConfirm($HYJJ,1);
		$this->assign('voo',$HYJJ);//会员级别
		
		
		$see = $_SERVER['HTTP_HOST'].__APP__;
		$see = str_replace("//","/",$see);
        $this->assign ( 'server', $see );
        $this->display();
        
    }


    

	// 用户登录页面
	public function login() {
		$fee = M('fee');
		$fee_rs = $fee->field('str21')->find();
		$this->assign('fflv',$fee_rs['str21']);
		unset($fee,$fee_rs);
		$this->display('login');
	}

	public function index()
	{
		//如果通过认证跳转到首页
		redirect(__APP__);
	}

	// 用户登出
    public function LogOut(){
		$_SESSION = array();
		//unset($_SESSION);
        $this->assign('jumpUrl',__URL__.'/login/');
        $this->success('退出成功！');
    }

	// 登录检测
	public function checkLogin() {
		if(empty($_POST['account'])) {
			$this->error('请输入帐号！');
		}elseif (empty($_POST['password'])){
			$this->error('请输入密码！');
		}elseif (empty($_POST['verify'])){
			$this->error('请输入验证码！');
		}
		$fee = M ('fee');
//		$sel = (int) $_POST['radio'];
//		if($sel <=0 or $sel >=3){
//			$this->error('非法操作！');
//			exit;
//		}
//		if($sel != 1){
//			$this->error('暂时不支持英文版登录！');
//			exit;
//		}

        //生成认证条件
        $map            =   array();
		// 支持使用绑定帐号登录
		$map['user_id']	   = $_POST['account'];
//		$map['nickname'] = $_POST['account'];   //用户名也可以登录
//		$map['_logic']    = 'or';
		//$map['_complex']    = $where;
	    //$map["status"]	=	array('gt',0);
		if($_SESSION['verify'] != md5($_POST['verify'])) {
			$this->error('验证码错误！');
		}

		import ( '@.ORG.RBAC' );
		$fck = M('fck');
		$field = 'id,user_id,password,is_pay,is_lock,nickname,user_name,is_agent,user_type,last_login_time,login_count,is_boss,pwd1';
		$authInfo = $fck->where($map)->field($field)->find();
        //使用用户名、密码和状态的方式进行认证
        
		
		//通过账号查出注册时间
		$zhao=$_POST['account'];
		$zh=$fck->where('user_id='.$zhao)->find();
		 $sj=$zh['rdt'];
		//在注册时间上加上48小时
		$hou=date('Y-m-d H:i:s',$sj+2*24*60*60);
		 $xian=date('Y-m-d H:i:s');
		 
		
//如果当前时间大于注册48后的时间判断有没有提供帮助如果没有不能登录
		  $cash=M('cash');
	  if($xian>$hou){
		 $data1['user_id']=$zhao;
		 $data1['s_type']=0;
		 $tigong=$cash->where($data1)->count();
		  }
	//	if($tigong==0){
	//	echo '抱歉你注册成功后48小时未提供帮助系统已自动封号';
	//	exit;
	//	}
		
		
		
        if(false == $authInfo) {
            $this->error('帐号不存在或已禁用！');
        }else {
         if($authInfo['pwd1'] != $_POST['password']) {
				$this->error('密码错误！');
				exit;
            }
            
            if($_POST['lang'] == 1){
            	$this->error('英文版本暂时无法登陆，请选择中文版本！');
            	exit;
            }
			
			if($_POST['agent']==2 && $authInfo['is_agent']<$_POST['agent']){
				$this->error('您为非报单中心,请选择会员登录入口！');
				exit;
			}
            
			if ($authInfo['is_pay'] <1){
				$this->error('用户尚未开通，暂时不能登录系统！');
				exit;
			}
			if ($authInfo['is_lock']!=0){
				$this->error('用户已锁定，请与管理员联系！');
				exit;
			}
            $_SESSION[C('USER_AUTH_KEY')]	=	$authInfo['id'];
            $_SESSION['loginUseracc']		=	$authInfo['user_id'];//用户名
			$_SESSION['loginNickName']		=	$authInfo['nickname'];//会员名
			$_SESSION['loginUserName']		=	$authInfo['user_name'];//开户名
            $_SESSION['lastLoginTime']		=	$authInfo['last_login_time'];
			//$_SESSION['login_count']	    =	$authInfo['login_count'];
			$_SESSION['login_isAgent']	    =	$authInfo['is_agent'];//是否区域代理
			$_SESSION['UserMktimes']        = 	mktime();
            //身份确认 = 用户名+识别字符+密码
			$_SESSION['login_sf_list_u']    = md5($authInfo['user_id'].'wodetp_new_1012!@#'.$authInfo['password'].$_SERVER['HTTP_USER_AGENT']);

			//登录状态
			$user_type = md5($_SERVER['HTTP_USER_AGENT'].'wtp'.rand(0,999999));
			$_SESSION['login_user_type'] = $user_type;
			$where['id'] = $authInfo['id'];
			$fck->where($where)->setField('user_type',$user_type);
//			$fck->where($where)->setField('last_login_time',mktime());
			//管理员

			$parmd = $this->_cheakPrem();
			if($authInfo['id'] == 1||$parmd[11]==1) {
            	$_SESSION['administrator']		=	1;
            }else{
				$_SESSION['administrator']		=	2;
			}

//			//管理员
//			if($authInfo['is_boss'] == 1) {
//            	$_SESSION['administrator'] =	1;
//            }elseif($authInfo['is_boss'] == 2){
//            	$_SESSION['administrator'] = 3;
//            }elseif($authInfo['is_boss'] == 3){
//                $_SESSION['administrator']  = 4;
//            }elseif($authInfo['is_boss'] == 4){
//                $_SESSION['administrator'] = 5;
//            }elseif($authInfo['is_boss'] == 5){
//                $_SESSION['administrator'] =   6;
//            }elseif($authInfo['is_boss'] == 6){
//                $_SESSION['administrator'] =   7;
//            }else{
//				$_SESSION['administrator'] = 2;
//			}

			$fck->execute("update __TABLE__ set last_login_time=new_login_time,last_login_ip=new_login_ip,new_login_time=".time().",new_login_ip='".$_SERVER['REMOTE_ADDR']."' where id=".$authInfo['id']);

		
			
			// 缓存访问权限
            RBAC::saveAccessList();
			$this->success('登录成功！');
		}
	}
	//二级密码验证
	public function cody(){
		$UrlID = (int)$_GET['c_id'];
		if (empty($UrlID)){
			$this->error('二级密码错误!');
			exit;
		}
		if(!empty($_SESSION['user_pwd2'])){
			$url = __URL__."/codys/Urlsz/$UrlID";
			$this->_boxx($url);
			exit;
		}
		$fck   =  M ('cody');
        $list	=  $fck->where("c_id=$UrlID")->getField('c_id');
		if (!empty($list)){
			$this->assign('vo',$list);
			$this->display('cody');
			exit;
		}else{
			$this->error('二级密码错误!');
			exit;
		}
	}
	//二级验证后调转页面
	public function codys(){
		$Urlsz = $_POST['Urlsz'];
		if(empty($_SESSION['user_pwd2'])){
			$pass  = $_POST['oldpassword'];
			$fck   =  M ('fck');
		    if (!$fck->autoCheckToken($_POST)){
	            $this->error('页面过期请刷新页面!');
	            exit();
	        }
			if (empty($pass)){
				$this->error('二级密码错误!');
				exit();
			}

			$where =array();
			$where['id'] = $_SESSION[C('USER_AUTH_KEY')];
			$where['passopen'] = md5($pass);
			$list = $fck->where($where)->field('id')->find();
			if($list == false){
				$this->error('二级密码错误!');
				exit();
			}
			$_SESSION['user_pwd2'] = 1;
		}else{
			$Urlsz = $_GET['Urlsz'];
		}
		switch ($Urlsz){
			case 1:
				$_SESSION['DLTZURL02'] = 'updateUserInfo';
				$bUrl = __URL__.'/updateUserInfo';//修改资料
				$this->_boxx($bUrl);
				break;
			case 2:
				$_SESSION['DLTZURL01'] = 'password';
				$bUrl = __URL__.'/password';//修改密码
				$this->_boxx($bUrl);
				break;
			case 3:
				$_SESSION['DLTZURL01'] = 'pprofile';
				$bUrl = __URL__.'/pprofile';//修改密码
				$this->_boxx($bUrl);
				break;
			case 4:
				$_SESSION['DLTZURL01'] = 'OURNEWS';
				$bUrl = __URL__.'/News';//修改密码
				$this->_boxx($bUrl);
				break;
			default;
				$this->error('二级密码错误!');
				break;
		}
	}

	 public function verify()
    {
    	ob_clean();
		$type	 =	 isset($_GET['type'])?$_GET['type']:'gif';
        import("@.ORG.Image");
        Image::buildImageVerify();
    }

	

}
?>