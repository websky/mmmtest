<?php
class IndexAction extends CommonAction {
	// 框架首页
	public function index() {
		ob_clean();
		$this->_checkUser();
		$this->_Config_name();//调用参数
		C ( 'SHOW_RUN_TIME', false ); // 运行时间显示
		C ( 'SHOW_PAGE_TRACE', false );
		$fck = D ('Fck');

		$id = $_SESSION[C('USER_AUTH_KEY')];
		$field = '*';
		$fck_rs = $fck -> field($field) -> find($id);
		
		$reid = $fck_rs['re_id'];
		$rers = $fck->where('id='.$reid)->field('user_tel')->find();
		
		$webcount = $fck->where('id>0')->count();
		$this->assign('webcount',$webcount);
		
		$reusertel = $rers['user_tel'];
		$this->assign('reusertel',$reusertel);

		$HYJJ="";
		$this->_levelConfirm($HYJJ,1);
		$this->assign('voo',$HYJJ);//会员级别

		$this->assign('fck_rs',$fck_rs);

		$fck->emptyMonthTime();
		$fck->getLevel();

		$ydate = strtotime(date('Y-m-d'));//当天时间
		$end_date =$ydate + (24*3600);//当天结束时间

		$fee_rs = M('fee')->field('s2,i4,str29,str3')->find();
		$fee_i4 = $fee_rs['i4'];
		$gg = $fee_rs['str29'];
		$b_money = $fee_rs['str3'];
		$this -> assign('gg',$gg);
		$this -> assign('b_money',$b_money);
		$this -> assign('fee_i4',$fee_i4);
		
		$map = array();
		$map['s_uid']   = $id;   //会员ID
		$map['s_read'] = 0;     // 0 为未读
        $info_count = M ('msg') -> where($map) -> count(); //总记录数
		$this -> assign('info_count',$info_count);

		$arss = $this->_cheakPrem();
        $this->assign('arss',$arss);
        
        $Guzhi = A("Guzhi");
        $Guzhi->stock_past_due();
        
		$fck -> mr_fenhong(1);
		$this->aotu_clearings();
		

		
		$ydate = strtotime(date('Y-m-d'));//当天时间
		$end_date =$ydate + (30*24*3600);//当天结束时间
		
		
		
		
		//投资奖
		 $touzi=M('Touzi');
     $cash=M('Cash');
    $bshty = M ('bonushistory');
		
		$beginThismonth=mktime(0,0,0,date('m'),1,date('Y'));                //获取每个月月初时间
        $endThismonth=mktime(23,59,59,date('m'),date('t'),date('Y'));      // 获取每个月未时间
	    $now=date('d');      //今天 时间
	
      $time =  date('d',$endThismonth); 
     
   $shop = $touzi->where('user_id='.$id)->field('pay')->find();
 
  
if($now==1){

    $touzi->where('user_id='.$id)->delete();

// $touzi->execute("update __TABLE__ set  pay='null' where user_id=".$id);

}

//调用后台设置的投资奖

         $fee = M('fee');
    	$fee_rs = $fee->field('s23')->find(1);
    	$s23 = explode("|",$fee_rs['s23']);
    	

  
if(empty($shop)){

 if($now==$time){         //当今天时间等于月未时间
   
     $data2['uid']=$id;
     $data2['s_type']=0;
     
     $shop = $cash->where($data2)->sum('money');
     

  if($shop>30000 && $shop<50000){

     $data['user_id']=$id;
     $data['name']='投资奖';
     $data['money']=$s23[0];
     $data['time']=time();
     $data['pay']=1;
     $touzi->Add($data);
     
      $data3['uid']=$id;
     $data3['pdt']=time();
     $data3['prep']=$s23[0];
     $data3['allp']=$s23[0];
     $data3['bz']='投资奖';
     $data3['type']=1;
     $bshty->add($data3);
	 $fck->execute("update __TABLE__ set touzi=touzi+".$s23[0]." where id=".$id);
     
     }
     
  elseif($shop>50000 && $shop<80000){
     $data['user_id']=$id;
     $data['name']='投资奖';
     $data['money']=$s23[1];
     $data['time']=time();
     $touzi->Add($data);
     
     $data3['uid']=$id;
     $data3['pdt']=time();
     $data3['prep']=$s23[1];
     $data3['allp']=$s23[1];
     $data3['bz']='投资奖';
     $data3['type']=1;
     $bshty->add($data3);
    
	 $fck->execute("update __TABLE__ set touzi=touzi+".$s23[1]." where id=".$id);
     
      
      }
      
 elseif($shop>80000){
      
     $data['user_id']=$id;
     $data['name']='投资奖';
     $data['money']=$s23[2];
     $data['time']=time();
     $touzi->Add($data);
     
     $data3['uid']=$id;
     $data3['pdt']=time();
     $data3['prep']=$s23[2];
     $data3['allp']=$s23[2];
     $data3['bz']='投资奖';
     $data3['type']=1;
     $bshty->add($data3);
   
	$fck->execute("update __TABLE__ set touzi=touzi+".$s23[2]." where id=".$id);
     
      }
      
     }
  
}

		$this->display('index');
	}

    //每日自动结算
	public function aotu_clearings(){
		$fck = D ('Fck');
		$fee = M ('fee');
		$nowday = strtotime(date('Y-m-d'));
		$nowweek = date("w");
		if($nowweek==0){
			$nowweek = 7;
		}
		$kou_w = $nowweek-1;
		$weekday = $nowday-$kou_w*24*3600;
		
		$now_dtime = strtotime(date("Y-m-d"));
		if(empty($_SESSION['auto_cl_ok'])||$_SESSION['auto_cl_ok']!=$now_dtime){
			$js_c = $fee->where('id=1 and f_time<'.$weekday)->count();
			if($js_c>0){
				//经理分红
				$fck->jl_fenghong();
			}
			$_SESSION['auto_cl_ok'] = $now_dtime;
		}
		unset($fck,$fee);
	}

}
?>