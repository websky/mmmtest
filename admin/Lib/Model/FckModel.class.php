<?php
class FckModel extends CommonModel {
	//数据库名称

   public function xiangJiao($Pid=0,$DanShu=1){
        //========================================== 往上统计单数
        $where = array();
        $where['id'] = $Pid;
        $field = 'treeplace,father_id';
        $vo = $this ->where($where)->field($field)->find();
        if ($vo){
            $Fid = $vo['father_id'];
            $TPe = $vo['treeplace'];
            $table = $this->tablePrefix.'fck';
            if ($TPe == 0 && $Fid > 0){
                $this->execute("update ". $table ." Set `l`=l+$DanShu, `shangqi_l`=shangqi_l+$DanShu  where `id`=".$Fid);
            }elseif($TPe == 1 && $Fid > 0){
                $this->execute("update ". $table ." Set `r`=r+$DanShu, `shangqi_r`=shangqi_r+$DanShu  where `id`=".$Fid);
            }elseif($TPe == 2 && $Fid > 0){
                $this->execute("update ". $table ." Set `lr`=lr+$DanShu, `shangqi_lr`=shangqi_lr+$DanShu  where `id`=".$Fid);
            }
            if ($Fid > 0) $this->xiangJiao($Fid,$DanShu);
        }
        unset($where,$field,$vo);
    }
    
	
	
	public function shangjiaTJ($ppath,$treep=0){
		$where = "id in (0".$ppath."0)";
		$lirs = $this->where($where)->order('p_level desc')->field('id,treeplace')->select();
		foreach($lirs as $lrs){
			$myid = $lrs['id'];
			$mytp = $lrs['treeplace'];
			if($treep==0){
				$this->execute("update __TABLE__ Set `re_nums_l`=re_nums_l+1,`re_nums_b`=re_nums_b+1 where `id`=".$myid);
			}else{
				$this->execute("update __TABLE__ Set `re_nums_r`=re_nums_r+1,`re_nums_b`=re_nums_b+1 where `id`=".$myid);
			}
			$treep = $mytp;
		}
		unset($lirs,$lrs,$where);
    }

//	public function xiangJiao($Pid=0,$DanShu=1,$plv=0,$op=1){
//        //========================================== 往上统计单数【有层碰奖】
//
//        $peng = M ('peng');
//        $where = array();
//        $where['id'] = $Pid;
//        $field = 'treeplace,father_id,p_level';
//        $vo = $this ->where($where)->field($field)->find();
//        if ($vo){
//            $Fid = $vo['father_id'];
//            $TPe = $vo['treeplace'];
//            $table = $this->tablePrefix .'fck';
//			$dt	= strtotime(date("Y-m-d"));//现在的时间
//            if ($TPe == 0 && $Fid > 0){
//            	$p_rs = $peng ->where("uid=$Fid and ceng = $op") ->find();
//            	if($p_rs){
//            		$peng->execute("UPDATE __TABLE__ SET `l`=l+{$DanShu}  WHERE uid=$Fid and ceng = $op");
//            	}else{
//            		$peng->execute("INSERT INTO __TABLE__ (uid,ceng,l) VALUES ($Fid	,$op,$DanShu) ");
//            	}
//
//                $this->execute("UPDATE ". $table ." SET `l`=l+{$DanShu}, `benqi_l`=benqi_l+{$DanShu}  WHERE `id`=".$Fid);
//            }elseif($TPe == 1 && $Fid > 0){
//            	$p_rs = $peng ->where("uid=$Fid and ceng = $op") ->find();
//            	if($p_rs){
//            		$peng->execute("UPDATE __TABLE__ SET `r`=r+{$DanShu}  WHERE uid=$Fid and ceng = $op");
//            	}else{
//            		$peng->execute("INSERT INTO __TABLE__ (uid,ceng,r) VALUES ($Fid,$op,$DanShu) ");
//            	}
//                $this->execute("UPDATE ". $table ." SET `r`=r+{$DanShu}, `benqi_r`=benqi_r+{$DanShu}  WHERE `id`=".$Fid);
//            }
//            $op++;-+*
//            if ($Fid > 0) $this->xiangJiao($Fid,$DanShu,$plv,$op);
//        }
//        unset($where,$field,$vo);
//    }

    public function addencAdd($ID=0,$inUserID=0,$money=0,$name=null,$UID=0,$time=0,$acttime=0,$bz=""){
        //添加 到数据表
        if ($UID > 0) {
            $where = array();
            $where['id'] = $UID;
            $frs = $this->where($where)->field('nickname')->find();
            $name_two = $name;
            $name = $frs['nickname'] . ' 开通会员 ' . $inUserID ;
            $inUserID = $frs['nickname'];
        }else{
            $name_two = $name;
        }

        $data = array();
        $history = M ('history');

        $data['user_id']		= $inUserID;
        $data['uid']			= $ID;
        $data['action_type']	= $name;
        if($time >0){
        	$data['pdt']		= $time;
        }else{
        	$data['pdt']		= mktime();
        }
        $data['epoints']		= $money;
        if(!empty($bz)){
        	$data['bz']			= $bz;
        }else{
        	$data['bz']			= $name;
        }
        $data['did']			= 0;
        $data['type']			= 1;
        $data['allp']			= 0;
        if($acttime>0){
        	$data['act_pdt']	= $acttime;
        }
        $result = $history ->add($data);
        unset($data,$history);
    }

    public function huikuiAdd($ID=0,$tz=0,$zk,$money=0,$nowdate=null){
        //添加 到数据表

        $data                   = array();
        $huikui                = M ('huikui');
        $data['uid']            = $ID;
        $data['touzi']    = $tz;
        $data['zhuangkuang']            = $zk;
        $data['hk']        = $money;
        $data['time_hk']             = $nowdate;
        $huikui ->add($data);
        unset($data,$huikui);
    }

    //计算奖金
    public function getusjj($uid,$money,$type=0,$odid){
    	$mrs = $this->where('id='.$uid)->find();
    	if($mrs){
			$this->paiduisy($odid);
			$this->tuijj($mrs['re_id'],$mrs['user_id'],$money);

		   $this->jinglijiang($mrs['id'],$mrs['user_id'],$money);		
    		if($type==1){
    			//报单奖
    			$this->baodanfei($mrs['id'],$mrs['user_id'],$money);
    		}
    	}
		unset($mrs);
    }
    
    

    	
	//经理奖
    public function jinglijiang($ID=0,$inUserID=0,$money=0){
    	$fee = M('fee');
    	$fee_rs = $fee->field('s22')->find(1);
    	$s22 = explode("|",$fee_rs['s22']);
		$inid=$inUserID;
    	$where = array();
    	$where['id'] = $ID;
    	$where['is_fenh'] = array('eq',0);
		$field = 'id,user_id,re_path,cpzj,re_name';
    	$frs = $this->where($where)->field($field)->find();
    	
    	$shangjia=$this->where('user_id='.$frs['re_name'])->field('dengji,id,user_id')->find();
    		
   
           
    	
		if ($shangjia['dengji']=="见习经理"){
			$myid = $shangjia['id'];
            $myusid = $shangjia['user_id'];
			$prii = $s22[0]/100;	
			$money_count = bcmul($prii,$money,2);
			if($money_count>0){
				
				$this->addCashhistory($myid,$money_count,6,'经理奖',1);
				$this->execute("update __TABLE__ set jingli=jingli+".$money_count." where id=".$myid);
				$this->execute("update __TABLE__ set cpzj=0 where user_id=".$inid);
        		$this->rw_bonus($myid,$inUserID,2,$money_count);
			}
			
			
		}
		
		if ($shangjia['dengji']=="正式经理"){
			$myid = $shangjia['id'];
            $myusid = $shangjia['user_id'];
			$prii = $s22[1]/100;	
			$money_count = bcmul($prii,$money,2);
			if($money_count>0){
				
				$this->addCashhistory($myid,$money_count,6,'经理奖',1);
				$this->execute("update __TABLE__ set jingli=jingli+".$money_count." where id=".$myid);
				$this->execute("update __TABLE__ set cpzj=0 where user_id=".$inid);
        		$this->rw_bonus($myid,$inUserID,2,$money_count);
			}
			
			
		}
		if ($shangjia['dengji']=="市场总监"){
			$myid = $shangjia['id'];
            $myusid = $shangjia['user_id'];
			$prii = $s22[2]/100;	
			$money_count = bcmul($prii,$money,2);
			if($money_count>0){
				
				$this->addCashhistory($myid,$money_count,6,'经理奖',1);
				$this->execute("update __TABLE__ set jingli=jingli+".$money_count." where id=".$myid);
				$this->execute("update __TABLE__ set cpzj=0 where user_id=".$inid);
        		$this->rw_bonus($myid,$inUserID,2,$money_count);
			}
			
			
		}
		
		
		unset($fee,$fee_rs,$frs,$where);
    }
	
    
    
    
    
	
	//直推奖
    public function tuijj($ID=0,$inUserID=0,$money=0){
    	$fee = M('fee');
    	$bonushistory=M('bonushistory');
    	
    	$fee_rs = $fee->field('s6,s9,s26')->find(1);
    	$s6 = explode("|",$fee_rs['s6']);
    	$s9 = explode("|",$fee_rs['s9']);
    	$s26 = explode("|",$fee_rs['s26']);
		$inid=$inUserID;
    	$where = array();
    	$where['id'] = $ID;
    
    	
    	$where['is_fenh'] = array('eq',0);
		$field = 'id,user_id,re_path,cpzj';
    	$frs = $this->where($where)->field($field)->find();
    	
    	
    	
		if ($frs){
			$myid = $frs['id'];
            $myusid = $frs['user_id'];
           // $cashmoney = $frs['cpzj'];
           
         
           $beginThismonth=mktime(0,0,0,date('m'),1,date('Y'));                //获取每个月月初时间
          $endThismonth=mktime(23,59,59,date('m'),date('t'),date('Y'));      // 获取每个月未时间
        
           $data['uid']=$myid;
           $data['bz']='推荐奖';
           $data['pdt']=array('between',array(' $beginThismonth','$endThismonth'));
           
           $tui=$bunushistory->where($data)->count();
           
           if($tui==0){
           
           $prii = $s6[0]/100;	
           }
            
           if($tui==1){
           
           $prii = ($s6[0]+$s26[0])/100;	
           }
           
		  if($tui==2){
           
           $prii = ($s6[0]+($s26[0])*2)/100;	
           }
		  if($tui==3){
           
           $prii = ($s6[0]+($s26[0])*3)/100;	
           }
           
		 if($tui==4){
           
           $prii = ($s6[0]+($s26[0])*4)/100;	
           }
           
           if($tui>=5){
            $prii = ($s6[0]+($s26[0])*4)/100;	
           
           }
           
			
            

			
			$money_count = bcmul($prii,$money,2);
			if($money_count>0){
				
				$this->addCashhistory($myid,$money_count,6,'推荐奖',1);
				$this->execute("update __TABLE__ set agent_use=agent_use+".$money_count." where id=".$myid);
				$this->execute("update __TABLE__ set cpzj=0 where user_id=".$inid);
        		$this->rw_bonus($myid,$inUserID,2,$money_count);
			}
		}
		unset($fee,$fee_rs,$frs,$where);
    }
	
	
	//加入货币流向
    public function addCashhistory($ID=0,$money=0,$bnum=0,$bz="",$type=0,$did=0){
        $bonushistory=M('bonushistory');
        //找最近的当前的货币总额
        $fck_rs=$this->where("is_pay>0 and id=".$ID)->field('agent_cash,agent_use')->find();
        if($type==1){
            $old_money=$fck_rs['agent_use'];
        	$new_money=$old_money+$money;
        }else{
			$old_money=$fck_rs['agent_use'];
			$new_money=$old_money;
		}
		

        $data=array();
        $nowtime=time();
        $data['uid']=$ID;
        $data['pdt']=$nowtime;
        $data['bz']=$bz;
        $data['prep']=$old_money;
        $data['epoints']=$money;
        $data['allp']=$new_money;
        $data['action_type']=$bnum;
        $data['type']=$type;   //钱包类型
        $data['did']=$did;   //钱包类型
        $bonushistory->add($data);
    }
	
	//排队收益入帐
    public function paiduisy($oid){
    	$fee = M('fee');
    	$bshty = M ('bonushistory');
    	$fee_rs = $fee->field('s7,s15')->find(1);
    	$s15 = $fee_rs['s15'];
    	$s7 = explode("|",$fee_rs['s7']);
		$scc = count($s7);
		
        //查询所有由$oid产生的排队收益
    	$lirs = $bshty->where('type=0 and did='.$oid)->order('id asc')->select();
    	$i = 1;
    	foreach($lirs as $lrs){
    		$money_count = 0;
    		$cshid = $lrs['id'];
    		$myid = $lrs['uid'];
    		$epoints = $lrs['epoints'];
			
			$this->execute("update __TABLE__ set agent_use=agent_use+".$epoints.",agent_cash=agent_cash-".$epoints." where id=".$myid);
			
			$bshty->execute("update __TABLE__ set type=1 where type=0 and id=".$cshid);
			
    		$i++;
    	}
    	unset($lirs,$lrs);
    	unset($fee,$fee_rs);
   }
	
	//删除订单后删除相应的排队收益
    public function cancel_order_isout($oid){
    	$fee = M('fee');
    	$bshty = M ('bonushistory');
		
        //查询所有由$oid产生的排队收益
    	$lirs = $bshty->where('type=0 and did='.$oid)->order('id asc')->select();
    	$i = 1;
    	foreach($lirs as $lrs){
    		$money_count = 0;
    		$myid = $lrs['uid'];
    		$cshid = $lrs['id'];
    		$epoints = $lrs['epoints'];
			
			$this->execute("update __TABLE__ set agent_cash=agent_cash-".$epoints." where id=".$myid);
    		$i++;
    	}
    	unset($lirs,$lrs);
    	unset($fee,$fee_rs);
		
		$bshty->where('type=0 and did='.$oid)->delete();	
    }

    //升级
    public function getLevel(){
    	$fee = M('fee');
    	$fee_rs = $fee->field('s9')->find(1);
    	$s9 = explode("|",$fee_rs['s9']); //代数
    	
    	$this->execute("update __TABLE__ set u_level=2 where u_level<2 and cpzj>=".$s9[1]);
    	$this->execute("update __TABLE__ set u_level=3 where u_level<3 and cpzj>=".$s9[2]);
    	$this->execute("update __TABLE__ set u_level=4 where u_level<4 and cpzj>=".$s9[3]);
    	$this->execute("update __TABLE__ set u_level=5 where u_level<5 and cpzj>=".$s9[4]);
    	$this->execute("update __TABLE__ set u_level=6 where u_level<6 and cpzj>=".$s9[5]);
    }
    
   public function getReid($id){
   		$rs = $this->where('id='.$id)->field('id,re_nums,is_fenh')->find();
   		return array('re_id'=>$rs['id'],'re_nums'=>$rs['re_nums'],'is_fenh'=>$rs['is_fenh']); 
   }
    
	//劳务奖b3
    public function guanglij($repath,$inUserID=0,$u_level=0,$tplce){
  
    	$fee = M('fee');
    	$fee_rs = $fee->field('str9')->find(1);
    	$str9 = explode("|",$fee_rs['str9']); //代数
    
    	$lirs = $this->where('id in (0'.$repath.'0)')->field('id,u_level,treeplace,is_fenh')->order('p_level desc')->select();
    	
    	$i = 1;
		foreach($lirs as $lrs){
			$myid = $lrs['id'];		
			$is_fenh = $lrs['is_fenh'];		
			$sss = $u_level-1;
			$myccc = $str9[$sss];
			
			$money_count = $myccc;
			
			//echo($money_count."*".$tplce);
					
			//dump($money_count);
			
			if($money_count>0&&$is_fenh==0&&$tplce>0){
				$this->rw_bonus($myid,$inUserID,3,$money_count);
			}
			
			$tplce= $lrs['treeplace'];
			$i++;
		}
        unset($fee,$fee_rs,$s15,$lirs,$lrs);
    }
    

	
	//每日分红
    public function mr_fenhong($type=0){
        $nowtime = strtotime(date('Y-m-d'));
    	$fee = M('fee');
    	$cash = M ('cash');
    	$bshty = M ('bonushistory');
		
    	$fee_rs = $fee->field('s1,s4,s5,s15,f_time')->find(1);
    	$ca_rs=$cash->field('x1')->find(1);
    	$s=$ca_rs['x1'];
    	$s1 = $fee_rs['s1']/100;
        $s4 = $fee_rs['s4'];
        $s5 = $fee_rs['s5'];
        $s15 = $fee_rs['s15'];
        $f_time = $fee_rs['f_time'];
        if($f_time<$nowtime||$type==1){
            $result = $fee->execute("update __TABLE__ set f_time=".$nowtime." where id=1 and f_time=".$f_time);
            if($result||$type==1){
            	$where = "is_pay>=0 and s_type=0 and x1<".$s5." and ldt<".$nowtime;
            	$list = $cash->where($where)->field('*')->select();
            	
            	foreach ($list as $lrs) {
					$caid= $lrs['id'];
            		$myid = $lrs['uid'];
                    $money = $lrs['money'];
                    $myis_pay = $lrs['is_pay'];
                    $inUserID = $lrs['user_id'];
                    $d_day = $lrs['x1'];
					$all_g = $d_day+1;
					$is_sj=$lrs['is_sj'];
					
					$fck_rs=$this->where("id=".$myid)->find();
					
					$is_fenh=$fck_rs['is_fenh'];
					$re_path=$fck_rs['re_path'];
					$fuserid=$fck_rs['user_id'];
					$agent_use=$fck_rs['agent_use'];
					$re_id=$fck_rs['re_id'];
					
					if($is_sj==12){      //如果延时了12小时
					$money_count1 = ($money*$s1)*0.005;
					$money_count = ($money*$s1)-$money_count1;
					}
					if($is_sj==24){         //如果延时了24小时
					$money_count1 = ($money*$s1)*0.01;
					$money_count = ($money*$s1)-$money_count1;
					
					}
					if($is_sj==0){
					
					$money_count = $money*($s1+$s4*$s);
					}
            		                    
					
                    if($money_count>0){ 
                        $cash->execute("update __TABLE__ set ldt=".$nowtime.",x1=x1+1 where id=".$caid);					
						if($myis_pay==0){
							
							$this->addCashhistory($myid,$money_count,6,"利息",0,$caid);
							$this->execute("update __TABLE__ set agent_cash=agent_cash+".$money_count." where id=".$myid);
						}else{
							$this->addCashhistory($myid,$money_count,6,"利息",1);
							$this->execute("update __TABLE__ set agent_use=agent_use+".$money_count." where id=".$myid);
						}
						//区域奖
						//if($myis_pay==0){ 
						//	$this->rw_bonus($myid,$inUserID,1,$money_count,$caid);
						//	$this->lingdaojiang($re_path,$inUserID,$money_count,$caid);
					//}else{
						//	$this->rw_bonus($myid,$inUserID,1,$money_count);
						//	$this->lingdaojiang($re_path,$inUserID,$money_count);
						//}
                    }
            	}
        		unset($list,$lrs,$where);
            }
        }
    	unset($fee_rs);
    }
   
	//领导奖
    public function lingdaojiang($repath,$inUserID=0,$money=0,$orid=0){
    	$fee = M('fee');
    	$bshty = M ('bonushistory');
    	$fee_rs = $fee->field('s7,s15')->find(1);
    	$s15 = $fee_rs['s15'];
    	$s7 = explode("|",$fee_rs['s7']);
		$scc = count($s7);
		
        //给上5代
    	$lirs = $this->where('id in (0'.$repath.'0)')->field('id,is_fenh,user_id,agent_use')->order('re_level desc')->limit($scc)->select();
    	$i = 1;
    	foreach($lirs as $lrs){
    		$money_count = 0;
    		$myid = $lrs['id'];
    		$is_fenh = $lrs['is_fenh'];
    		$myUserID = $lrs['user_id'];
    		$agent_use = $lrs['agent_use'];
		
			$prii = $s7[$i-1]/100;
			$money_count = bcmul($prii,$money,2);
			
			
			
		        
    		if($money_count>0&&$is_fenh==0){
				
				$zz_content = '第'.$i.'代管理奖';
				if($orid==0){
					$this->addCashhistory($myid,$money_count,6,$zz_content,1);
					$this->execute("update __TABLE__ set agent_use=agent_use+".$money_count." where id=".$myid);
				}else{
					$this->addCashhistory($myid,$money_count,6,$zz_content,0,$orid);
					$this->execute("update __TABLE__ set agent_cash=agent_cash+".$money_count." where id=".$myid);
				}
				$this->rw_bonus($myid,$inUserID,3,$money_count,$orid);
    		}
    		$i++;
    		
    	}
    	unset($lirs,$lrs);
    	unset($fee,$fee_rs);
    }

    //报单费
    public function baodanfei($uid,$inUserID,$cpzj=0){
    	$bonus = M ('bonus');
		$fee = M('fee');
    	$fee_rs = $fee->field('s14')->find();
		$s14 = explode("|",$fee_rs['s14']);
		
		$frs = $this->where('id='.$uid.' and is_pay>0 and is_fenh=0')->field('id,user_id,bank_province,bank_city')->find();
		if($frs){
			$fsid = $frs['id'];
			$myusid = $frs['user_id'];
			$bankcity = $frs['bank_city'];
			$bankprovince = $frs['bank_province'];
			
			$qywhe1 = array();
			$qywhe1['shop_a'] = $bankprovince;
			$qywhe1['shoplx'] = 1;
			$qywhe1['is_agent'] = 2;
			$qywhe1['is_fenh'] = 0;
			$qyrs1 = $this -> where($qywhe1) -> find();
			if($qyrs1){
				$myid = $qyrs1['id'];
				
				$prii = $s14[0]/100;
				$money_count = bcmul($cpzj,$prii,2);
				
				if($money_count>0){
					$this->addCashhistory($myid,$money_count,6,'区域奖',1);
					$this->execute("update __TABLE__ set agent_use=agent_use+".$money_count." where id=".$myid);
					$this->rw_bonus($myid,$inUserID,3,$money_count);
				}
			}else{
				$prii = $s14[0]/100;
				$money_count = bcmul($cpzj,$prii,2);
				
				if($money_count>0){
					$this->addCashhistory(1,$money_count,6,'区域奖',1);
					$this->execute("update __TABLE__ set agent_use=agent_use+".$money_count." where id=1");
					$this->rw_bonus(1,$inUserID,3,$money_count);
				}
			}
			unset($qywhe1,$qyrs1);
			
			$qywhe2 = array();
			$qywhe2['shop_b'] = $bankcity;
			$qywhe2['shoplx'] = 2;
			$qywhe2['is_agent'] = 2;
			$qywhe2['is_fenh'] = 0;
			$qyrs2 = $this -> where($qywhe2) -> find();
			if($qyrs2){
				$myid = $qyrs2['id'];
				
				$prii = $s14[1]/100;
				$money_count = bcmul($cpzj,$prii,2);
				
				if($money_count>0){
					$this->addCashhistory($myid,$money_count,6,'区域奖',1);
					$this->execute("update __TABLE__ set agent_use=agent_use+".$money_count." where id=".$myid);
					$this->rw_bonus($myid,$inUserID,3,$money_count);
				}
			}else{
				$prii = $s14[1]/100;
				$money_count = bcmul($cpzj,$prii,2);
				
				if($money_count>0){
					$this->addCashhistory(1,$money_count,6,'区域奖',1);
					$this->execute("update __TABLE__ set agent_use=agent_use+".$money_count." where id=1");
					$this->rw_bonus(1,$inUserID,3,$money_count);
				}
			}
			unset($qywhe2,$qyrs2);
			
		}
	    unset($bonus,$fee,$fee_rs,$frs,$s14);
    }
    
    
    
    
    
    
    

	//销售奖
    public function jiandianjiang($ppath,$inUserID=0,$money){

        $fee = M('fee');
        $fee_rs = $fee->field('s5,s7')->find(1);
		$s5 = $fee_rs['s5']/100;
        $s7 = explode("|",$fee_rs['s7']);
        $scc = count($s7);
        $max_c = 0;
        for($i=0;$i<$scc;$i++){
            if($s5[$i]>$max_c){
                $max_c = $scc + 1;
            }
        }
    
        $lirs = $this->where('id in (0'.$ppath.'0)')->field('id,u_level,re_nums,xy_money,is_fenh')->order('p_level desc')->limit($max_c)->select();
        $i = 0;
        foreach($lirs as $lrs){
            $money_count = 0;
            $myid = $lrs['id'];
            $is_fenh = $lrs['is_fenh'];
            $re_nums = $lrs['re_nums'];
            $xy_money = $lrs['xy_money'];
            $myulv = $lrs['u_level'];
            $sssss = $myulv-1;
            $myccc = $s7[$myulv];
            $money_count = bcmul($s5, $money,2);
            if($money_count>0&&$is_fenh==0&&$i<$myccc){
                $this->rw_bonus($myid,$inUserID,3,$money_count);
            }
            $i++;
        }
        unset($fee,$fee_rs,$s15,$lirs,$lrs);
    }
	
    //层奖和对碰奖的日封顶
    public function zfd_jj($uid,$money=0){
    		$fee = M('fee');
    	    $fee_rs = $fee->field('str1')->find();
    		$str1 = explode("|",$fee_rs['str1']);//分红奖封顶
    		
    	
    		$rs = $this->where('id='.$uid)->field('u_level,day_feng')->find();
    		if($rs){
    			$day_feng = $rs['day_feng'];
    			$feng = $str1[$rs['u_level']-1];
    			if($money > $feng){
    				$money = $feng;
    			}
    	
    			if($day_feng >= $feng){
    				$money = 0;
    			}else{
    				$tt_money = $money + $day_feng;
    				if( $tt_money > $feng){
    					$money = $feng-$day_feng;
    				}
    			}
    		}
    	
    		return $money;
    	}
    	

    	

	//各种扣税
    public function rw_bonus($myid,$inUserID=0,$bnum=0,$money_count=0,$corid=0){
    	$fee = M('fee');
    	$fee_rs = $fee->field('s9,str4,str5,s5,s6')->find();
    	$s9 = explode('|',$fee_rs['s9']);
    
    	$s6 = $fee_rs['s6']/100; //重消奖
    	$str5 = $fee_rs['str5'];  //网络管理费
    	
    	$str4 = $fee_rs['str4']/100;  //进入慈善基金
    	$s5 = $fee_rs['s5'];  //进入励基金
    	

    	$money_ka = 0;
    	$money_kb = 0;
    	$money_kc = 0;
    	$money_kd = 0;
    	
    	$last_n = $money_count-$money_kc;//第一次剩余
		
    	$usqla = "";
		
		if($corid==0){
			$mrs = $this->where('id='.$myid.' and wlf=0')->field('id,user_id,wlf,wlf_money')->find();
			if($mrs){
				$uuid = $mrs['id'];
				$inUserID_did = $mrs['user_id'];
				$wlf = $mrs['wlf'];
				$wlf_money = $mrs['wlf_money'];
				$all_mm = $wlf_money+$last_n;
				$k_mon = 0;
				if($all_mm>=$str5){
					$k_mon = $str5-$wlf_money;
					$this->execute("UPDATE __TABLE__ SET wlf=".time()." where `id`=".$uuid." and wlf=0");
				}else{
					$k_mon = $last_n;
				}
				if($k_mon<0){
					$k_mon = 0;
				}
				if($k_mon>0){
					$money_kb = $k_mon;
					$usqla .= ",wlf_money=wlf_money+".$money_kb;
				}
			}
		}
    	unset($mrs);

		$last_m = $money_count-$money_ka-$money_kb-$money_kc;//剩余，此值写入现金账户
	
    	$bonus = M('bonus');
    	$bid = $this->_getTimeTableList($myid);
    	$inbb = "b".$bnum;
    	
     	if($bnum==4){
     		$usqla = ",agent_xf=agent_xf+".$money_count.""; 
     	}
    	
    	$usqlc = ""; //agent_cf重消奖
    	$bonus->execute("UPDATE __TABLE__ SET b0=b0+".$last_m.",".$inbb."=".$inbb."+".$money_count.",b5=b5-".$money_kb."  WHERE id={$bid}"); //加到记录表
    	$this->execute("update __TABLE__ set ".$usqlc.",zjj=zjj+".$money_count.",xy_money=xy_money+".$money_count.$usqla." where id=".$myid);//加到fck
		
    	unset($bonus);

    	if($money_count>0){
    		$this->addencAdd($myid,$inUserID,$money_count,$bnum);
    	}
		if($corid==0){
			if($money_kb>0){
				$this->addCashhistory($myid,-$money_kb,5,'管理费',1);
				$this->execute("update __TABLE__ set agent_use=agent_use-".$money_kb." where id=".$myid);
			}
		}
    	unset($fee,$fee_rs,$s9,$mrs);
    }
        
    //分红添加记录
    public function add_xf($one_prices=0,$cj_ss=0){
		$fenhong = M('fenhong');
		$data = array();
// 		$data['uid'] = 1;
// 		$data['user_id'] = $cj_ss;
		$data['f_num'] = $cj_ss;
		$data['f_money'] = $one_prices;
		$data['pdt'] = mktime();
		$fenhong->add($data);
		unset($fenhong,$data);
    }

	//日封顶
    public function ap_rifengding(){

    	$fee = M('fee');
    	$fee_rs = $fee->field('s7')->find();
    	$s7 = explode("|",$fee_rs['s7']);

    	$where=array();
    	$where['b8'] = array('gt',0);
    	$mrs=$this->where($where)->field('id,b8,day_feng,get_level')->select();
    	foreach($mrs as $vo){
    		$day_feng = $vo['day_feng'];
    		$ss = $vo['get_level'];
    		$bbb = $vo['b8'];
    		$fedd = $s7[$ss];//封顶
			$get_money = $bbb;
    		$all_money = $bbb+$day_feng;
    		$fdok = 0;
    		if($all_money>=$fedd){
    			$fdok = 1;
    			$get_money = $fedd-$day_feng;
    		}
    		if($get_money<0){
    			$get_money = 0;
    		}
    		if($get_money>=0){
    			$this->query("UPDATE __TABLE__ SET `b8`=".$get_money.",day_feng=day_feng+".$get_money." where `id`=".$vo['id']);
    		}
    		if($get_money>0){
    			if($fdok==1){
    				$this->query("UPDATE __TABLE__ SET x_num=x_num+1 where `id`=".$vo['id']);
    			}
    		}
    	}
    	unset($fee,$fee_rs,$s7,$where,$mrs);
    }

	//总封顶
    public function ap_zongfengding(){

    	$fee = M('fee');
    	$fee_rs = $fee->field('s15')->find();
    	$s15 = $fee_rs['s15'];

    	$where=array();
    	$where['b0'] = array('gt',0);
    	$where['_string'] = 'b0+zjj>'.$s15;
    	$mrs=$this->where($where)->field('id,b0,zjj')->select();
    	foreach($mrs as $vo){
    		$zjj = $vo['zjj'];
    		$bbb = $vo['b0'];
    		$get_money = $s15-$zjj;

    		if($get_money>0){
    			$this->query("UPDATE __TABLE__ SET `b0`=".$get_money." where `id`=".$vo['id']);
    		}
    	}
    	unset($mrs);
    }

	//奖金大汇总（包括扣税等）
    public function quanhuizong(){

    	$this->execute('UPDATE __TABLE__ SET `b0`=b1+b2+b3+b4+b5+b6+b7+b8');

    	$this->execute('UPDATE __TABLE__ SET `b0`=0,b1=0,b2=0,b3=0,b4=0,b5=0,b6=0,b7=0,b8=0,b9=0,b10=0 where is_fenh=1');

    }


    //清空时间
	public function emptyTime(){

		$nowdate = strtotime(date('Y-m-d'));

		$this->query("UPDATE `xt_fck` SET `day_feng`=0,_times=".$nowdate." WHERE _times !=".$nowdate."");

	}
	
	
	//清空月封顶
	public function emptyMonthTime(){  //zyq_date 记录当前月
	
		$nowmonth = date('m');
	
		$this->query("UPDATE `xt_fck` SET `re_money`=0,zyq_date=".$nowmonth." WHERE zyq_date !=".$nowmonth."");
	
	}

	public function gongpaixtsmall($uid){
		$fck = M ('fck');
		$mouid=$uid;
		$field = 'id,user_id,p_level,p_path,u_pai';
		$where = 'is_pay>0 and (p_path like "%,'.$mouid.',%" or id='.$mouid.')';
	
		$re_rs = $fck ->where($where)->order('p_level asc,u_pai asc')->field($field)->select();
		$fck_where = array();
		foreach($re_rs as $vo){
			$faid=$vo['id'];
			$fck_where['is_pay']   = array('egt',0);
			$fck_where['father_id']   = $faid;
			$count = $fck->where($fck_where)->count();
			if ( is_numeric($count) == false){
				$count = 0;
			}
			if ($count<2){
				$father_id=$vo['id'];
				$father_name=$vo['user_id'];
				$TreePlace=$count;
				$p_level=$vo['p_level']+1;
				$p_path=$vo['p_path'].$vo['id'].',';
				$u_pai=$vo['u_pai']*2+$TreePlace;
	
				$arry=array();
				$arry['father_id']=$father_id;
				$arry['father_name']=$father_name;
				$arry['treeplace']=$TreePlace;
				$arry['p_level']=$p_level;
				$arry['p_path']=$p_path;
				$arry['u_pai']=$u_pai;
				return $arry;
				break;
			}
		}
	}
    public function bobifengding(){

		$fee = M ('fee');
		$bonus = M ('bonus');
		$fee_rs = M ('fee') -> find();
    	$table = $this->tablePrefix .'fck';
    	$z_money = 0;//总支出
        $z_money = $this->where('is_pay = 1')->sum('b2');
        $times = M ('times');
        $trs = $times->order('id desc')->field('shangqi')->find();
        if ($trs){
            $benqi = $trs['shangqi'];
        }else{
            $benqi = strtotime(date('Y-m-d'));
        }
        $zsr_money = 0;//总收入
        $zsr_money = $this->where('pdt>='. $benqi .' and is_pay=1')->sum('cpzj');
        $bl = $z_money / $zsr_money ;
        $fbl = $fee_rs['s11'] / 100;
        if ($bl > $fbl){
            //$bl = $fbl;
            //$xbl = $bl - $fbl;
            $z_o1=$zsr_money*$fbl;
            $z_o2=$z_o1/$z_money;
            $this->query("UPDATE ". $table ." SET `b2`=b2*{$z_o2} where `is_pay`>=1 ");
        }



    }


	public  function _getTimeTableList($uid)
    {
    	$times = M ('times');
    	$bonus = M ('bonus');
    	$boid = 0;
    	$nowdate = strtotime(date('Y-m-d'))+3600*24-1;
    	$settime_two['benqi'] = $nowdate;
    	$settime_two['type']  = 0;
    	$trs = $times->where($settime_two)->find();
    	if (!$trs){
    		$rs3 = $times->where('type=0')->order('id desc')->find();
    		if ($rs3){
    			$data['shangqi']  = $rs3['benqi'];
    			$data['benqi']    = $nowdate;
    			$data['is_count'] = 0;
    			$data['type']     = 0;
    		}else{
    			$data['shangqi']  = strtotime('2010-01-01');
    			$data['benqi']    = $nowdate;
    			$data['is_count'] = 0;
    			$data['type']     = 0;
    		}
    		$shangqi = $data['shangqi'];
    		$benqi   = $data['benqi'];
    		unset($rs3);
    		$boid = $times->add($data);
    		unset($data);
    	}else{
    		$shangqi = $trs['shangqi'];
    		$benqi   = $trs['benqi'];
    		$boid = $trs['id'];
    	}
    	$_SESSION['BONUSDID'] = $boid;
    	$brs = $bonus->where("uid={$uid} AND did={$boid}")->find();
    	if ($brs){
    		$bid = $brs['id'];
    	}else{
    		$frs = $this->where("id={$uid}")->field('id,user_id')->find();
    		$data = array();
    		$data['did'] = $boid;
    		$data['uid'] = $frs['id'];
    		$data['user_id'] = $frs['user_id'];
    		$data['e_date'] = $benqi;
    		$data['s_date'] = $shangqi;
    		$bid = $bonus->add($data);
    	}
    	return $bid;
    }
	
	//判断进入B网
    public function pd_into_websiteb($uid){
		//$fck = D ('fck');
		$fck=new FckModel('fck');
    	$fck2 = M ('fck2');
    	$where = "is_pay>0 and is_lock=0 and is_bb>=0 and id=".$uid;
    	$lrs = $fck->where($where)->field('id,user_id,re_id,user_name,nickname,u_level')->find();
    	if($lrs){
    		$myid = $lrs['id'];
    		$result = $fck->execute("update __TABLE__ set is_bb=is_bb+1 where id=".$myid." and is_bb>=0");
    		if($result){
    			$data=array();
    			$data['fck_id'] = $lrs['id'];
    			$data['re_num'] = $lrs['re_id'];
    			$data['user_id'] = $lrs['user_id'];
    			$data['user_name'] = $lrs['user_name'];
    			$data['nickname'] = $lrs['nickname'];
    			$data['u_level'] = $lrs['u_level'];
    			$data['ceng'] = 0;
    
    			$farr = $fck->gongpaixt_Two_big_B();
    			$data['father_id']		= $farr['father_id'];
    			$data['father_name']	= $farr['father_name'];
    			$data['treeplace']		= $farr['treeplace'];
    			$data['p_level']		= $farr['p_level'];
    			$data['p_path']			= $farr['p_path'];
    			$data['u_pai']			= $farr['u_pai'];
    			$data['is_pay']			= 1;
    			$data['pdt']			= time();
    			$ress = $fck2->add($data);  // 添加
    			$ppath = $data['p_path'];
    			$inUserID = $data['user_id'];
    			$ulevel = $data['u_level'];
    			unset($data,$farr);
    			if($ress){
    				//b网见点
    				$fck->jiandianjiang_bb($ppath,$inUserID,$ulevel);
    			}
    		}
    	}
    	unset($fck2,$lrs,$where,$fck);
    }
	
}
?>