<?php
class CashAction extends CommonAction {

	public function _initialize() {
		header("Content-Type:text/html; charset=utf-8");
		$this->_Config_name();//调用参数
		$this->_checkUser();
	}

	public function cody(){
		//===================================二级验证
		$UrlID = (int) $_GET['c_id'];
		if (empty($UrlID)){
			$this->error('二级密码错误!');
			exit;
		}
		if(!empty($_SESSION['user_pwd2'])){
			$url = __URL__."/codys/Urlsz/$UrlID";
			$this->_boxx($url);
			exit;
		}
		$cody   =  M ('cody');
        $list	=  $cody->where("c_id=$UrlID")->field('c_id')->find();
		if ($list){
			$this->assign('vo',$list);
			$this->display('../Public/cody');
			exit;
		}else{
			$this->error('二级密码错误!');
			exit;
		}
	}

	public function codys(){
		//=============================二级验证后调转页面
		$Urlsz = (int) $_POST['Urlsz'];
		if(empty($_SESSION['user_pwd2'])){
			$pass  = $_POST['oldpassword'];
			$fck   =  M ('fck');
			if (!$fck->autoCheckToken($_POST)){
				$this->error('页面过期请刷新页面!');
	            exit();
			}
			if (empty($pass)){
				$this->error('二级密码错误!');
				exit();
			}

			$where = array();
			$where['id'] = $_SESSION[C('USER_AUTH_KEY')];
			$where['passopen'] = md5($pass);
			$list = $fck->where($where)->field('id,is_agent')->find();
			if($list == false){
				$this->error('二级密码错误!');
				exit();
			}
			$_SESSION['user_pwd2'] = 1;
		}else{
			$Urlsz = $_GET['Urlsz'];
		}
		switch ($Urlsz){
			case 1;
				$_SESSION['Urlszpass'] = 'Mysseb_sell';
				$bUrl = __URL__.'/eb_sell';
				$this->_boxx($bUrl);
				break;
			case 2;
				$_SESSION['Urlszpass'] = 'Mysseb_buy';
				$bUrl = __URL__.'/eb_buy';
				$this->_boxx($bUrl);
				break;
			case 3;
				$_SESSION['Urlszpass'] = 'Mysseb_list_b';
				$bUrl = __URL__.'/eb_list_b';
				$this->_boxx($bUrl);
				break;
			case 4;
				$_SESSION['Urlszpass'] = 'Mysseb_list';
				$bUrl = __URL__.'/eb_list';
				$this->_boxx($bUrl);
				break;
			case 5;
				$_SESSION['Urlszpass'] = 'Mysseb_history';
				$bUrl = __URL__.'/eb_history';
                $this->_boxx($bUrl);
				break;
			case 6;
                $_SESSION['UrlPTPass'] = 'MyssEBlist';
                $bUrl = __URL__.'/admin_eblist';//列表
                $this->_boxx($bUrl);
                break;
			case 7;
                $_SESSION['UrlPTPass'] = 'Mysshoudongcash';
                $bUrl = __URL__.'/adminCash';//列表
                $this->_boxx($bUrl);
                break;
			case 8;
                $_SESSION['UrlPTPass'] = 'Myshen';
                $bUrl = __URL__.'/eb_shen';//列表
                $this->_boxx($bUrl);
                break;
			
			default;
				$this->error('二级密码错误!');
				exit;
		}
	}
	
	  //本息钱包
		public function wallet(){  
		$id  = $_SESSION[C('USER_AUTH_KEY')];
		
			$fck = M ('fck');
				//查询是否是报单中心
		
		$baodan=$fck->where('id='.$id)->find();
		
		$bao=$baodan['baodanhao']; 
		
		
	
		
		$this->assign('bao',$bao);
		
		
		
		$bshty = M ('bonushistory');
        $u_all = $fck -> where('id='.$id)->field('*') -> find();
		
		$where= array();
		$where['uid'] = $id;
		$where['bz']=array('NEQ','经理奖');  
		$where['bz']=array('NEQ','投资奖'); 
        $where['bz']=array('NEQ','推荐奖'); 		
		$field  = '*';
		//=====================分页开始==============================================
		import ( "@.ORG.ZQPage" );  //导入分页类
		$count = $bshty->where($where)->count();//总页数
		$listrows = 10;//每页显示的记录数
		$Page = new ZQPage($count,$listrows,1);
		//===============(总页数,每页显示记录数,css样式 0-9)
		$show = $Page->show();//分页变量
		$this->assign('page',$show);//分页变量输出到模板
		$list = $bshty->where($where)->field($field)->order('id desc')->page($Page->getPage().','.$listrows)->select();
		$this->assign('list',$list);//数据输出到模板
		//=================================================
		
		$fee = M('fee');
		$fee_rs = $fee->field('s3,str1')->find();
		$s3 = explode('|',$fee_rs['s3']);
		$str1 = $fee_rs['str1'];
		
		$this -> assign('s3',$s3);//会员级别
		$this -> assign('str1',$str1);//会员级别
		$this -> assign('rs',$u_all);        
        $this->display ('wallet');
    }
	
	
	  public function shan(){
    
     $cid=$_GET['cid']; 
     $fck=M('fck');
     
     $chu=$fck->where('id='.$cid)->delete();
     if($chu){
     
     $this->success('删除成功');
     
     }
    
    }
	
	
    
    //经理钱包
	public function wallett(){  
		$id  = $_SESSION[C('USER_AUTH_KEY')];
		
		
		
		
		$fck = M ('fck');
		
		
			
				//查询是否是报单中心
		
		$baodan=$fck->where('id='.$id)->find();
		
		$bao=$baodan['baodanhao']; 
		
		
	
		
		$this->assign('bao',$bao);
		
		
		$bshty = M ('bonushistory');
        $u_all = $fck -> where('id='.$id)->field('*') -> find();
		
		$where= array();
		$where['uid'] = $id;
	
		$where['action_type']=array('EQ',66);  
	
		$field  = '*';
		//=====================分页开始==============================================
		import ( "@.ORG.ZQPage" );  //导入分页类
		$count = $bshty->where($where)->count();//总页数
		$listrows = 10;//每页显示的记录数
		$Page = new ZQPage($count,$listrows,1);
		//===============(总页数,每页显示记录数,css样式 0-9)
		$show = $Page->show();//分页变量
		$this->assign('page',$show);//分页变量输出到模板
		$list = $bshty->where($where)->field($field)->order('id desc')->page($Page->getPage().','.$listrows)->select();
		$this->assign('list',$list);//数据输出到模板
		//=================================================
		
		$fee = M('fee');
		$fee_rs = $fee->field('s3,str1')->find();
		$s3 = explode('|',$fee_rs['s3']);
		$str1 = $fee_rs['str1'];
		$this -> assign('s3',$s3);//会员级别
		$this -> assign('str1',$str1);//会员级别
		$this -> assign('rs',$u_all);        
        $this->display ('wallett');
    }
    
    
    //基金钱包
    
	public function wallettt(){  
		$id  = $_SESSION[C('USER_AUTH_KEY')];
		$fck = M ('fck');
		
				//查询是否是报单中心
		
		$baodan=$fck->where('id='.$id)->find();
		
		$bao=$baodan['baodanhao']; 
		
		
	
		
		$this->assign('bao',$bao);
		
		
		
		$bshty = M ('bonushistory');
        $u_all = $fck -> where('id='.$id)->field('*') -> find();
		
		$where= array();
		$where['uid'] = $id;  
		$where['bz']=array('EQ','投资奖');  
		$field  = '*';
		//=====================分页开始==============================================
		import ( "@.ORG.ZQPage" );  //导入分页类
		$count = $bshty->where($where)->count();//总页数
		$listrows = 10;//每页显示的记录数
		$Page = new ZQPage($count,$listrows,1);
		//===============(总页数,每页显示记录数,css样式 0-9)
		$show = $Page->show();//分页变量
		$this->assign('page',$show);//分页变量输出到模板
		$list = $bshty->where($where)->field($field)->order('id desc')->page($Page->getPage().','.$listrows)->select();
		$this->assign('list',$list);//数据输出到模板
		//=================================================
		
		$fee = M('fee');
		$fee_rs = $fee->field('s3,str1')->find();
		$s3 = explode('|',$fee_rs['s3']);
		$str1 = $fee_rs['str1'];
		$this -> assign('s3',$s3);//会员级别
		$this -> assign('str1',$str1);//会员级别
		$this -> assign('rs',$u_all);        
        $this->display ('wallettt');
    }
    
    public function tigong(){


// 	$where['uid']=$id;
// 	$where['s_type']=0;
//    $frss = $cash->where($where)->select();
//    $this->assign('tig',$frss);
 	
 	//dump($frss); exit;
 	

    
    
            $id = $_SESSION[C('USER_AUTH_KEY')];  //登录AutoId
		$fck = M('fck');
		
			
				//查询是否是报单中心
		
		$baodan=$fck->where('id='.$id)->find();
		
		$bao=$baodan['baodanhao']; 
		
		
	
		
		$this->assign('bao',$bao);
		
		
		
		
		$cash = M ('cash');
		$cashpp = M ('cashpp');
		$ebwhere= array();
		$ebwhere['uid'] = $id;
		$ebwhere['s_type'] = 0;
		$ebwhere['is_pay'] = 0;
        $ebfield  = '*';
		$f_list = $cash->where($ebwhere)->field($ebfield)->order('id desc')->select();
        $this->assign('tig',$f_list);//数据输出到模板
		
		$ebwhere= array();
		$ebwhere['uid'] = $id;
		$ebwhere['s_type'] = 1;
		$ebwhere['is_pay'] = 0;
        $ebfield  = '*';
		$s_list = $cash->where($ebwhere)->field($ebfield)->order('id desc')->limit(5)->select();
        $this->assign('s_list',$s_list);//数据输出到模板
		
		
		$cwhere= array();
		$cwhere['uid']	   	= $id;
		$cwhere['is_pay']	= 0;
		
		$elist = $cashpp->where($cwhere)->field($ebfield)->order('id desc')->select();
		$this->assign('elist',$elist);//数据输出到模板
	

 //$cashpp->getLastSql($elist);  
	//	dump($elist); exit;
		
		foreach($elist as $elvo){
			$skid = $elvo['bid'];
			$sk_frs[$skid] = $fck -> where('id='.$skid)->find();
		}
		$this->assign('sk_frs',$sk_frs);
		//=================================================
		
		
		
	
		//=================================================
		
		

		$map = array();
		$map['s_uid']   = $id;   //会员ID
		$map['s_read'] = 0;     // 0 为未读
        $info_count = M ('msg') -> where($map) -> count(); //总记录数
		$this -> assign('info_count',$info_count);

		//会员级别
        $urs = $fck -> where('id='.$id)->field('*') -> find();
		$this -> assign('fck_rs',$urs);//总奖金

		$fee = M('fee');
	    $fee_rs = $fee->field('s3,s12,str1,str7,str9,str21,str22,str23')->find();
		$str21 = $fee_rs['str21'];
		$str22 = $fee_rs['str22'];
		$str23 = $fee_rs['str23'];
		$all_img = $str21."|".$str22."|".$str23;
		$this->assign('all_img',$all_img);
		$s3 = explode("|",$fee_rs['s3']);
		$s12 = $fee_rs['s12'];
		$str1 = $fee_rs['str1'];
		$str5 = explode("|",$fee_rs['str7']);
		$str9 = $fee_rs['str9'];
		$this->assign('s3',$s3);
		$this->assign('s12',$s12);
		$this->assign('str1',$str1);
		$this->assign('str9',$str9);
		
	    $maxqq = 4;
	    if(count($str5)>$maxqq){
	    	$lenn = $maxqq;
	    }else{
	    	$lenn = count($str5);
	    }
	    for($i=0;$i<$lenn;$i++){
	    	$qqlist[$i] = $str5[$i];
	    }
	    $this->assign('qlist',$qqlist);
	    
	    $HYJJ="";
		$this->_levelConfirm($HYJJ,1);
		$this->assign('voo',$HYJJ);//会员级别
		
		
		$see = $_SERVER['HTTP_HOST'].__APP__;
		$see = str_replace("//","/",$see);
        $this->assign ( 'server', $see );

        $where= array();
		$where['uid'] = $id;
		$where['s_type']=array('EQ','0');  
		  
		$field  = '*';
		//=====================分页开始==============================================
		import ( "@.ORG.ZQPage" );  //导入分页类
		$count = $cash->where($where)->count();//总页数
		$listrows = 10;//每页显示的记录数
		$Page = new ZQPage($count,$listrows,1);
		//===============(总页数,每页显示记录数,css样式 0-9)
		$show = $Page->show();//分页变量
		$this->assign('page',$show);//分页变量输出到模板
		
    
    
 	
 	$this->display();
    }
    
  public function jieshou(){

// 	$id  = $_SESSION[C('USER_AUTH_KEY')];
// 	$cash=M('cash');
// 	$fck=M('fck');
// 	$where['uid']=$id;
// 	$where['s_type']=1;
//    $frss = $cash->where($where)->select();
//    $this->assign('tig',$frss);
// 	
 	//dump($frss); exit;

         $this->_checkUser();
		$ppfg = $_POST['ppfg'];
        $id = $_SESSION[C('USER_AUTH_KEY')];  //登录AutoId
		$fck = M('fck');
		
			
				//查询是否是报单中心
		
		$baodan=$fck->where('id='.$id)->find();
		
		$bao=$baodan['baodanhao']; 
		
		
	
		
		$this->assign('bao',$bao);
		
		
		
		$cash = M ('cash');
		$cashpp = M ('cashpp');
		$ebwhere= array();
		$ebwhere['uid'] = $id;
		$ebwhere['s_type'] = 1;
		$ebwhere['is_pay'] = 0;
        $ebfield  = '*';
		$f_list = $cash->where($ebwhere)->field($ebfield)->order('id desc')->select();
        $this->assign('tig',$f_list);//数据输出到模板
		
		$ebwhere= array();
		$ebwhere['uid'] = $id;
		$ebwhere['s_type'] = 1;
		$ebwhere['is_pay'] = 0;
        $ebfield  = '*';
		$s_list = $cash->where($ebwhere)->field($ebfield)->order('id desc')->select();
        $this->assign('s_list',$s_list);//数据输出到模板
		
		
		
		
		foreach($elist as $elvo){
			$skid = $elvo['bid'];
			$sk_frs[$skid] = $fck -> where('id='.$skid)->find();
		}
		$this->assign('sk_frs',$sk_frs);
		//=================================================
		
		$fwhere= array();
		$fwhere['bid']	   = $id;
		$fwhere['is_pay']	= 0;
		
		$flist = $cashpp->where($fwhere)->field($ebfield)->order('id desc')->select();
		$this->assign('flist',$flist);//数据输出到模板
		
		
		foreach($flist as $flvo){
			$hkid = $flvo['uid'];
			$hk_frs[$hkid] = $fck -> where('id='.$hkid)->find();
		}
		$this->assign('hk_frs',$hk_frs);
		//=================================================
		
		

		$map = array();
		$map['s_uid']   = $id;   //会员ID
		$map['s_read'] = 0;     // 0 为未读
        $info_count = M ('msg') -> where($map) -> count(); //总记录数
		$this -> assign('info_count',$info_count);

		//会员级别
        $urs = $fck -> where('id='.$id)->field('*') -> find();
		$this -> assign('fck_rs',$urs);//总奖金

		$fee = M('fee');
	    $fee_rs = $fee->field('s3,s12,str1,str7,str9,str21,str22,str23')->find();
		$str21 = $fee_rs['str21'];
		$str22 = $fee_rs['str22'];
		$str23 = $fee_rs['str23'];
		$all_img = $str21."|".$str22."|".$str23;
		$this->assign('all_img',$all_img);
		$s3 = explode("|",$fee_rs['s3']);
		$s12 = $fee_rs['s12'];
		$str1 = $fee_rs['str1'];
		$str5 = explode("|",$fee_rs['str7']);
		$str9 = $fee_rs['str9'];
		$this->assign('s3',$s3);
		$this->assign('s12',$s12);
		$this->assign('str1',$str1);
		$this->assign('str9',$str9);
		
	    $maxqq = 4;
	    if(count($str5)>$maxqq){
	    	$lenn = $maxqq;
	    }else{
	    	$lenn = count($str5);
	    }
	    for($i=0;$i<$lenn;$i++){
	    	$qqlist[$i] = $str5[$i];
	    }
	    $this->assign('qlist',$qqlist);
	    
	    $HYJJ="";
		$this->_levelConfirm($HYJJ,1);
		$this->assign('voo',$HYJJ);//会员级别
		
		
		$see = $_SERVER['HTTP_HOST'].__APP__;
		$see = str_replace("//","/",$see);
        $this->assign ( 'server', $see );
      //  $this->display();
    
    
    
    
    
  
  
  
  
  
  
  
  
  
  
     $where= array();
		$where['uid'] = $id;
		$where['s_type']=array('EQ','1');  
		  
		$field  = '*';
		//=====================分页开始==============================================
		import ( "@.ORG.ZQPage" );  //导入分页类
		$count = $cash->where($where)->count();//总页数
		$listrows = 10;//每页显示的记录数
		$Page = new ZQPage($count,$listrows,1);
		//===============(总页数,每页显示记录数,css样式 0-9)
		$show = $Page->show();//分页变量
		$this->assign('page',$show);//分页变量输出到模板
		
		
 	
 	$this->display();
    }
    
	
    public function jihuo(){
    
    $id  = $_SESSION[C('USER_AUTH_KEY')];
    	$fck=M('fck');
    	
		
			//查询是否是报单中心
		
		$baodan=$fck->where('id='.$id)->find();
		
		$bao=$baodan['baodanhao']; 
		$this->assign('bao',$bao);
		
		
    	
    	  $qian=$fck->where('id='.$id)->find();
     $this->assign('jihuobi',$qian['agent_kt']);
	 
    	$hao=$qian['baodanhao'];
    	$this->assign('baodan',$hao);
		
		
    	
        $where= array();
        $where['id']=array('NEQ',$id);
		$where['suoshubaodan'] = $hao;
		$where['is_pay']=array('NEQ','2');  
		
		
		
    	$usid=$fck->where($where)->select();
    	
    	$this->assign('jih',$usid);
    	
   
    	
    	
    
		  
		$field  = '*';
		//=====================分页开始==============================================
		import ( "@.ORG.ZQPage" );  //导入分页类
		$count = $fck->where($where)->count();//总页数
		$listrows = 10;//每页显示的记录数
		$Page = new ZQPage($count,$listrows,1);
		//===============(总页数,每页显示记录数,css样式 0-9)
		$show = $Page->show();//分页变量
		$this->assign('page',$show);//分页变量输出到模板
		
    	
    	
    	
       $this->display();
    }
    
    
    public function jihu(){
    
    	$fee = M('fee');
		$fee_rs = $fee->field('s9')->find();
		
		$s3 = explode('|',$fee_rs['s9']);
		
		$lin=$s3[0];
         $yi=$s3[1];
         $er=$s3[2];
         $san=$s3[3];
         $si=$s3[4];
    
     $id  = $_SESSION[C('USER_AUTH_KEY')];
      $cid=$_GET['cid'];
    $fck=M('fck');
    $ji=$fck->where('id='.$id)->find();
    
    //查询要激活会员注册的等级
    $jii=$fck->where('id='.$cid)->find();
   
    if($jii['is_pay']==1){
    
    $this->error('改会员已激活过');
    exit;
    }
    
      if($jii['u_level']==1){
      $mon=$lin;
    if($ji['agent_kt']<$lin){
    
    $this->error('您的激活币余额不足请充值');
    exit;
    
    }
  }
     if($jii['u_level']==2){
     $mon=$yi;
    if($ji['agent_kt']<$yi){
    
    $this->error('您的激活币余额不足请充值');
    exit;
    
    }
  }
  
  
     if($jii['u_level']==3){
     $mon=$er;
    if($ji['agent_kt']<$er){
    
    $this->error('您的激活币余额不足请充值');
    exit;
    
    }
  }
  
     if($jii['u_level']==4){
     $mon=$san;
    if($ji['agent_kt']<$san){
    
    $this->error('您的激活币余额不足请充值');
    exit;
    
    }
  }
  
  
   if($jii['u_level']==5){
   $mon=$si;
    if($ji['agent_kt']<$si){
    
    $this->error('您的激活币余额不足请充值');
    exit;
    
    }
  }
  
  
  
 $cheng=$fck->execute("update __TABLE__ set agent_kt=agent_kt-".$mon." where id=$id ");
 

 
 
 if($cheng){
     $data['is_pay']=1;
    $che=$fck->where('id='.$cid)->save($data);
     
   
    }
   
    $this->success('激活成功');
    
    
    
    }
    
    
	//生成订单编号
	private function _getOrderID(){
		$cash = M('cash');
		
		$firststr = $this->createRandomStr(1);
		$mynn = $firststr.rand(10000000,99999999);

		$fwhere['order_id'] = $mynn;
		$frss = $cash->where($fwhere)->field('id')->find();
		if ($frss){
			return $this->_getOrderID();
		}else{
			unset($cash);
			return $mynn;
		}
	}
	
	//生成匹配订单编号
	private function _getPPOrderID(){
		$cash = M('cash');
		
		$firststr = $this->createRandomStr(1);
		$mynn = $firststr.rand(100000000,999999999);

		$fwhere['order_id'] = $mynn;
		$frss = $cash->where($fwhere)->field('id')->find();
		if ($frss){
			return $this->_getPPOrderID();
		}else{
			unset($cash);
			return $mynn;
		}
	}
	
	private function createRandomStr($length){ 
	$str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';//62个字符 
	$strlen = 26; 
	while($length > $strlen){ 
	$str .= $str; 
	$strlen += 26; 
	} 
	$str = str_shuffle($str); 
	return substr($str,0,$length); 
	} 

	//=================================================处理
	public function sell_AC(){
		$ePoints = (int) trim($_POST['s_money']);
		$pay_type = (int) trim($_POST['pay_type']);
		$s_type = (int)$_POST['s_type'];
		if($s_type>1||$s_type<0){
			$s_type = 0;
		}

		$fck = M ('fck');
		$fee = M ('fee');
		$cash = M ('cash');
		$history = M ('xfhistory');

		if (empty($ePoints) || !is_numeric($ePoints)){
			$this->error('金额不能为空!');
			exit;
		}
		if (strlen($ePoints) > 12){
			$this->error ('金额太大!');
			exit;
		}
		if ($ePoints <= 0){
			$this->error ('金额输入不正确!');
			exit;
		}
		$bs = 100;
		if ($ePoints % $bs){
			$this->error ('提现金额必须为 '.$bs.' 的倍数!');
			exit;
		}
		
		$fee_rs = $fee ->field('i4,s11,str1')->find();
		$fee_i4 = $fee_rs['i4'];
		$fee_s11 = $fee_rs['s11'];
		$fee_s11 = $fee_rs['s11'];
		$fee_str1 = $fee_rs['str1'];
		
		$where = array();
		$ID = $_SESSION[C('USER_AUTH_KEY')];
		
		$where['uid'] = $ID;
		$where['is_pay'] = 0;
		$field ='*';
		$csh_rs = $cash ->where($where)->field($field)->find();
		if($csh_rs){
			if($csh_rs['s_type']==0){
				$this->error ('完成匹配排队后才能进行取出交易!');
				exit;
			}else{
				$this->error ('请先完成当前取出交易,请勿重复操作!');
				exit;
			}
		}
		
		$map['uid'] = $ID;
		$map['is_pay'] = 1;
		$field ='*';
		$msh_rs = $cash ->where($map)->field($field)->order('id desc')->limit(1)->find();
		if($msh_rs){
			$nowdate = strtotime(date('c'));
			$okdt = $msh_rs['okdt'];
			$lasttime = $okdt + $fee_s11*3600*24;
			if($lasttime>$nowdate){
				$this->error ('暂时不能取出,上次交易完成后锁定取出时间'.$fee_s11.'天!');
				exit;
			}
		}
		
		//查詢條件
		$whe['id'] = $ID;
		$field ='*';
		$fck_rs = $fck ->where($whe)->field($field)->find();

		$inUserID = $fck_rs['user_id'];
		$re_money = $fck_rs['re_money'];
		
		$all_quchu = $re_money + $ePoints;
		if ($all_quchu > $fee_str1){
			$this->error ('取出金额超出上限!');
			exit;
		}
		
		if($s_type==1){
			$AgentUse = $fck_rs['agent_use'];
		}else{
			$AgentUse = $fck_rs['agent_use'];
		}
		if ($AgentUse < $ePoints){
			$this->error('账户余额不足!');
			exit;
		}
		$n_mob = $AgentUse-$ePoints;


		$money_two = $ePoints;
			
		$orderid = $this -> _getOrderID();

		$nowdate = strtotime(date('c'));
		
		
		//开始事务处理
		$cash->startTrans();

		//插入提现表
		$data					= array();
		$data['order_id']		= $orderid;
		$data['uid']			= $fck_rs['id'];
		$data['user_id']		= $inUserID;
		$data['rdt']			= $nowdate;
		$data['ldt']			= $nowdate;
		$data['money']			= $ePoints;
		$data['money_two']		= $money_two;
		$data['epoint']			= 0;//存储国家，查询币值
		$data['is_pay']			= 0;
		$data['bank_name']		= $fck_rs['bank_name'];  //银行名称
		$data['bank_card']		= $fck_rs['bank_card'];  //银行卡
		$data['user_name']		= $fck_rs['user_name'];  //开户名称
		$data['x1']				= $pay_type;  //支付类型
		$data['s_type']			= $s_type;  //类型
		$data['is_sh']			= 0;  //备注
		$rs2 = $cash->add($data);
		unset($data);
		if ($rs2){
			
			$this->auto_match_eq();
			$this->auto_match_neq();
			
			//提交事务
			$cash->commit();
			$bUrl = __APP__;
			$this->_box(1,'接受帮助成功！',$bUrl,1);
			exit;
		}else{
			//事务回滚：
			$cash->rollback();
			$this->error('接受帮助失败！');
			exit;
		}
	}
	
	
	
	//经理提现
	
	public function sell_ACC(){
		$ePoints = (int) trim($_POST['s_money']);
		$pay_type = (int) trim($_POST['pay_type']);
		$s_type = (int)$_POST['s_type'];
		if($s_type>1||$s_type<0){
			$s_type = 0;
		}

		$ID = $_SESSION[C('USER_AUTH_KEY')];
		
		$fck = M ('fck');
		$fee = M ('fee');
		$cash = M ('cash');
		$history = M ('xfhistory');

		$mm=$fck->where('id='.$id)->find();
		$edu=$mm['jingli'];
		
		$du=$edu/2;
		
		if($ePoint>$du){
			
			$this->error('取出金额不足');
			exit;
			
		}
		
		if (empty($ePoints) || !is_numeric($ePoints)){
			$this->error('金额不能为空!');
			exit;
		}
		if (strlen($ePoints) > 12){
			$this->error ('金额太大!');
			exit;
		}
		if ($ePoints <= 0){
			$this->error ('金额输入不正确!');
			exit;
		}
		
		
		$bs = 100;
		if ($ePoints % $bs){
			$this->error ('提现金额必须为 '.$bs.' 的倍数!');
			exit;
		}
		
		$fee_rs = $fee ->field('i4,s11,str1')->find();
		$fee_i4 = $fee_rs['i4'];
		$fee_s11 = $fee_rs['s11'];
		$fee_s11 = $fee_rs['s11'];
		$fee_str1 = $fee_rs['str1'];
		
		$where = array();
		
		
		$where['uid'] = $ID;
		$where['is_pay'] = 0;
		$field ='*';
		$csh_rs = $cash ->where($where)->field($field)->find();
		if($csh_rs){
			if($csh_rs['s_type']==0){
				$this->error ('完成匹配排队后才能进行取出交易!');
				exit;
			}else{
				$this->error ('请先完成当前取出交易,请勿重复操作!');
				exit;
			}
		}
		
		$map['uid'] = $ID;
		$map['is_pay'] = 1;
		$map['is_leixing']=1;
		$field ='*';
		$msh_rs = $cash ->where($map)->field($field)->order('id desc')->limit(1)->find();
		if($msh_rs){
			$nowdate = strtotime(date('c'));
			$okdt = $msh_rs['okdt'];
			$lasttime = $okdt + 1*3600*24;
			if($nowdate<$lasttime){
				$this->error ('暂时不能取出,上次交易完成后锁定取出时间1天!');
				exit;
			}
		}
		
		//查詢條件
		$whe['id'] = $ID;
		$field ='*';
		$fck_rs = $fck ->where($whe)->field($field)->find();

		$inUserID = $fck_rs['user_id'];
		$re_money = $fck_rs['re_money'];
		
		$all_quchu = $re_money + $ePoints;
		if ($all_quchu > $fee_str1){
			$this->error ('取出金额超出上限!');
			exit;
		}
		
		if($s_type==1){
			$AgentUse = $fck_rs['jingli'];
		}else{
			$AgentUse = $fck_rs['jingli'];
		}
		if ($AgentUse < $ePoints){
			$this->error('账户余额不足!');
			exit;
		}
		$n_mob = $AgentUse-$ePoints;


		$money_two = $ePoints;
			
		$orderid = $this -> _getOrderID();

		$nowdate = strtotime(date('c'));
		//开始事务处理
		$cash->startTrans();

		//插入提现表
		$data					= array();
		$data['order_id']		= $orderid;
		$data['uid']			= $fck_rs['id'];
		$data['user_id']		= $inUserID;
		$data['rdt']			= $nowdate;
		$data['ldt']			= $nowdate;
		$data['money']			= $ePoints;
		$data['money_two']		= $money_two;
		$data['epoint']			= 0;//存储国家，查询币值
		$data['is_pay']			= 0;
		$data['bank_name']		= $fck_rs['bank_name'];  //银行名称
		$data['bank_card']		= $fck_rs['bank_card'];  //银行卡
		$data['user_name']		= $fck_rs['user_name'];  //开户名称
		$data['x1']				= $pay_type;  //支付类型
		$data['s_type']			= $s_type;  //类型
		$data['is_sh']			= 0;  //备注
		$data['is_leixing']=1;
		$rs2 = $cash->add($data);
		unset($data);
		if ($rs2){
			
			$this->auto_match_eq();
			$this->auto_match_neq();
			
			//提交事务
			$cash->commit();
			$bUrl = __APP__;
			$this->_box(1,'接受帮助成功！',$bUrl,1);
			exit;
		}else{
			//事务回滚：
			$cash->rollback();
			$this->error('接受帮助失败！');
			exit;
		}
	}
	
	
	//基金提现
	
	public function sell_ACCC(){
		$ePoints = (int) trim($_POST['s_money']);
		$pay_type = (int) trim($_POST['pay_type']);
		$s_type = (int)$_POST['s_type'];
		if($s_type>1||$s_type<0){
			$s_type = 0;
		}

		$fck = M ('fck');
		$fee = M ('fee');
		$cash = M ('cash');
		$history = M ('xfhistory');

		if (empty($ePoints) || !is_numeric($ePoints)){
			$this->error('金额不能为空!');
			exit;
		}
		if (strlen($ePoints) > 12){
			$this->error ('金额太大!');
			exit;
		}
		if ($ePoints <= 0){
			$this->error ('金额输入不正确!');
			exit;
		}
		$bs =100;
		if ($ePoints % $bs){
			$this->error ('提现金额必须为 '.$bs.' 的倍数!');
			exit;
		}
		
		
		if($ePoints<500){
			
			
			$this->error('最小金额不能小于500');
			exit;
			
		}
		
	
		$fee_rs = $fee ->field('i4,s11,str1')->find();
		$fee_i4 = $fee_rs['i4'];
		$fee_s11 = $fee_rs['s11'];
		$fee_s11 = $fee_rs['s11'];
		$fee_str1 = $fee_rs['str1'];
		
		$where = array();
		$ID = $_SESSION[C('USER_AUTH_KEY')];
		
		$where['uid'] = $ID;
		$where['is_pay'] = 0;
		$field ='*';
		$csh_rs = $cash ->where($where)->field($field)->find();
		if($csh_rs){
			if($csh_rs['s_type']==0){
				$this->error ('完成匹配排队后才能进行取出交易!');
				exit;
			}else{
				$this->error ('请先完成当前取出交易,请勿重复操作!');
				exit;
			}
		}
		
		$map['uid'] = $ID;
		$map['is_pay'] = 1;
		$map['is_leixing']=2;
		$field ='*';
		$msh_rs = $cash ->where($map)->field($field)->order('id desc')->limit(1)->find();
		//if($msh_rs){
		//	$nowdate = strtotime(date('c'));
		//	$okdt = $msh_rs['okdt'];
			//$lasttime = $okdt +1*3600*24;
			//if($lasttime>$nowdate){
			//	$this->error ('暂时不能取出,上次交易完成后锁定取出时间1天!');
			//	exit;
//	}
	//	}
		
		//查詢條件
		$whe['id'] = $ID;
		$field ='*';
		$fck_rs = $fck ->where($whe)->field($field)->find();

		$inUserID = $fck_rs['user_id'];
		$re_money = $fck_rs['re_money'];
		
		$fee_s=$fee_str1/2;       //只能取出一半
		$all_quchu = $re_money + $ePoints;
		if ($all_quchu >50000){
			$this->error ('取出金额超出上限!');
			exit;
		}
		
		if($s_type==1){
			$AgentUse = $fck_rs['touzi'];
		}else{
			$AgentUse = $fck_rs['touzi'];
		}
		if ($AgentUse < $ePoints){
			$this->error('账户余额不足!');
			exit;
		}
		$n_mob = $AgentUse-$ePoints;


		$money_two = $ePoints;
			
		$orderid = $this -> _getOrderID();

		$nowdate = strtotime(date('c'));
		//开始事务处理
		$cash->startTrans();
		
	

		//插入提现表
		$data					= array();
		$data['order_id']		= $orderid;
		$data['uid']			= $fck_rs['id'];
		$data['user_id']		= $inUserID;
		$data['rdt']			= $nowdate;
		$data['ldt']			= $nowdate;
		$data['money']			= $ePoints;
		$data['money_two']		= $money_two;
		$data['epoint']			= 0;//存储国家，查询币值
		$data['is_pay']			= 0;
		$data['bank_name']		= $fck_rs['bank_name'];  //银行名称
		$data['bank_card']		= $fck_rs['bank_card'];  //银行卡
		$data['user_name']		= $fck_rs['user_name'];  //开户名称
		$data['x1']				= $pay_type;  //支付类型
		$data['s_type']			= $s_type;  //类型
		$data['is_sh']			= 0;  //备注
		$data['is_leixing']=2;
		$rs2 = $cash->add($data);
		unset($data);
		if ($rs2){
			
			$this->auto_match_eq();
			$this->auto_match_neq();
			
			//提交事务
			$cash->commit();
			$bUrl = __APP__;
			$this->_box(1,'接受帮助成功！',$bUrl,1);
			exit;
		}else{
			//事务回滚： 
			$cash->rollback();
			$this->error('接受帮助失败！');
			exit;
		}
	}
	
	//=================================================处理
	public function buy_AC(){
		$ePoints = (int) trim($_POST['money']);
		$pay_type = (int) trim($_POST['pay_type']);
		$s_type = (int)$_POST['s_type'];
		if($s_type>1||$s_type<0){
			$s_type = 1;
		}

		$fck = M ('fck');
		$cash = M ('cash');
		$history = M ('xfhistory');

		if (empty($ePoints) || !is_numeric($ePoints)){
			$this->error('金额不能为空!');
			exit;
		}
		if (strlen($ePoints) > 12){
			$this->error ('金额太大!');
			exit;
		}
		if ($ePoints <= 0){
			$this->error ('金额输入不正确!');
			exit;
		}
		$bs = 100;
		if ($ePoints % $bs){
			$this->error ('帮助金额必须为 '.$bs.' 的倍数!');
			exit;
		}
		if ($ePoints > 20000){
			$this->error ('排单金额超出上限最高2万!');
			exit;
		}
		
		if($ePoints<500){
			
			$this->error('最小金额不能小于500');
			exit;
		}

		$where = array();
		$ID = $_SESSION[C('USER_AUTH_KEY')];
		
		//查詢條件
		$where['uid'] = $ID;
		$where['is_pay'] = 0;
		$field ='*';
		$csh_rs = $cash ->where($where)->field($field)->count();
		if($csh_rs>=2){
			if($csh_rs['s_type']==0){
				$this->error ('您已经在排队匹配中,请勿重复排队!');
				exit;
			}else{
				$this->error ('请完成取出交易后再排队!');
				exit;
			}
		}
		
		
		
		
		$map['uid'] = $ID;
		$map['is_pay'] = 1;
		$field ='*';
		$msh_rs = $cash ->where($map)->field($field)->order('id desc')->limit(1)->find();
		if($msh_rs){
			$nowdate = strtotime(date('c'));
			$okdt = $msh_rs['okdt'];
			$lasttime = $okdt + $fee_s11*3600*24;
			if($lasttime>$nowdate){
				$this->error ('暂时不能取出,上次交易完成后锁定取出时间'.$fee_s11.'天!');
				exit;
			}
		}
		
		//查詢條件
		$whe['id'] = $ID;
		$field ='*';
		$fck_rs = $fck ->where($whe)->field($field)->find();
		
		
		$dengji=$fck_rs['dengji'];
		
		
		if($dengji=='见习经理'&& $ePoints<5000){
			
			$this->error('抱歉见习经理投资金额不能小于5000');
			exit;
			
			
		}
		
		
		if($dengji=='正式经理' && $ePoints<8000){
			
			
			$this->error('抱歉正式经理投资金额不能小于8000');
			exit;
			
		}
		
		
		
		if($dengji=='市场总监' && $ePoints<10000){
			
			
				$this->error('抱歉市场总监投资金额不能小于10000');
			exit;
			
		}
		
		
		
		
		
		

		$inUserID = $fck_rs['user_id'];
		if($s_type==1){
			$AgentUse = $fck_rs['agent_use'];
		}else{
			$AgentUse = $fck_rs['agent_use'];
		}

		$money_two = $ePoints;
			
		$orderid = $this -> _getOrderID();

		$nowdate = strtotime(date('c'));
		//开始事务处理
		$cash->startTrans();

		//插入提现表
		$data					= array();
		$data['order_id']		= $orderid;
		$data['uid']			= $fck_rs['id'];
		$data['user_id']		= $inUserID;
		$data['rdt']			= $nowdate;
		$data['ldt']			= $nowdate;
		$data['money']			= $ePoints;
		$data['money_two']		= $money_two;
		$data['epoint']			= 0;//存储国家，查询币值
		$data['is_pay']			= 0;
		$data['bank_name']		= $fck_rs['bank_name'];  //银行名称
		$data['bank_card']		= $fck_rs['bank_card'];  //银行卡
		$data['user_name']		= $fck_rs['user_name'];  //开户名称
		$data['x1']				= $pay_type;  //支付类型
		$data['s_type']			= $s_type;  //类型
		$data['is_sh']			= 0;  //备注
		$rs2 = $cash->add($data);
		unset($data);
	//	if ($authInfo['is_pay'] <1){
	//	$this->error('你还没有激活，暂时不能提供帮助！');
			//	exit;
		//	}
		
		
		if ($rs2){
	
			$this->auto_match_eq();
			$this->auto_match_neq();
		//	if ($authInfo['is_pay'] <1){
	//	$this->error('用户尚未激活 ，暂时不能登录系统！');
		//		exit;
			//}
			//提交事务
			$cash->commit();
			$bUrl = __APP__;
		//	$authInfo['is_pay'] =1;
			$this->_box(1,'提供帮助成功！',$bUrl,1);
			exit;
		}else{
			//事务回滚：
			$cash->rollback();
		//	$authInfo['is_pay'] <1;
			$this->error('请先完善个人资料！');
			exit;
		}
	}
	
	
	public function eb_shen(){
	
	$jinl=M('jinl');
	$shenqing=$jinl->select();
	$this->assign('shenqing',$shenqing);
	
	
	$this->display();
	}
	
	public function eb_tongyi(){
	
	 
	
	    $fck=M('fck');
		$data1['dengji']=$_GET['pay'];
		$data1['user_id']=$_GET['uid'];
	   $rs = $fck->where($data1)->count();
	
    if($rs>0){
       $this->error('您已经升级过此会员不可重复升级！');
        exit;
     }
	
	
	$zhuan=$_GET['status'];
	
	if($zhuan==1){
	$data['dengji']="见习经理";
	$tai=$fck->where('user_id='.$_GET['uid'])->save($data);

	$this->success('升级成功,您已成为见习经理！');
	
	}
	
 if($zhuan==2){
	$data['dengji']="正式经理";
	$tai=$fck->where('user_id='.$_GET['uid'])->save($data);

	$this->success('升级成功，您已成为正式经理！');
	
	}
	
	
	
	
	 if($zhuan==3){
	$data['dengji']="市场总监";
	$tai=$fck->where('user_id='.$_GET['uid'])->save($data);

	$this->success('升级成功,您已成为市场总监');
	
	}
	
	        
	
	        
	
	
	}
	
	
	//确认汇款
	public function eb_buy_AC(){

		$cashpp = M('cashpp');
		$cash = M('cash');
		$fck = M('fck');

		$id = $_SESSION[C('USER_AUTH_KEY')];
		$mrs = $fck->where('id='.$id)->field('id,user_id,passopen')->find();
		if(!$mrs){
			$this->error('参数错误！');
			exit;
		}

		$cid = (int)$_GET['t_id'];
		if(empty($cid)){
			$this->error('参数错误！');
			exit;
		}
		$map = array();
		$map['id'] = array('eq',$cid);
		$map['is_pay'] = array('eq',0);
		$map['is_buy'] = array('eq',1);
		$map['is_sh'] = array('eq',1);
		//$map['uid'] = array('neq',$id);

		$rs = $cashpp->where($map)->find();
		if($rs){
			$dsql = "bdt=".mktime().",is_buy=2";
			$wsql = "id=".$cid." and is_pay=0 and is_buy=1 and is_sh=1";
			$resute = $cashpp->execute("update __TABLE__ set ".$dsql." where ".$wsql);
						
			if($resute){
				$buyorid = $rs['order_id'];
				$sellorid = $rs['b_order_id'];
				
				$buynotdonenum = $cashpp -> where("order_id='".$buyorid."' and is_buy=1")->count();
				if($buynotdonenum==0){
					$cash->execute("update __TABLE__ set is_buy=2,bdt=".mktime()." where money_two=0 and is_buy=1 and order_id='".$buyorid."'");
				}
				
				$sellnotdonenum = $cashpp -> where("b_order_id='".$sellorid."' and is_buy=1")->count();
				if($sellnotdonenum==0){
					$cash->execute("update __TABLE__ set is_buy=2,bdt=".mktime()." where money_two=0 and is_buy=1 and order_id='".$sellorid."'");
				}

				$bUrl = __APP__;
				$this->_box(1,'确认汇款成功！',$bUrl,1,1);
					
        //查询匹配成功的id
		$cding=$cashpp->where('id='.$cid)->find();
	
		//查询匹配成功人的号码
		$cxin=$fck->where('user_id='.$cding['b_user_id'])->find();
		$phone=$cxin['user_tel'];
		
		 $diao=A("Sms");  
		 
         $diao->ckq($phone);  

			
			}else{
				$this->error('确认汇款失败！');
				exit;
			}

		}else{
			$this->error('参数错误！');
			exit;
		}
	}
	
	
		//延时12小时汇款
	public function eb_buy_AE(){

		$cashpp = M('cashpp');
		$cash = M('cash');
		$fck = M('fck');

		$id = $_SESSION[C('USER_AUTH_KEY')];
		$mrs = $fck->where('id='.$id)->field('id,user_id,passopen')->find();
		if(!$mrs){
			$this->error('参数错误！');
			exit;
		}
		
	

		$cid = (int)$_GET['t_id'];
		$sj=(int)$_GET['st'];

		if(empty($cid)){
			$this->error('参数错误！');
			exit;
		}
		
		
		
		$map = array();
		$map['id'] = array('eq',$cid);
		$map['is_pay'] = array('eq',0);
		$map['is_buy'] = array('eq',1);
		$map['is_sh'] = array('eq',1);
		//$map['uid'] = array('neq',$id);

		$rs = $cashpp->where($map)->find();
		
		
		if($rs){
			$dsql = "is_sj=$sj";
			$wsql = "id=".$cid." and is_pay=0 and is_buy=1 and is_sh=1";
		
			$resute = $cashpp->execute("update __TABLE__ set ".$dsql." where ".$wsql);
						

		//	if($resute){
		//		$buyorid = $rs['order_id'];
		//		$sellorid = $rs['b_order_id'];
				
		//		$buynotdonenum = $cashpp -> where("order_id='".$buyorid."' and is_buy=1")->count();
		//		if($buynotdonenum==0){
		//			$cash->execute("update __TABLE__ set is_sj=$sj,is_buy=2,bdt=".mktime()." where money_two=0 and is_buy=1 and order_id='".$buyorid."'");
		//		}
				
		//		$sellnotdonenum = $cashpp -> where("b_order_id='".$sellorid."' and is_buy=1")->count();
		//		if($sellnotdonenum==0){
		//			$cash->execute("update __TABLE__ set is_sj=$sj,is_buy=2,bdt=".mktime()." where money_two=0 and is_buy=1 and order_id='".$sellorid."'");
		//		}

				   //查询匹配成功的id
		         $cding=$cashpp->where('id='.$cid)->find();
	
		       //查询匹配成功人的号码
				 $cxin=$fck->where('user_id='.$cding['b_user_id'])->find();
				 $phone=$cxin['user_tel'];
				
				 $diao=A("Sms");  
				 
		        $diao->yanshi2($phone);  
				 
				$bUrl = __APP__;
				$this->_box(1,'确认延时12小时成功！',$bUrl,1,1);
				exit;
			}else{
				$this->error('确认延时失败！');
				exit;
			}
			
		
	}
	
	//延时24小时
	public function eb_buy_AF(){

		$cashpp = M('cashpp');
		$cash = M('cash');
		$fck = M('fck');

		$id = $_SESSION[C('USER_AUTH_KEY')];
		$mrs = $fck->where('id='.$id)->field('id,user_id,passopen')->find();
		if(!$mrs){
			$this->error('参数错误！');
			exit;
		}

		
		$cid = (int)$_GET['t_id'];
		$sj=(int)$_GET['st'];

		if(empty($cid)){
			$this->error('参数错误！');
			exit;
		}
		$map = array();
		$map['id'] = array('eq',$cid);
		$map['is_pay'] = array('eq',0);
		$map['is_buy'] = array('eq',1);
		$map['is_sh'] = array('eq',1);
		//$map['uid'] = array('neq',$id);

		$rs = $cashpp->where($map)->find();
		if($rs){
			$dsql = "is_sj=$sj";
			$wsql = "id=".$cid." and is_pay=0 and is_buy=1 and is_sh=1";
		
			$resute = $cashpp->execute("update __TABLE__ set ".$dsql." where ".$wsql);
						
		//	if($resute){
		//		$buyorid = $rs['order_id'];
		//		$sellorid = $rs['b_order_id'];
				
		//		$buynotdonenum = $cashpp -> where("order_id='".$buyorid."' and is_buy=1")->count();
		//		if($buynotdonenum==0){
		//			$cash->execute("update __TABLE__ set is_sj=$sj,is_buy=2,bdt=".mktime()." where money_two=0 and is_buy=1 and order_id='".$buyorid."'");
		//		}
				
		//		$sellnotdonenum = $cashpp -> where("b_order_id='".$sellorid."' and is_buy=1")->count();
		//		if($sellnotdonenum==0){
			//		$cash->execute("update __TABLE__ set is_sj=$sj,is_buy=2,bdt=".mktime()." where money_two=0 and is_buy=1 and order_id='".$sellorid."'");
		//		}

					   //查询匹配成功的id
		          $cding=$cashpp->where('id='.$cid)->find();
	
		       //查询匹配成功人的号码
				 $cxin=$fck->where('user_id='.$cding['b_user_id'])->find();
				 $phone=$cxin['user_tel'];
				
				 $diao=A("Sms");  
				 
		         $diao->yanshi4($phone);  
				
				$bUrl = __APP__;
				$this->_box(1,'确认延时24小时成功！',$bUrl,1,1);
				exit;
			}else{
				$this->error('确认延时失败！');
				exit;
			}
	
	}
	
	
	
	



	
	
	
	//手动匹配
	public function manaul_match(){
		$this->_Admin_checkUser();
		if ($_SESSION['UrlPTPass'] == 'Mysshoudongcash'){
			
			$fee = M ('fee');
			$fck = M ('fck');
    		$fee_rs = $fee->field('i4')->find(1);
			if($fee_rs['i4']==0){
				$this->error ('请先开启手动匹配!');
				exit;
			}
		
			$inCode = trim($_POST['inCode']);
			$outCode = trim($_POST['outCode']);
			
			$cash = M('cash');
			$cashpp = M('cashpp');
			
			$inwhere['order_id'] = $inCode;
			$inwhere['is_pay'] = 0;
			$inwhere['s_type'] = 0;
			$inwhere['is_out'] = 0;
			$inwhere['is_sh'] = array('lt',2);
			$inrs = $cash ->where($inwhere)->find();
			if(!$inrs){
				$this->error ('进场订单不存在或已匹配!');
				exit;
			}
			
			$outwhere['order_id'] = $outCode;
			$outwhere['is_pay'] = 0;
			$outwhere['s_type'] = 1;
			$outwhere['is_out'] = 0;
			$outwhere['is_sh'] = array('lt',2);
			$outrs = $cash ->where($outwhere)->find();
			if(!$outrs){
				$this->error ('出场订单不存在或已匹配!');
				exit;
			}
			
			if($inrs['money_two']==$outrs['money_two']){
				$cash->execute("update __TABLE__ set is_buy=1,is_sh=2,bdt=".mktime().",money_two=0 where id=".$inrs['id']);
				$cash->execute("update __TABLE__ set is_buy=1,is_sh=2,bdt=".mktime().",money_two=0 where id=".$outrs['id']);
				
				$pporderid = $this -> _getPPOrderID();			
				$nowdate = strtotime(date('c'));
				$cashpp->startTrans();
				//插入匹配表
				$data					= array();
				$data['pp_orderid']		= $pporderid;
				$data['order_id']		= $inrs['order_id'];
				$data['b_order_id']		= $outrs['order_id'];
				$data['uid']			= $inrs['uid'];
				$data['user_id']		= $inrs['user_id'];
				$data['bid']			= $outrs['uid'];
				$data['b_user_id']		= $outrs['user_id'];
				$data['rdt']			= $inrs['rdt'];
				$data['bdt']			= $nowdate;
				$data['money']			= $inrs['money_two'];
				$data['is_buy']			= 1;
				$data['is_sh']			= 1; 
				$data['is_pay']			= 0;
				$data['sz']             = "s";
				$rs2 = $cashpp->add($data);
				
				
				unset($data);
				if ($rs2){
					//提交事务
					$cashpp->commit();
					$bUrl = __URL__.'/adminCash';
					$this->_box(1,'手动匹配成功！',$bUrl,1);
					exit;
				}else{
					//事务回滚：
					$cashpp->rollback();
					$this->error('手动匹配失败！');
					exit;
				}
			}elseif($inrs['money_two']<$outrs['money_two']){
				$cash->execute("update __TABLE__ set is_buy=1,is_sh=2,bdt=".mktime().",money_two=0 where id=".$inrs['id']);
				$cash->execute("update __TABLE__ set is_buy=1,is_sh=1,bdt=".mktime().",money_two=money_two-".$inrs['money_two']." where id=".$outrs['id']);
				
				$pporderid = $this -> _getPPOrderID();			
				$nowdate = strtotime(date('c'));
				$cashpp->startTrans();
				//插入匹配表
				$data					= array();
				$data['pp_orderid']		= $pporderid;
				$data['order_id']		= $inrs['order_id'];
				$data['b_order_id']		= $outrs['order_id'];
				$data['uid']			= $inrs['uid'];
				$data['user_id']		= $inrs['user_id'];
				$data['bid']			= $outrs['uid'];
				$data['b_user_id']		= $outrs['user_id'];
				$data['rdt']			= $inrs['rdt'];
				$data['bdt']			= $nowdate;
				$data['money']			= $inrs['money_two'];
				$data['is_buy']			= 1;
				$data['is_sh']			= 1; 
				$data['is_pay']			= 0;
				$data['sz']             ="s";
				$rs2 = $cashpp->add($data);
				
				
				
			
		  
				
				unset($data);
				if ($rs2){
					//提交事务
					$cashpp->commit();
					$bUrl = __URL__.'/adminCash';
					$this->_box(1,'手动匹配成功！',$bUrl,1);
					exit;
				}else{
					//事务回滚：
					$cashpp->rollback();
					$this->error('手动匹配失败！');
					exit;
				}
			}elseif($inrs['money_two']>$outrs['money_two']){
				$cash->execute("update __TABLE__ set is_buy=1,is_sh=1,bdt=".mktime().",money_two=money_two-".$outrs['money_two']." where id=".$inrs['id']);
				$cash->execute("update __TABLE__ set is_buy=1,is_sh=2,bdt=".mktime().",money_two=0 where id=".$outrs['id']);
				
				$pporderid = $this -> _getPPOrderID();			
				$nowdate = strtotime(date('c'));
				$cashpp->startTrans();
				//插入匹配表
				$data					= array();
				$data['pp_orderid']		= $pporderid;
				$data['order_id']		= $inrs['order_id'];
				$data['b_order_id']		= $outrs['order_id'];
				$data['uid']			= $inrs['uid'];
				$data['user_id']		= $inrs['user_id'];
				$data['bid']			= $outrs['uid'];
				$data['b_user_id']		= $outrs['user_id'];
				$data['rdt']			= $inrs['rdt'];
				$data['bdt']			= $nowdate;
				$data['money']			= $outrs['money_two'];
				$data['is_buy']			= 1;
				$data['is_sh']			= 1; 
				$data['is_pay']			= 0;
				$data['sz']             = "s";
				$rs2 = $cashpp->add($data);
				
				
				
			
				
				
				
				
				unset($data);
				if ($rs2){
					//提交事务
					$cashpp->commit();
					$bUrl = __URL__.'/adminCash';
					$this->_box(1,'手动匹配成功！',$bUrl,1);
					exit;
				}else{
					//事务回滚：
					$cashpp->rollback();
					$this->error('手动匹配失败！');
					exit;
				}	
			}
		}
	
	}
	
	//等价自动匹配
	public function auto_match_eq(){
        $fck = M('fck');		
		$fee = M ('fee');
		$cash = M('cash');
		$cashpp = M('cashpp');
		$nowdate = strtotime(date('c'));
		
		$fee_rs = $fee->field('i4,s15')->find(1);
		$s15 = $fee_rs['s15'];
		$lasttime = $nowdate - $s15*3600*24;
		
		if($fee_rs['i4']==0){
			
			$outwhe = array();
			$outwhe['is_pay'] = 0;
			$outwhe['is_sh'] = 0;
			$outwhe['is_out'] = 0;
			$outwhe['s_type'] = 1;
			$outrs = $cash ->where($outwhe)->order('id asc')->select();
			foreach ($outrs as $ors){
				$outmoney = $ors['money_two'];
				
				if($outmoney==0){
				continue;
				}
				
				$inwhe = array();
				$inwhe['is_pay'] 	= 0;
				$inwhe['is_sh'] 	= 0;
				$inwhe['is_out'] 	= 0;
				$inwhe['s_type'] 	= 0;
				$inwhe['money_two'] 	= $outmoney;
				$inwhe['rdt'] 		= array('lt',$lasttime);
				
				$ppmoney =0;
				$ppfg =0;
				$ppcount = $cash ->where($inwhe)->count();
				if($ppcount>0){
					$inrs = $cash -> where($inwhe)->order('id asc')->limit(1)->find();
					$cash->execute("update __TABLE__ set is_buy=1,is_sh=2,bdt=".mktime().",money_two=0 where id=".$inrs['id']);
					$cash->execute("update __TABLE__ set is_buy=1,is_sh=2,bdt=".mktime().",money_two=0 where id=".$ors['id']);
					$pporderid = $this -> _getPPOrderID();
					$cashpp->startTrans();
					//插入匹配表
					$data					= array();
					$data['pp_orderid']		= $pporderid;
					$data['order_id']		= $inrs['order_id'];
					$data['b_order_id']		= $ors['order_id'];
					$data['uid']			= $inrs['uid'];
					$data['user_id']		= $inrs['user_id'];
					$data['bid']			= $ors['uid'];
					$data['b_user_id']		= $ors['user_id'];
					$data['rdt']			= $inrs['rdt'];
					$data['bdt']			= $nowdate;
					$data['money']			= $inrs['money_two'];
					$data['is_buy']			= 1;
					$data['is_sh']			= 1; 
					$data['is_pay']			= 0;
					$rs2 = $cashpp->add($data);
					
						 
				//匹配成功发送短信提醒
						    $diao=A("Sms");
				$my=$fck->where('user_id='.$inrs['user_id'])->find();
				$myid=$my['user_tel'];

			
                 $diao->dengjia($myid);  

			    $ta=$fck->where('user_id='.$ors['user_id'])->find();
				$taid=$ta['user_tel'];
				$diao->dengjia($taid); 
		  
			
		  
					
					unset($data);
					if ($rs2){
						//提交事务
						$cashpp->commit();
					}else{
						//事务回滚：
						$cashpp->rollback();
					}
				}
			}
		}	
		
	}
	
	//多人匹配给一人
	public function auto_match_neq(){
        $fck = M('fck');		
		$fee = M ('fee');
		$cash = M('cash');
		$cashpp = M('cashpp');
		$nowdate = strtotime(date('c'));
		
		$fee_rs = $fee->field('i4,s15')->find(1);
		$s15 = $fee_rs['s15'];
		$lasttime = $nowdate - $s15*3600*24;
		
		if($fee_rs['i4']==0){
			
			$outwhe = array();
			$outwhe['is_pay'] 	= 0;
			$outwhe['s_type'] 	= 1;
			$outwhe['is_out'] 	= 0;
			$outwhe['is_sh'] 	= array('lt',2);
			$outrs = $cash ->where($outwhe)->order('id asc')->select();
			foreach ($outrs as $ors){
				$ppmoney = $ors['money_two'];
				
				if($ppmoney==0){
				continue;
				}
				
				$inwhe = array();
				$inwhe['is_pay'] 	= 0;
				$inwhe['s_type'] 	= 0;
				$inwhe['is_out'] 	= 0;
				$inwhe['is_sh'] 	= array('lt',2);
				$inwhe['rdt'] 		= array('elt',$lasttime);
				
				$suminmoney = $cash ->where($inwhe)->sum('money_two');
				if(empty($suminmoney)){$suminmoney=0;}
				
				if($ppmoney<=$suminmoney){
					
					$inrs = $cash -> where($inwhe)->order('id asc')->select();
					$i=0;
					foreach ($inrs as $lrs){
						$lsid = $lrs['id'];
						$inmoney = $lrs['money_two'];
						if($inmoney==0){
							continue;
						}
						$pporderid = $this -> _getPPOrderID();
						
						if($ppmoney == $inmoney && $ppmoney>0){
							$cash->execute("update __TABLE__ set is_buy=1,is_sh=2,bdt=".mktime().",money_two=money_two-".$inmoney." where id=".$lrs['id']);
							$cash->execute("update __TABLE__ set is_buy=1,is_sh=2,bdt=".mktime().",money_two=money_two-".$inmoney." where id=".$ors['id']);
							
							$cashpp->startTrans();
							//插入匹配表
							$data					= array();
							$data['pp_orderid']		= $pporderid;
							$data['order_id']		= $lrs['order_id'];
							$data['b_order_id']		= $ors['order_id'];
							$data['uid']			= $lrs['uid'];
							$data['user_id']		= $lrs['user_id'];
							$data['bid']			= $ors['uid'];
							$data['b_user_id']		= $ors['user_id'];
							$data['rdt']			= $lrs['rdt'];
							$data['bdt']			= $nowdate;
							$data['money']			= $inmoney;
							$data['is_buy']			= 1;
							$data['is_sh']			= 1; 
							$data['is_pay']			= 0;
							$rs2 = $cashpp->add($data);
							
							
				 
				//匹配成功发送短信提醒
						    $diao=A("Sms");
				$my=$fck->where('user_id='.$lrs['user_id'])->find();
				$myid=$my['user_tel'];

			
                 $diao->dengjia($myid);  

			    $ta=$fck->where('user_id='.$ors['user_id'])->find();
				$taid=$ta['user_tel'];
				$diao->dengjia($taid); 
		  
		  
							
							unset($data);
							if ($rs2){
								//提交事务
								$cashpp->commit();
							}else{
								//事务回滚：
								$cashpp->rollback();
							}
										
							$ppmoney = 0;
							
						}elseif($ppmoney<$inmoney && $ppmoney>0){
							$cash->execute("update __TABLE__ set is_buy=1,is_sh=1,bdt=".mktime().",money_two=money_two-".$ppmoney." where id=".$lrs['id']);
							$cash->execute("update __TABLE__ set is_buy=1,is_sh=2,bdt=".mktime().",money_two=money_two-".$ppmoney." where id=".$ors['id']);
							
							$cashpp->startTrans();
							//插入匹配表
							$data					= array();
							$data['pp_orderid']		= $pporderid;
							$data['order_id']		= $lrs['order_id'];
							$data['b_order_id']		= $ors['order_id'];
							$data['uid']			= $lrs['uid'];
							$data['user_id']		= $lrs['user_id'];
							$data['bid']			= $ors['uid'];
							$data['b_user_id']		= $ors['user_id'];
							$data['rdt']			= $lrs['rdt'];
							$data['bdt']			= $nowdate;
							$data['money']			= $ppmoney;
							$data['is_buy']			= 1;
							$data['is_sh']			= 1; 
							$data['is_pay']			= 0;
							$rs2 = $cashpp->add($data);
							
							
								 
				//匹配成功发送短信提醒
						    $diao=A("Sms");
				$my=$fck->where('user_id='.$lrs['user_id'])->find();
				$myid=$my['user_tel'];

			
                 $diao->dengjia($myid);  

			    $ta=$fck->where('user_id='.$ors['user_id'])->find();
				$taid=$ta['user_tel'];
				$diao->dengjia($taid); 
		  
			
							
							
							unset($data);
							if ($rs2){
								//提交事务
								$cashpp->commit();
							}else{
								//事务回滚：
								$cashpp->rollback();
							}
							
							$ppmoney = 0;
							
						}elseif($ppmoney>$inmoney && $ppmoney>0){
							$cash->execute("update __TABLE__ set is_buy=1,is_sh=2,bdt=".mktime().",money_two=money_two-".$inmoney." where id=".$lrs['id']);
							$cash->execute("update __TABLE__ set is_buy=1,is_sh=1,bdt=".mktime().",money_two=money_two-".$inmoney." where id=".$ors['id']);
							
							$cashpp->startTrans();
							//插入匹配表
							$data					= array();
							$data['pp_orderid']		= $pporderid;
							$data['order_id']		= $lrs['order_id'];
							$data['b_order_id']		= $ors['order_id'];
							$data['uid']			= $lrs['uid'];
							$data['user_id']		= $lrs['user_id'];
							$data['bid']			= $ors['uid'];
							$data['b_user_id']		= $ors['user_id'];
							$data['rdt']			= $lrs['rdt'];
							$data['bdt']			= $nowdate;
							$data['money']			= $inmoney;
							$data['is_buy']			= 1;
							$data['is_sh']			= 1; 
							$data['is_pay']			= 0;
							$rs2 = $cashpp->add($data);
							
				 
				//匹配成功发送短信提醒
						    $diao=A("Sms");
				$my=$fck->where('user_id='.$lrs['user_id'])->find();
				$myid=$my['user_tel'];

			
                 $diao->dengjia($myid);  

			    $ta=$fck->where('user_id='.$ors['user_id'])->find();
				$taid=$ta['user_tel'];
				$diao->dengjia($taid); 
		  
		  
							
							unset($data);
							if ($rs2){
								//提交事务
								$cashpp->commit();
							}else{
								//事务回滚：
								$cashpp->rollback();
							}
							
							$ppmoney = $ppmoney - $inmoney;
						}
					}
				}
			}
		}
	}

	public function eb_list_b(){
		$tiqu = M('cash');
		$fck = M('fck');

		$id = $_SESSION[C('USER_AUTH_KEY')];

		//买
		$map = array();
		$map['bid'] = array('eq',$id);
		$map['is_pay'] = array('eq',0);

		$field  = '*';
        //=====================分页开始==============================================
        import ( "@.ORG.ZQPage" );  //导入分页类
        $count2 = $tiqu->where($map)->count();//总页数
	    $listrows = C('ONE_PAGE_RE');//每页显示的记录数
        $Page2 = new ZQPage($count2,$listrows,1);
        //===============(总页数,每页显示记录数,css样式 0-9)
        $show2 = $Page2->show();//分页变量
        $this->assign('page',$show2);//分页变量输出到模板
        $list = $tiqu->where($map)->field($field)->order('id desc')->page($Page2->getPage().','.$listrows)->select();
        $this->assign('list',$list);//数据输出到模板
        //=================================================
        foreach($list as $trs){
        	$suid = $trs['uid'];
        	$tid = $trs['id'];
        	$sers = $fck->where('id='.$suid)->field('bank_name,bank_card,bank_address,qq')->find();
        	$bn[$tid]['bank_name'] = $sers['bank_name'];
			$bn[$tid]['bank_card'] = $sers['bank_card'];
			$bn[$tid]['bank_address'] = $sers['bank_address'];
			$bn[$tid]['qq'] = $sers['qq'];
			unset($sers);
        }
		$this->assign('bn',$bn);

		unset($tiqu,$fck,$field,$trs);
		$this->display ();
	}
	
	public function eb_buy_list(){
		$cashpp = M('cashpp');
		$cash = M('cash');
		$fck = M('fck');
		$fee = M('fee');
		$id = $_SESSION[C('USER_AUTH_KEY')];
		
		$fee_rs = $fee->field('str9')->find(1);
		$str9 = $fee_rs['str9'];
		
		$cid = (int)$_GET['cid'];
		
		$map = array();
		$map['id'] = array('eq',$cid);
		
		$field  = '*';
        //=====================分页开始==============================================
        import ( "@.ORG.ZQPage" );  //导入分页类
        $count = $cashpp->where($map)->count();//总页数
	    $listrows = C('ONE_PAGE_RE');//每页显示的记录数
        $Page = new ZQPage($count,$listrows,1);
        //===============(总页数,每页显示记录数,css样式 0-9)
        $show = $Page->show();//分页变量
        $this->assign('page2',$show);//分页变量输出到模板
        $list2 = $cashpp->where($map)->field($field)->order('id desc')->page($Page->getPage().','.$listrows)->select();
        $this->assign('list2',$list2);//数据输出到模板
        //=================================================
		
		foreach($list2 as $rs){
        	$tid = $rs['bid'];
			$bfield='id,bank_province,bank_city,bank_card,bank_name,qq,user_tel,user_name';
        	$mrs[$tid] = $fck->where('id='.$tid)->field($bfield)->find();
        }
		
		$orderid = $list2['order_id'];
		$bdt = $list2['bdt'];
		$locktime = $bdt+$str9*3600;
		
		$this->assign('mrs',$mrs);//数据输出到模板
		$this->assign('orderid',$orderid);
		$this->assign('locktime',$locktime);
		$this->display();
	}
	
	public function eb_sell_list(){
		$cashpp = M('cashpp');
		$cash = M('cash');
		$fck = M('fck');
		$fee = M('fee');
		$id = $_SESSION[C('USER_AUTH_KEY')];
		
		$fee_rs = $fee->field('str9')->find(1);
		$str9 = $fee_rs['str9'];
		
		$cid = (int)$_GET['cid'];
		$where = array();
		$where['id'] = array('eq',$cid);
		
		$crs = $cash ->where($where)->find();
		$orderid = $crs['order_id'];
		$bdt = $crs['bdt'];
		$locktime = $bdt+$str9*3600;
		
		$map = array();
		$map['b_order_id'] = array('eq',$crs['order_id']);
		
		$field  = '*';
        //=====================分页开始==============================================
        import ( "@.ORG.ZQPage" );  //导入分页类
        $count = $cashpp->where($map)->count();//总页数
	    $listrows = C('ONE_PAGE_RE');//每页显示的记录数
        $Page = new ZQPage($count,$listrows,1);
        //===============(总页数,每页显示记录数,css样式 0-9)
        $show = $Page->show();//分页变量
        $this->assign('page2',$show);//分页变量输出到模板
        $list2 = $cashpp->where($map)->field($field)->order('id desc')->page($Page->getPage().','.$listrows)->select();
        $this->assign('list2',$list2);//数据输出到模板
        //=================================================
		
		foreach($list2 as $rs){
        	$tid = $rs['uid'];
			$bfield='id,bank_province,bank_city,bank_card,bank_name,qq,user_tel,user_name';
        	$mrs[$tid] = $fck->where('id='.$tid)->field($bfield)->find();
        }
		
		$this->assign('mrs',$mrs);//数据输出到模板
		$this->assign('orderid',$orderid);
		$this->assign('locktime',$locktime);
		$this->display();
	}
	
	public function eb_list(){
		$tiqu = M('cash');
		$fck = M('fck');

		$id = $_SESSION[C('USER_AUTH_KEY')];


		//卖
		$map = array();
		$map['uid'] = array('eq',$id);
		$map['is_pay'] = array('eq',0);

		$field  = '*';
        //=====================分页开始==============================================
        import ( "@.ORG.ZQPage" );  //导入分页类
        $count = $tiqu->where($map)->count();//总页数
	    $listrows = C('ONE_PAGE_RE');//每页显示的记录数
        $Page = new ZQPage($count,$listrows,1);
        //===============(总页数,每页显示记录数,css样式 0-9)
        $show = $Page->show();//分页变量
        $this->assign('page2',$show);//分页变量输出到模板
        $list2 = $tiqu->where($map)->field($field)->order('id desc')->page($Page->getPage().','.$listrows)->select();
        $this->assign('list2',$list2);//数据输出到模板
        //=================================================

        //---------


		//买
		$map = array();
		$map['bid'] = array('eq',$id);
		$map['is_pay'] = array('eq',0);

		$field  = '*';
        //=====================分页开始==============================================
        import ( "@.ORG.ZQPage" );  //导入分页类
        $count2 = $tiqu->where($map)->count();//总页数
	    $listrows = C('ONE_PAGE_RE');//每页显示的记录数
        $Page2 = new ZQPage($count2,$listrows,1);
        //===============(总页数,每页显示记录数,css样式 0-9)
        $show2 = $Page2->show();//分页变量
        $this->assign('page',$show2);//分页变量输出到模板
        $list = $tiqu->where($map)->field($field)->order('id desc')->page($Page2->getPage().','.$listrows)->select();
        $this->assign('list',$list);//数据输出到模板
        //=================================================
        foreach($list2 as $rs){
        	$myid = $rs['bid'];
        	$tid = $rs['id'];
        	$mrs = $fck->where('id='.$myid)->field('id,seller_rate')->find();
        	$mrate = (int)$mrs['seller_rate'];
        	$s_img = "";
        	if($mrate>0){
        		for($i=1;$i<=$mrate;$i++){
					$s_img .='<img src="__PUBLIC__/Images/star.gif" />';
				}
        	}
        	$voo[$tid] = $s_img;
        }

        foreach($list as $trs){
        	$suid = $trs['uid'];
        	$tid = $trs['id'];
        	$sers = $fck->where('id='.$suid)->field('bank_name,bank_card,bank_address,qq')->find();
        	$bn[$tid]['bank_name'] = $sers['bank_name'];
			$bn[$tid]['bank_card'] = $sers['bank_card'];
			$bn[$tid]['bank_address'] = $sers['bank_address'];
			$bn[$tid]['qq'] = $sers['qq'];
			unset($sers);
        }
		$this->assign('bn',$bn);
		$this->assign('voo',$voo);

		unset($tiqu,$fck,$field,$rs,$trs);
		$this->display ();
	}

	public function eb_listAC(){

		$cash = M('cash');
		$fck = M('fck');

		$id = $_SESSION[C('USER_AUTH_KEY')];
		$mrs = $fck->where('id='.$id)->field('id,user_id')->find();
		if(!$mrs){
			$this->error('参数错误！');
			exit;
		}

		$cid = (int)$_GET['cid'];
		if(empty($cid)){
			$this->error('参数错误！');
			exit;
		}

		$map = array();
		$map['id'] = array('eq',$cid);
		$map['is_pay'] = array('eq',0);
		$map['is_buy'] = array('eq',1);
		$map['bid'] = array('eq',$id);

		$rs = $cash->where($map)->find();
		$this->assign('rs',$rs);
		$suid = $rs['uid'];
        $sers = $fck->where('id='.$suid)->field('bank_name,bank_card,bank_address,qq,user_tel')->find();
        $bn['bank_name'] = $sers['bank_name'];
		$bn['bank_card'] = $sers['bank_card'];
		$bn['bank_address'] = $sers['bank_address'];
		$bn['qq'] = $sers['qq'];
		$bn['user_tel'] = $sers['user_tel'];
		unset($sers);
		$this->assign('bn',$bn);

		$this->display();
	}

	//锁定
	public function eb_list_AC(){

		$cash = M('cash');
		$fck = M('fck');

		$id = $_SESSION[C('USER_AUTH_KEY')];
		$mrs = $fck->where('id='.$id)->field('id,user_id,passopen')->find();
		if(!$mrs){
			$this->error('参数错误！');
			exit;
		}

		$cid = (int)$_POST['cid'];
		if(empty($cid)){
			$this->error('参数错误！');
			exit;
		}

		if(md5($_POST['verify']) != $_SESSION['verify']) {
			$this->error('验证码错误！');
			exit;
		}

		$uspass = $_POST['uspass'];
		$pass3 = $mrs['passopen'];
		$passMD = md5($uspass);
		if($pass3!=$passMD){
			$this->error('二级密码错误！');
			exit;
		}

		$map = array();
		$map['id'] = array('eq',$cid);
		$map['is_pay'] = array('eq',0);
		$map['is_buy'] = array('eq',1);
		$map['bid'] = array('eq',$id);

		$rs = $cash->where($map)->find();
		if($rs){

			$se_uid = $rs['uid'];
			$sers = $fck->where('id='.$se_uid)->field('id,user_tel,user_name,bank_name,bank_card,bank_address,qq')->find();

			$dsql = "ldt=".mktime().",is_buy=2," .
					"bank_name='".$sers['bank_name']."',bank_card='".$sers['bank_card']."'," .
					"user_name='".$sers['user_name']."',x1='".$sers['bank_address']."',x2='".$sers['qq']."'";//锁定交易时连同银行信息一同锁定
			$wsql = "id=".$cid." and is_pay=0 and is_buy=1 and bid=".$id;

			$resute = $cash->execute("update __TABLE__ set ".$dsql." where ".$wsql);
			if($resute!=false){
				$bUrl = __URL__.'/eb_list_b';
				$this->_box(1,'锁定购买成功，请耐心等待卖家确认！',$bUrl,1);
				exit;
			}else{
				$this->error('锁定失败！');
				exit;
			}

		}else{
			$this->error('参数错误！');
			exit;
		}
	}

	//撤销确认
	public function eb_list_CAC(){

		$cash = M('cash');
		$fck = M('fck');

		$id = $_SESSION[C('USER_AUTH_KEY')];
		$mrs = $fck->where('id='.$id)->field('id,user_id')->find();
		if(!$mrs){
			$this->error('参数错误！');
			exit;
		}

		$cid = (int)$_GET['cid'];
		if(empty($cid)){
			$this->error('参数错误！');
			exit;
		}

		$map = array();
		$map['id'] = array('eq',$cid);
		$map['is_pay'] = array('eq',0);
		$map['is_buy'] = array('eq',1);
		$map['bid'] = array('eq',$id);

		$rs = $cash->where($map)->find();
		$this->assign('rs',$rs);
		$this->display();
	}

	//买家撤销
	public function eb_list_cancel(){

		$cash = M('cash');
		$fck = M('fck');
		$xfhistory = M('xfhistory');


		$id = $_SESSION[C('USER_AUTH_KEY')];
		$mrs = $fck->where('id='.$id)->field('id,user_id,passopen')->find();
		if(!$mrs){
			$this->error('参数错误！');
			exit;
		}

		$cid = (int)$_POST['cid'];
		if(empty($cid)){
			$this->error('参数错误！');
			exit;
		}
		$cbz = trim($_POST['cbz']);
		if(empty($cbz)){
			$this->error('请填写撤销原因！');
			exit;
		}

		if(md5($_POST['verify']) != $_SESSION['verify']) {
			$this->error('验证码错误！');
			exit;
		}

		$uspass = $_POST['uspass'];
		$pass3 = $mrs['passopen'];
		$passMD = md5($uspass);
		if($pass3!=$passMD){
			$this->error('二级密码错误！');
			exit;
		}

		$map = array();
		$map['id'] = array('eq',$cid);
		$map['is_pay'] = array('eq',0);
		$map['is_buy'] = array('eq',1);
		$map['bid'] = array('eq',$id);

		$rs = $cash->where($map)->find();
		if($rs){

			$dsql = "bid=0,b_user_id='',bdt=0,is_buy=0";
			$wsql = "id=".$cid." and is_pay=0 and is_buy=1 and bid=".$id;

			$resute = $cash->execute("update __TABLE__ set ".$dsql." where ".$wsql);
			if($resute){

				$data = array();
				$data['uid'] = $rs['uid'];
				$data['user_id'] = $rs['user_id'];
				$data['did'] = $rs['bid'];
				$data['d_user_id'] = $rs['b_user_id'];
				$data['action_type'] = 1;//1买家撤销 2卖家撤销 3交易完成
				$data['pdt'] = mktime();
				$data['epoints'] = $rs['money'];
				$data['allp'] = $rs['money'];
				$data['bz'] = '买家撤销，原因：'.$cbz;
				$xfhistory->add($data);

				$bUrl = __URL__.'/eb_list_b';
				$this->_box(1,'撤销成功！',$bUrl,1);
				exit;
			}else{
				$this->error('撤销失败！');
				exit;
			}

		}else{
			$this->error('参数错误！');
			exit;
		}
	}

	//卖家撤销确认
	public function eb_listDelAC(){

		$cash = M('cash');
		$fck = M('fck');

		$id = $_SESSION[C('USER_AUTH_KEY')];
		$mrs = $fck->where('id='.$id)->field('id,user_id')->find();
		if(!$mrs){
			$this->error('参数错误！');
			exit;
		}

		$cid = (int)$_GET['del_id'];
		if(empty($cid)){
			$this->error('参数错误！');
			exit;
		}

		$map = array();
		$map['id'] = array('eq',$cid);
		$map['is_pay'] = array('eq',0);
		$map['is_buy'] = array('lt',2);
		$map['uid'] = array('eq',$id);

		$rs = $cash->where($map)->find();
		$this->assign('rs',$rs);
		$this->display();
	}

	//卖家撤销
	public function eb_list_del(){

		$cash = M('cash');
		$fck = D('Fck');
		$xfhistory = M('xfhistory');
		$bonushistory=M('bonushistory');


		$id = $_SESSION[C('USER_AUTH_KEY')];
		$mrs = $fck->where('id='.$id)->field('id,user_id,passopen')->find();
		if(!$mrs){
			$this->error('参数错误！');
			exit;
		}
		
		$cid = (int)$_GET['cid'];
		//echo $cid; exit;
		$map = array();
		$map['id'] = array('eq',$cid);
		//$map['is_pay'] = array('eq',0);
		//$map['is_buy'] = array('lt',1);
		$map['uid'] = array('eq',$id);
		
		$rs = $cash->where($map)->find();
		//echo  $cash->getLastSql($rs);  
		//		dump($rs); exit;
		
		
		if($rs){
			$data = array();
			$data['uid'] = $rs['uid'];
			$data['user_id'] = $rs['user_id'];
			$data['did'] = $rs['bid'];
			$data['d_user_id'] = $rs['b_user_id'];
			$data['action_type'] = $rs['s_type'];//1买家撤销 2卖家撤销 3交易完成
			$data['pdt'] = mktime();
			$data['epoints'] = $rs['money'];
			$data['allp'] = $rs['money'];
			$data['bz'] = '撤销订单';
			$result = $xfhistory->add($data);

			if($result){
				$cash->where($map)->delete();
				$fck->cancel_order_isout($rs['id']);
				$bonushistory->where('did='.$rs['id'])->delete();
				
				$bUrl = __APP__;
				//$this->_boxx($bUrl);
				$this->_box(1,'撤销成功！',$bUrl,1,1);
				exit;
			}else{
				$this->error('撤销失败！');
				exit;
			}

		}else{
			$this->error('参数错误！');
			exit;
		}
	}

	public function eb_list_DAC(){

		$cash = M('cash');
		$fck = M('fck');

		$id = $_SESSION[C('USER_AUTH_KEY')];
		$mrs = $fck->where('id='.$id)->field('id,user_id')->find();
		if(!$mrs){
			$this->error('参数错误！');
			exit;
		}

		$cid = (int)$_GET['cid'];
		if(empty($cid)){
			$this->error('参数错误！');
			exit;
		}

		$map = array();
		$map['id'] = array('eq',$cid);
		$map['is_pay'] = array('eq',0);
		$map['is_buy'] = array('eq',2);
		$map['uid'] = array('eq',$id);

		$rs = $cash->where($map)->find();
		$this->assign('rs',$rs);
		$this->display();
	}

	//确认交易
	public function eb_list_done(){
        $fee=M('Fee');
		$cashpp = M('cashpp');
		$cash = M('cash');
		$fck = D('Fck');
		$xfhistory = M('xfhistory');

		$fee_rs = $fee->field('s5')->find(1);
		$s5 = $fee_rs['s5'];
		$id = $_SESSION[C('USER_AUTH_KEY')];
		$mrs = $fck->where('id='.$id)->field('id,user_id,passopen')->find();
		if(!$mrs){
			$this->error('参数错误！');
			exit;
		}

		$cid = (int)$_GET['t_id'];
		if(empty($cid)){
			$this->error('参数错误！');
			exit;
		}

		$map = array();
		$map['id'] = array('eq',$cid);
		$map['is_pay'] = array('eq',0);
		$map['is_buy'] = array('eq',2);
		$map['is_sh'] = array('eq',1);
		
	//	echo $cid; exit;
	
				   //查询匹配成功的id
		$cding=$cashpp->where('id='.$cid)->find();
		
		
	
		//查询匹配成功人的号码
		$cxin=$fck->where('user_id='.$cding['user_id'])->find();
		$phone=$cxin['user_tel'];
		$tiid=$cxin['uid'];
		//echo $phone;exit;
		 $diao=A("Sms");  
		 
         $diao->qsh($phone);  
		
		

		$rs = $cashpp->where($map)->find();
		$ding=$rs['order_id'];
		$dan=$cash->where('order_id='.$ding)->find();
		$lei=$dan['leixing'];
		$user_id=$rs['user_id'];				//打款者
		$oktime=$rs['bdt'];				//匹配时间
		if($rs){
			//启动事务
			$fck->startTrans();
			
			$cmoney = $rs['money'];
			
			$dsql = "is_pay=1,okdt=".mktime()."";
			$wsql = "id=".$cid." and is_pay=0 and is_buy=2";
			$result = $cashpp->execute("update __TABLE__ set ".$dsql." where ".$wsql);
			$xtime=$time-$oktime;
			if($xtime<=18000){			//5个小时内打款
			$extra=$cmoney*0.001*$s5;		//额外奖金解冻
			$cmoney1=$extra+$cmoney;
			}
			$fck->addCashhistory($mrs['id'],-$cmoney,5,'取出扣帐',1);
			if($lei==0){            //本息
				
				$result2 = $fck->execute("update __TABLE__ set `agent_use`=agent_use-".$cmoney.",`re_money`=re_money+".$cmoney." where id=".$mrs['id']);
				
			}
			
			if($lei==1){            //经理
				
				$result2 = $fck->execute("update __TABLE__ set `jingli`=jingli-".$cmoney.",`re_money`=re_money+".$cmoney." where id=".$mrs['id']);
				
			}
			
			if($lei==2){               //基金
				
				$result2 = $fck->execute("update __TABLE__ set `touzi`=touzi-".$cmoney.",`re_money`=re_money+".$cmoney." where id=".$mrs['id']);
				
			}

			if($result && $result2){
				$buyorid = $rs['order_id'];
				$sellorid = $rs['b_order_id'];
				
				$buynotdonenum = $cashpp -> where("order_id='".$buyorid."' and is_pay=0")->count();
				if($buynotdonenum==0){
					$cash->execute("update __TABLE__ set is_pay=1,okdt=".mktime()." where money_two=0 and is_buy=2 and order_id='".$buyorid."'");
					
					$jsrs = $cash -> where("order_id='".$buyorid."'")->find();
					$sell = $cash -> where("order_id='".$sellorid."'")->find();
					//$sell['money']=800;
					$result3 = $fck -> execute("update __TABLE__ set `cpzj`=".$sell['money']." where  id=".$jsrs['uid']);
					
				//	if($result3){
					
				//	echo $jsrs['user_id'];
                //       exit;					
					$fck -> getusjj($jsrs['uid'],$jsrs['money'],1,$jsrs['id'],$jsrs['user_id']);
					
				//	}
				}
				
				$sellnotdonenum = $cashpp -> where("b_order_id='".$sellorid."' and is_pay=0")->count();
				if($sellnotdonenum==0){
					$cash->execute("update __TABLE__ set is_pay=1,okdt=".mktime()." where money_two=0 and is_buy=2 and order_id='".$sellorid."'");
				}
				
				$fck->addCashhistory($rs['uid'],$cmoney,6,'存入入帐',1);
				if(!empty($cmoney1)){
				$res=$fck->execute("update __TABLE__ set `agent_use`=agent_use+".$cmoney1." where id=".$rs['uid']);
				if($res){
					$fck->addCashhistory($rs['uid'],$extra,6,'额外利息',1);
				}
				}else{
				$fck->execute("update __TABLE__ set `agent_use`=agent_use+".$cmoney." where id=".$rs['uid']);
				
				}
				
				
			
			
				

				$data = array();
				$data['uid'] = $rs['uid'];
				$data['user_id'] = $rs['user_id'];
				$data['did'] = $rs['bid'];
				$data['d_user_id'] = $rs['b_user_id'];
				$data['action_type'] = 3;//1买家撤销 2卖家撤销 3交易完成
				$data['pdt'] = mktime();
				$data['epoints'] = $rs['money'];
				$data['allp'] = $rs['money'];
				$data['bz'] = '交易完成';
				$xfhistory->add($data);
				//执行
				$fck->commit();
				
				$fck ->jinglijiang($jsrs['uid'],$sell['money'],1,$jsrs['id']);
				
				


					
			      
		      
		      
				$bUrl = __APP__;
				$this->_box(1,'确认交易成功！',$bUrl,1,1);
				//$this->_boxx($bUrl);
				exit;
			}else{
				//事务回滚：
				$fck->rollback();
				$this->error('确认交易失败！');
				exit;
			}

		}else{
			$this->error('参数错误！');
			exit;
		}
	}

	//买家未收到款项撤销确认
	public function eb_list_NODAC(){

		$cash = M('cash');
		$fck = M('fck');
		$fee = M('fee');

//		$fee_rs = $fee->field('str40')->find();
//		$str40 = $fee_rs['str40'];
		$str40 = 48;

		$id = $_SESSION[C('USER_AUTH_KEY')];
		$mrs = $fck->where('id='.$id)->field('id,user_id')->find();
		if(!$mrs){
			$this->error('参数错误！');
			exit;
		}

		$cid = (int)$_GET['cid'];
		if(empty($cid)){
			$this->error('参数错误！');
			exit;
		}

		$map = array();
		$map['id'] = array('eq',$cid);
		$map['is_pay'] = array('eq',0);
		$map['is_buy'] = array('eq',2);
		$map['uid'] = array('eq',$id);

		$rs = $cash->where($map)->find();
		$this->assign('rs',$rs);
		$nnt = $rs['ldt'];
		$voo = $nnt+3600*$str40;
		$this->assign('voo',$voo);
		$this->display();
	}

	//买家未收到款项撤销
	public function eb_list_Nook(){

		$cash = M('cash');
		$fck = M('fck');
		$xfhistory = M('xfhistory');


		$id = $_SESSION[C('USER_AUTH_KEY')];
		$mrs = $fck->where('id='.$id)->field('id,user_id,passopen')->find();
		if(!$mrs){
			$this->error('参数错误！');
			exit;
		}

		$cid = (int)$_POST['cid'];
		if(empty($cid)){
			$this->error('参数错误！');
			exit;
		}
		$cbz = "未收到款，等待后台处理。";

		if(md5($_POST['verify']) != $_SESSION['verify']) {
			$this->error('验证码错误！');
			exit;
		}

		$uspass = $_POST['uspass'];
		$pass3 = $mrs['passopen'];
		$passMD = md5($uspass);
		if($pass3!=$passMD){
			$this->error('二级密码错误！');
			exit;
		}

		$map = array();
		$map['id'] = array('eq',$cid);
		$map['is_pay'] = array('eq',0);
		$map['is_buy'] = array('eq',2);
		$map['uid'] = array('eq',$id);

		$rs = $cash->where($map)->find();
		if($rs){

			$dsql = "is_pay=2,okdt=".mktime()."";//2为交由后台处理，3为已经处理
			$wsql = "id=".$cid." and is_pay=0 and is_buy=2 and uid=".$id;

			$resute = $cash->execute("update __TABLE__ set ".$dsql." where ".$wsql);

			$data = array();
			$data['uid'] = $rs['uid'];
			$data['user_id'] = $rs['user_id'];
			$data['did'] = $rs['bid'];
			$data['d_user_id'] = $rs['b_user_id'];
			$data['action_type'] = 2;//1买家撤销 2卖家撤销 3交易完成
			$data['pdt'] = mktime();
			$data['epoints'] = $rs['money'];
			$data['allp'] = $rs['money'];
			$data['bz'] = '卖家撤销，原因：'.$cbz;
			$xxid = $xfhistory->add($data);

			if($resute&&$xxid){

				$cash->execute("update __TABLE__ set x4='".$xxid."' where id=".$cid);//更新历史记录ID

				//执行
				$fck->commit();

				$bUrl = __URL__.'/eb_list';
				$this->_box(1,'撤销成功，请等待后台审核处理！',$bUrl,1);
				exit;
			}else{
				//事务回滚：
				$fck->rollback();
				$this->error('撤销失败！');
				exit;
			}
		}else{
			$this->error('参数错误！');
			exit;
		}
	}

	public function eb_history(){
		$xfhistory = M('xfhistory');
		$fck = M('fck');

		$id = $_SESSION[C('USER_AUTH_KEY')];

		$map = array();
		$where['uid'] = array('eq',$id);
		$where['did'] = array('eq',$id);
		$where['_logic']    = 'or';
		$map['_complex']    = $where;

		$sdate = $_REQUEST['Sdate'];
		$edate = $_REQUEST['Edate'];
		if(!empty($sdate)){
			$ss_d = strtotime($sdate);
			$map['pdt'] = array('egt',$ss_d);
		}
		if(!empty($edate)){
			$ee_d = strtotime($edate)+3600*24-1;
			$map['_string'] = 'pdt<='.$ee_d;
		}

		$field  = '*';
        //=====================分页开始==============================================
        import ( "@.ORG.ZQPage" );  //导入分页类
        $count = $xfhistory->where($map)->count();//总页数
	    $listrows = C('ONE_PAGE_RE');//每页显示的记录数
         $page_where = 'Sdate=' . $sdate . '&Edate=' . $edate;//分页条件
            $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where);
        //===============(总页数,每页显示记录数,css样式 0-9)
        $show = $Page->show();//分页变量
        $this->assign('page',$show);//分页变量输出到模板
        $list = $xfhistory->where($map)->field($field)->order('id desc')->page($Page->getPage().','.$listrows)->select();
        $this->assign('list',$list);//数据输出到模板
        //=================================================

		unset($xfhistory,$fck,$field);
		$this->display ();
	}

	//后台管理EB交易
	public function admin_eblist(){
		//列表过滤器，生成查询Map对象
		if ($_SESSION['UrlPTPass'] == 'MyssEBlist'){

			$cash = M('cashpp');
			$UserID = $_REQUEST['UserID'];
			$ss_type = (int) $_REQUEST['ry'];
			if (!empty($UserID)){
				$UserID = strtoupper($UserID);
				import ( "@.ORG.KuoZhan" );  //导入扩展类
                $KuoZhan = new KuoZhan();
                if ($KuoZhan->is_utf8($UserID) == false){
                    $UserID = iconv('GB2312','UTF-8',$UserID);
                }
                unset($KuoZhan);

				$where['user_id'] = array('like',"%".$UserID."%");
				$where['b_user_id'] = array('like',"%".$UserID."%");
				$where['_logic']    = 'or';
				$map['_complex']    = $where;
				$UserID = urlencode($UserID);
			}

			if($ss_type==1){
				$map['is_pay'] = array('eq',0);
				$map['is_buy'] = array('eq',0);
			}elseif($ss_type==2){
				$map['is_pay'] = array('eq',0);
				$map['is_buy'] = array('eq',1);
			}elseif($ss_type==3){
				$map['is_pay'] = array('eq',0);
				$map['is_buy'] = array('eq',2);
			}elseif($ss_type==4){
				$map['is_pay'] = array('eq',1);
			}elseif($ss_type==5){
				$map['is_pay'] = array('eq',2);
			}elseif($ss_type==6){
				$map['is_pay'] = array('gt',2);
			}

			$ft_date = strtotime(date("Y-m-d"))-3600*24*30;//30天内
			$map['_string'] = "rdt>".$ft_date;
			$this->assign('UserID',$UserID);
			$this->assign('ry',$ss_type);
//			$map['is_pay'] = array('gt',1);
            //查询字段
            $field  = '*';
            //=====================分页开始==============================================
            import ( "@.ORG.ZQPage" );  //导入分页类
            $count = $cash->where($map)->count();//总页数
       		$listrows = C('ONE_PAGE_RE');//每页显示的记录数
            $page_where = 'UserID=' . $UserID . '&ry=' . $ss_type;//分页条件
            $Page = new ZQPage($count, $listrows, 2, 0, 3, $page_where);
            //===============(总页数,每页显示记录数,css样式 0-9)
            $show = $Page->show();//分页变量
            $this->assign('page',$show);//分页变量输出到模板
            $list = $cash->where($map)->field($field)->order('id desc')->page($Page->getPage().','.$listrows)->select();
            $this->assign('list',$list);//数据输出到模板
            //=================================================

			$this->display ();
			return;
		}else{
			$this->error('数据错误!');
			exit;
		}
	}

	public function admin_eblistAC(){
		//列表过滤器，生成查询Map对象
		if ($_SESSION['UrlPTPass'] == 'MyssEBlist'){

			$cash = M('cash');
			$cashpp = M('cashpp');
			$xfhistory = M('xfhistory');
			$fck = D('Fck');

			$cid = $_GET['cid'];

			$map = array();
			$map['id'] = array('eq',$cid);
			$map['is_pay'] = array('eq',0);
			$map['is_buy'] = array('eq',2);
			$map['is_sh'] = array('eq',1);
	
			$rs = $cashpp->where($map)->find();
			if($rs){
				//启动事务
				$fck->startTrans();
				
				$cmoney = $rs['money'];
				$buyid = $rs['uid'];
				$sellid = $rs['bid'];
				
				$dsql = "is_pay=1,okdt=".mktime()."";
				$wsql = "id=".$cid." and is_pay=0 and is_buy=2";
				$result = $cashpp->execute("update __TABLE__ set ".$dsql." where ".$wsql);
				
				$fck->addCashhistory($sellid,-$cmoney,5,'取出扣帐',1);
				$result2 = $fck->execute("update __TABLE__ set `agent_use`=agent_use-".$cmoney.",`re_money`=re_money+".$cmoney." where id=".$sellid);
	
				if($result && $result2){
					$buyorid = $rs['order_id'];
					$sellorid = $rs['b_order_id'];
					
					$buynotdonenum = $cashpp -> where("order_id='".$buyorid."' and is_pay=0")->count();
					if($buynotdonenum==0){
						$cash->execute("update __TABLE__ set is_pay=1,okdt=".mktime()." where money_two=0 and is_buy=2 and order_id='".$buyorid."'");
						
						$jsrs = $cash -> where("order_id='".$buyorid."'")->find();
						$result3 = $fck -> execute("update __TABLE__ set `cpzj`=".$jsrs['money']." where cpzj=0 and id=".$jsrs['uid']);
						if($result3){
						$fck -> getusjj($jsrs['uid'],$jsrs['money'],1,$jsrs['id'],$tiid);}
					}
					
					$sellnotdonenum = $cashpp -> where("b_order_id='".$sellorid."' and is_pay=0")->count();
					if($sellnotdonenum==0){
						$cash->execute("update __TABLE__ set is_pay=1,okdt=".mktime()." where money_two=0 and is_buy=2 and order_id='".$sellorid."'");
					}
					
					$fck->addCashhistory($rs['uid'],$cmoney,6,'存入入帐',1);
					$fck->execute("update __TABLE__ set `agent_use`=agent_use+".$cmoney." where id=".$rs['uid']);
	
					$data = array();
					$data['uid'] = $rs['uid'];
					$data['user_id'] = $rs['user_id'];
					$data['did'] = $rs['bid'];
					$data['d_user_id'] = $rs['b_user_id'];
					$data['action_type'] = 3;//1买家撤销 2卖家撤销 3交易完成
					$data['pdt'] = mktime();
					$data['epoints'] = $rs['money'];
					$data['allp'] = $rs['money'];
					$data['bz'] = '交易完成';
					$xfhistory->add($data);
					//执行
					$fck->commit();
	
					$bUrl = __APP__;
					$this->_box(1,'确认交易成功！',$bUrl,1,1);
					//$this->_boxx($bUrl);
					exit;
				}else{
					//事务回滚：
					$fck->rollback();
					$this->error('确认交易失败！');
					exit;
				}
	
			}else{
				$this->error('参数错误！');
				exit;
			}


			$this->display ();
			return;
		}else{
			$this->error('数据错误!');
			exit;
		}
	}

	//卖家撤销
	public function admin_eblist_del(){
		if ($_SESSION['UrlPTPass'] == 'MyssEBlist'||$_SESSION['UrlPTPass'] == 'Myssusereblist'){
			$fck = M('fck');
			$cash = M('cash');
			$cashpp = M('cashpp');
			$xfhistory = M('xfhistory');

			$cid = (int)$_REQUEST['cid'];
			$dp = (int)$_REQUEST['dp'];
			if(empty($cid)){
				$this->error('参数错误！');
				exit;
			}

			$map = array();
			$map['id'] = array('eq',$cid);
			$map['is_pay'] = array('eq',0);
			$map['is_buy'] = array('elt',2);

			$rs = $cashpp->where($map)->find();
			if($rs){

				$data = array();
				$data['uid'] = $rs['uid'];
				$data['user_id'] = $rs['user_id'];
				$data['did'] = $rs['bid'];
				$data['d_user_id'] = $rs['b_user_id'];
				$data['action_type'] = $rs['is_buy'];//1买家撤销 2卖家撤销 3交易完成
				$data['pdt'] = mktime();
				$data['epoints'] = $rs['money'];
				$data['allp'] = $rs['money'];
				$data['bz'] = '<font color=red>后台提交撤销操作</font>';
				$result = $xfhistory->add($data);

				if($result){
					$cmoney = $rs['money'];
					$buyorderid = $rs['order_id'];
					$sellorderid = $rs['b_order_id'];
					
					$cash->execute("update __TABLE__ set `money_two`=money_two+".$cmoney." where order_id='".$buyorderid."'");
					$cash->execute("update __TABLE__ set is_buy=0,is_sh=0 where money=money_two and order_id='".$buyorderid."'");
					$cash->execute("update __TABLE__ set is_buy=1,is_sh=1 where money>money_two and order_id='".$buyorderid."'");
						
					
					$cash->execute("update __TABLE__ set `money_two`=money_two+".$cmoney." where order_id='".$sellorderid."'");
					$cash->execute("update __TABLE__ set is_buy=0,is_sh=0 where money=money_two and order_id='".$sellorderid."'");
					$cash->execute("update __TABLE__ set is_buy=1,is_sh=1 where money>money_two and order_id='".$sellorderid."'");
					
					if($rs['is_buy']>=1){
					$cash->execute("update __TABLE__ set is_out=1 where order_id='".$buyorderid."'");
					}

					$result1 = $cashpp->where($map)->delete();
					
					if($result1){
					$this->auto_match_eq();
					$this->auto_match_neq();
					}
					
					$bUrl = __URL__.'/admin_eblist';
					$this->_box(1,'撤销成功！',$bUrl,1);
					exit;
				}else{
					$this->error('撤销失败！');
					exit;
				}

			}else{
				$this->error('参数错误！');
				exit;
			}
		}else{
			$this->error('数据错误!');
			exit;
		}
	}
	
		//确认交易
	public function admin_eblist_done(){
		$this->_Admin_checkUser();
		if ($_SESSION['UrlPTPass'] == 'Mysshoudongcash'){
			$cashpp = M('cashpp');
			$cash = M('cash');
			$fck = M('fck');
			$xfhistory = M('xfhistory');
		
			$cid = (int)$_GET['cid'];
			if(empty($cid)){
				$this->error('参数错误！');
				exit;
			}
	
			$map = array();
			$map['id'] = array('eq',$cid);
			$map['is_pay'] = array('eq',0);
			$map['is_buy'] = array('eq',2);
			$map['is_sh'] = array('eq',2);
	
			$rs = $cash->where($map)->find();
			if($rs){
				//启动事务
				$fck->startTrans();
				
				$cmoney = $rs['money'];
				$outid = $rs['bid'];
				
				$dsql = "is_pay=1,okdt=".mktime()."";
				$wsql = "id=".$cid." and is_pay=0 and is_buy=2 and uid=".$id;
				$result = $cash->execute("update __TABLE__ set ".$dsql." where ".$wsql);
	
				$result2 = $fck->execute("update __TABLE__ set agent_use=agent_use-".$cmoney." where id=".$outid);
	
				if($result&&$result2){
					$pporid = $rs['b_order_id'];
									
					$pprs = $cashpp -> where("b_order_id='".$pporid."'") -> select();			
					foreach($pprs as $prs){
						$inorid = $prs['order_id'];
						$incrs = $cash -> where("order_id='".$inorid."'")->find();
						
						$dsql1 = "okdt=".mktime().",is_pay=1";
						$wsql1 = "id=".$incrs['id']." and is_pay=0 and is_sh=2";
						if($incrs['is_buy']==2){
							$cash->execute("update __TABLE__ set ".$dsql1." where ".$wsql1);
							$fck->execute("update __TABLE__ set agent_use=agent_use+".$incrs['money']." where ".$incrs['uid']);
						}
					}
	
					$data = array();
					$data['uid'] = $rs['uid'];
					$data['user_id'] = $rs['user_id'];
					$data['did'] = $rs['bid'];
					$data['d_user_id'] = $rs['b_user_id'];
					$data['action_type'] = 3;//1买家撤销 2卖家撤销 3交易完成
					$data['pdt'] = mktime();
					$data['epoints'] = $rs['money'];
					$data['allp'] = $rs['money'];
					$data['bz'] = '交易完成';
					$xfhistory->add($data);
					//执行
					$fck->commit();
	
					$bUrl = __URL__.'/eb_list';
					$this->_box(1,'确认交易成功！',$bUrl,1);
					exit;
				}else{
					//事务回滚：
					$fck->rollback();
					$this->error('确认交易失败！');
					exit;
				}
	
			}else{
				$this->error('参数错误！');
				exit;
			}
		}
	}
	
	
public function autopp(){
	$this->auto_match_eq();
	$this->auto_match_neq();
	$bUrl = __URL__.'/adminCash';//列表
    $this->_box(1,'操作完成！',$bUrl,1);
	}	
	
	
	
	//手动匹配
	public function adminCash(){
		$this->_Admin_checkUser();
		if ($_SESSION['UrlPTPass'] == 'Mysshoudongcash'){
			$cash = M ('cash');
			$ebwhere= array();
			$ebwhere['s_type'] = 0;
			$ebfield  = '*';
			//=====================分页开始==============================================
			import ( "@.ORG.ZQPage" );  //导入分页类
			$count = $cash->where($ebwhere)->count();//总页数
			$listrows = 5;//每页显示的记录数
			$Page = new ZQPage($count,$listrows,1);
			//===============(总页数,每页显示记录数,css样式 0-9)
			$show = $Page->show();//分页变量
			$this->assign('page',$show);//分页变量输出到模板
			$elist = $cash->where($ebwhere)->field($ebfield)->order('id desc')->page($Page->getPage().','.$listrows)->select();
			$this->assign('elist',$elist);//数据输出到模板
			//=================================================
			
			$ebwhere= array();
			$ebwhere['s_type'] = 1;
			$ebfield  = '*';
			//=====================分页开始==============================================
			import ( "@.ORG.ZQPage" );  //导入分页类
			$count = $cash->where($ebwhere)->count();//总页数
			$listrows = 5;//每页显示的记录数
			$Page1 = new ZQPage($count,$listrows,1);
			//===============(总页数,每页显示记录数,css样式 0-9)
			$show = $Page1->show();//分页变量
			$this->assign('page1',$show);//分页变量输出到模板
			$eblist = $cash->where($ebwhere)->field($ebfield)->order('id desc')->page($Page1->getPage().','.$listrows)->select();
			$this->assign('eblist',$eblist);//数据输出到模板
			//=================================================

			$this->display();
		}else{
			$this->error('错误!');
		}
	}
	
	public function changepingzheng(){
			$fck = M('fck');
			$cashpp = M('cashpp');
			$id   = $_SESSION[C('USER_AUTH_KEY')];
			//输出登录用户资料记录
			$cid = (int)$_GET['cid'];
			//echo $cid; exit;
			$where = array();
			$where['id'] = array('eq',$cid);
			
			$crs = $cashpp ->where($where)->find();
			
			//dump($crs); exit;
			$this->assign('crs',$crs);

			$this->display('changepingzheng');
	}
    
	/* --------------- 修改凭证信息 ---------------- */
	public function changepingzhengSave(){
			$fck = M('fck');
			$cashpp = M('cashpp');

			$myw = array();
			$myw['id'] = $_SESSION[C('USER_AUTH_KEY')];
			$mrs = $fck->where($myw)->field('id,wenti_dan')->find();
			if(!$mrs){
				$this->error('非法提交数据!');
				exit;
			}else{
			}

			$PPID = $_POST['PPID']; 
			
			$data = array();
			//echo $_POST['aa'];
			//dump($data); exit;
			
			
			$usimg = trim($_POST['image']);
			
			
			if(empty($usimg)){
				
				$this->error('请上传凭证');
				exit;
				
			}
			
		//	echo $using; exit;
			if(!empty($usimg)){
				$data['sellbz']	= $usimg;
			}
			$data['id'] = $PPID;
			$rs = $cashpp->save($data);
			
			if($rs){
				$bUrl = __URL__.'/tigong';
				$this->_box(1,'上传凭证成功！',$bUrl,1);
			}else{
				$this->error('操作错误2!');
				exit;
			}
	}
	
	/**
     * 上传图片
     * **/
	public function upload_fengcai_pp() {
        if(!empty($_FILES)) {
            //如果有文件上传 上传附件
            $this->_upload_fengcai_pp();
        }
    }

    protected function _upload_fengcai_pp()
    {
        header("content-type:text/html;charset=utf-8");
        // 文件上传处理函数

        //载入文件上传类
        import("@.ORG.UploadFile");
        $upload = new UploadFile();

        //设置上传文件大小
        $upload->maxSize  = 1048576 * 2 ;// TODO 50M   3M 3292200 1M 1048576

        //设置上传文件类型
        $upload->allowExts  = explode(',','jpg,gif,png,jpeg');

        //设置附件上传目录
        $upload->savePath =  './Public/Uploads/image/';

        //设置需要生成缩略图，仅对图像文件有效
       $upload->thumb =  false;

       //设置需要生成缩略图的文件前缀
        $upload->thumbPrefix   =  'm_';  //生产2张缩略图

       //设置缩略图最大宽度
        $upload->thumbMaxWidth =  '800';

       //设置缩略图最大高度
        $upload->thumbMaxHeight = '600';

       //设置上传文件规则
//		$upload->saveRule = uniqid;
		$upload->saveRule = date("Y").date("m").date("d").date("H").date("i").date("s").rand(1,100);

       //删除原图
       $upload->thumbRemoveOrigin = true;

        if(!$upload->upload()) {
            //捕获上传异常
            $error_p=$upload->getErrorMsg();
            echo "<script>alert('".$error_p."');history.back();</script>";
        }else {
            //取得成功上传的文件信息
            $uploadList = $upload->getUploadFileInfo();
            $U_path=$uploadList[0]['savepath'];
            $U_nname=$uploadList[0]['savename'];
			
            $U_inpath=(str_replace('./Public/','__PUBLIC__/',$U_path)).$U_nname;

            echo "<script>window.parent.form1.image.value='".$U_inpath."';</script>";
            echo "<span style='font-size:12px;'>上传完成！</span>";
            exit;
               
        }
    }

}
?>